package com.atechtron.beegru.superAdmin.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.atechtron.beegru.login.dto.PropertyEnquiryDto;
import com.atechtron.beegru.login.dto.BeegruScoreDto;
import com.atechtron.beegru.login.dto.BranchDto;
import com.atechtron.beegru.login.dto.FeedbackDto;
import com.atechtron.beegru.login.dto.PostYourRequirementDto;
import com.atechtron.beegru.login.dto.PropertyInfoDto;
import com.atechtron.beegru.login.dto.PropertyTagDto;
import com.atechtron.beegru.login.dto.UserDetails;
import com.atechtron.beegru.login.service.LoginService;
import com.atechtron.beegru.util.GeneratePdfReport1;
import com.atechtron.beegru.website.dto.AboutUsDto;
import com.atechtron.beegru.website.dto.TestimonialsDto;
import com.atechtron.beegru.website.service.WebsiteService;

@Controller
@RequestMapping(value = "/admin")
public class AdminController {
	
	@Autowired
	private LoginService loginService;
	
	//@Autowired
//	private WebsiteService loginService;
	
	@GetMapping("/login")
    public String login(Model model, String error, String logout) throws UsernameNotFoundException{
    	String url = "";
    	try {
    		if (error != null)
    			System.out.println(error +"inside error");
    			model.addAttribute("error", "Your username and password is invalid.");
    			url = "admin_login";

    		if (logout != null)
            	model.addAttribute("message", "You have been logged out successfully.");
            	url = "admin_login";
            	return url;
    	}catch (Exception e) {
    		System.out.println(e.getMessage()+" inside user Controller");
    		e.printStackTrace();
    		return url;
    	}
    }
	
	 @RequestMapping(value = "/forget_password", method = RequestMethod.GET)
	 public String forgetPassword() {
	    return "forgot_password";
	 }
	
	@RequestMapping(value = "/superadmin_dashboard", method = RequestMethod.GET)
	public String superadmin_dashboard() {
		try {
			return "superadmin_dashboard";
		}catch(Exception e) {
			e.printStackTrace();
			return "superadmin_dashboard";
		}
	}	
	
	@RequestMapping(value = "/superadmin_issues", method = RequestMethod.GET)
	public String superadmin_issues() {
		try {
			return "superadmin_issues";
		}catch(Exception e) {
			e.printStackTrace();
			return "superadmin_issues";
		}
	}	
	
	@RequestMapping(value = "/super_admin_beegru_score", method = RequestMethod.GET)
	public String super_admin_beegru_score(Model model) {
		List<BeegruScoreDto> beegruScore = new ArrayList<BeegruScoreDto>();
		try {
			beegruScore = loginService.fetchBeegruScore();
			model.addAttribute("beegruScore", beegruScore);
			return "beegru_score";
		}catch(Exception e) {
			e.printStackTrace();
			return "beegru_score";
		}
	}	
	
	@RequestMapping(value = "/super_admin_home_screen_images",method = RequestMethod.GET)
	public String super_admin_home_screen_images() {
		try {
			return "super_admin_home_screen_images";
		}catch(Exception e) {
			e.printStackTrace();
			return "super_admin_home_screen_images";
		}
	}
	
	@RequestMapping(value = "/post_your_requirement", method = RequestMethod.GET)
	public String post_your_requiement(Model model) {
		List<PostYourRequirementDto> postYourRequirementList = new ArrayList<PostYourRequirementDto>();
		try {
			postYourRequirementList = loginService.getPostYourRequirement();
			System.out.println(postYourRequirementList);
			model.addAttribute("postYourRequirementList",postYourRequirementList);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return "post_your_requirement";
	}
	
	@RequestMapping(value = "/superadmin_profile", method = RequestMethod.GET)
	public String superadmin_profile() {
		try {
			return "superadmin_profile";
		}catch(Exception e) {
			e.printStackTrace();
			return "superadmin_profile";
		}
	}	
	
	@RequestMapping(value = "/superadmin_edit_properties", method = RequestMethod.GET)
	public String superadmin_edit_properties() {
		try {
			return "superadmin_edit_properties";
		}catch(Exception e) {
			e.printStackTrace();
			return "superadmin_edit_properties";
		}
	}	
	
	@RequestMapping(value = "/superadmin_upload_properties", method = RequestMethod.GET)
	public String superadmin_upload_properties() {
		try {
			return "superadmin_upload_properties";
		}catch(Exception e) {
			e.printStackTrace();
			return "superadmin_upload_properties";
		}
	}	
	
	@RequestMapping(value = "/superadmin_uploaded_properties", method = RequestMethod.GET)
	public String superadmin_uploaded_properties(Model model) {
		List<PropertyInfoDto> propertyInfoList = new ArrayList<PropertyInfoDto>();
		try {
			propertyInfoList = loginService.fetchPropertyInfo();
			model.addAttribute("propertyInfoList",propertyInfoList);
			return "superadmin_uploaded_properties";
		}catch(Exception e) {
			e.printStackTrace();
			return "superadmin_uploaded_properties";
		}
	}	
	
	@RequestMapping(value = "/propertyEnquiry", method = RequestMethod.GET)
	public String getPropertyEnquiryList(Model model){
		List<PropertyEnquiryDto> propertyEnquiryList = new ArrayList<PropertyEnquiryDto>();
		try {
			propertyEnquiryList = loginService.fetchPropertyEnquiryList();
			model.addAttribute("propertyEnquiryList",propertyEnquiryList);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return "property_enquire";
	}

	
	@RequestMapping(value = "/superadmin_user_details", method = RequestMethod.GET)
	public String superadmin_user_details(Model model) {
		try {
			List<UserDetails> userDetails = loginService.fetchUserDetails(1);
			System.out.println(userDetails);
			model.addAttribute("userDetails",userDetails);
			return "superadmin_user_details";
		}catch(Exception e) {
			e.printStackTrace();
			return "superadmin_user_details";
		}
	}
	
	@RequestMapping(value = "/superadmin_agent_details", method = RequestMethod.GET)
	public String superadmin_agent_details(Model model) {
		try {
			List<UserDetails> userDetails = loginService.fetchUserDetails(5);
			System.out.println(userDetails);
			model.addAttribute("userDetails",userDetails);
			return "superadmin_agent_details";
		}catch(Exception e) {
			e.printStackTrace();
			return "superadmin_agent_details";
		}
	}	
	
	@RequestMapping(value = "/superadmin_branch_Details", method = RequestMethod.GET)
	public String superadmin_branch_Details(Model model) {
		List<BranchDto> branchDtoList = new ArrayList<BranchDto>();
		try {
			branchDtoList = loginService.fetchBranchDetails();
			model.addAttribute("branchDtoList", branchDtoList);
			return "branch_details";
		}catch(Exception e) {
			e.printStackTrace();
			return "branch_details";
		}
	}
	
	@RequestMapping(value = "/superadmin_manager_details", method = RequestMethod.GET)
	public String superadmin_manager_details(Model model) {
		System.out.println("inside super Admin Manager Details");
		try {
			List<UserDetails> userDetails = loginService.fetchUserDetails(3);
			System.out.println(userDetails.size()+"dsssddddddddddddd");
			model.addAttribute("userDetails",userDetails);
			return "superadmin_manager_details";
		}catch(Exception e) {
			e.printStackTrace();
			return "superadmin_manager_details";
		}
	}	
	
	@RequestMapping(value = "/superadmin_staff_details", method = RequestMethod.GET)
	public String superadmin_staff_details(Model model) {
		try {
			List<UserDetails> userDetails = loginService.fetchUserDetails(4);
			System.out.println(userDetails);
			model.addAttribute("userDetails",userDetails);
			return "superadmin_staff_details";
		}catch(Exception e) {
			e.printStackTrace();
			return "superadmin_staff_details";
		}
	}
	
	@RequestMapping(value = "/feedback", method = RequestMethod.GET)
	public String feedback(Model model) {
		List<FeedbackDto> feedbackDto = new ArrayList<FeedbackDto>();
		try {
			feedbackDto = loginService.fetchFeedbacks();
			model.addAttribute("feedbackDto", feedbackDto);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return "feedback";
	}
//	service_types
//	add_blog
//	blogs
	
	@RequestMapping(value = "/service_types", method = RequestMethod.GET)
	public String service_types() {
		try {
			return "service_types";
		}catch(Exception e) {
			e.printStackTrace();
			return "service_types";
		}
	}
	
	@RequestMapping(value = "/add_blog", method = RequestMethod.GET)
	public String add_blog() {
		try {
			return "add_blog";
		}catch(Exception e) {
			e.printStackTrace();
			return "add_blog";
		}
		
	}
	
	@RequestMapping(value = "/blogs", method = RequestMethod.GET)
	public String blogs() {
		try {
			return "blogs";
		}catch(Exception e) {
			e.printStackTrace();
			return "blogs";
		}
	}
	
	@RequestMapping(value = "/aboutUsDetails", method = RequestMethod.GET)
	public String aboutUsDetails(Model model) {
		AboutUsDto aboutUs = new AboutUsDto();
		try {
			aboutUs = loginService.fetchAboutUs();
			model.addAttribute("aboutUs",aboutUs);
			return "aboutus";
		}catch(Exception e) {
			e.printStackTrace();
			return "aboutus";
		}
	}
	
	@RequestMapping(value = "/testimonials", method = RequestMethod.GET)
	public String testimonails(Model model) {
		List<TestimonialsDto> testimonialsDtoList = new ArrayList<TestimonialsDto>();
		try {
			testimonialsDtoList = loginService.fetchTestimonialsDto();
			model.addAttribute("testimonialsDtoList",testimonialsDtoList);
			return "testimonials";
		}catch(Exception e) {
			e.printStackTrace();
			return "testimonials";
		}
	}
	
	@RequestMapping(value = "/tags", method = RequestMethod.GET)
	public String tags(Model model) {
		List<PropertyTagDto> propertyTagDto = new ArrayList<PropertyTagDto>();
		try {
			propertyTagDto = loginService.fetchPropertyTags();
			model.addAttribute("propertyTagDto", propertyTagDto);
			System.out.println(propertyTagDto);
			return "tags";
		}catch(Exception e) {
			e.printStackTrace();
			return "tags";
		}
	}
	
	@RequestMapping(value = "/pdfreport1/{orderId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
	public ResponseEntity<InputStreamResource> citiesReport(@PathVariable int orderId) throws IOException {
		System.out.println(orderId);
			ByteArrayInputStream bis = GeneratePdfReport1.citiesReport();
			HttpHeaders headers = new HttpHeaders();
			headers.add("Content-Disposition", "inline; filename=HARA.pdf");
			return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF)
					.body(new InputStreamResource(bis));
		//ByteArrayInputStream bis = service.generatePdfReport(orderId);
	}	
	
	
}
