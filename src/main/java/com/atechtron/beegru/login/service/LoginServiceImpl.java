package com.atechtron.beegru.login.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.atechtron.beegru.auth.service.UserService;
import com.atechtron.beegru.emailAndSms.EmailTemplates;
import com.atechtron.beegru.emailAndSms.SmsTemplates;
import com.atechtron.beegru.entitytodtoconverter.DtoConverter;
import com.atechtron.beegru.login.dto.BeegruScoreDto;
import com.atechtron.beegru.login.dto.BranchDto;
import com.atechtron.beegru.login.dto.FeedbackDto;
import com.atechtron.beegru.login.dto.ManagerDto;
import com.atechtron.beegru.login.dto.PostYourRequirementDto;
import com.atechtron.beegru.login.dto.PropertiesDto;
import com.atechtron.beegru.login.dto.PropertyAssignedDto;
import com.atechtron.beegru.login.dto.PropertyEnquiryDto;
import com.atechtron.beegru.login.dto.PropertyImageDto;
import com.atechtron.beegru.login.dto.PropertyInfoDto;
import com.atechtron.beegru.login.dto.PropertyTagDto;
import com.atechtron.beegru.login.dto.StaffDto;
import com.atechtron.beegru.login.dto.UserDetails;
import com.atechtron.beegru.login.model.AboutUs;
import com.atechtron.beegru.login.model.AgentDetails;
import com.atechtron.beegru.login.model.BeegruScore;
import com.atechtron.beegru.login.model.BranchDetails;
import com.atechtron.beegru.login.model.ManagerDetails;
import com.atechtron.beegru.login.model.PeopleTestimonials;
import com.atechtron.beegru.login.model.PostYourRequirement;
import com.atechtron.beegru.login.model.Properties;
import com.atechtron.beegru.login.model.PropertyAssignDetails;
import com.atechtron.beegru.login.model.PropertyTags;
import com.atechtron.beegru.login.model.PropertyVicinity;
import com.atechtron.beegru.login.model.ServiceDetails;
import com.atechtron.beegru.login.model.Services;
import com.atechtron.beegru.login.model.StaffDetails;
import com.atechtron.beegru.login.model.WishList;
import com.atechtron.beegru.repository.AboutUsRepository;
import com.atechtron.beegru.repository.AgentDetailsRepository;
import com.atechtron.beegru.repository.BeegruScoreRepository;
import com.atechtron.beegru.repository.BranchRepository;
import com.atechtron.beegru.repository.ContactUsRepository;
import com.atechtron.beegru.repository.ManagerDetailsRepository;
//import com.atechtron.beegru.repository.EmployeeDetailsRepository;
import com.atechtron.beegru.repository.PeopleTestimonialsRepository;
import com.atechtron.beegru.repository.PostYourRequirementRepository;
import com.atechtron.beegru.repository.PropertyAssignDetailsRepository;
import com.atechtron.beegru.repository.PropertyEnquiryRepository;
import com.atechtron.beegru.repository.PropertyRepository;
import com.atechtron.beegru.repository.PropertyTagsRepository;
import com.atechtron.beegru.repository.ServiceDetailsRepository;
import com.atechtron.beegru.repository.ServicesRepository;
import com.atechtron.beegru.repository.StaffRepository;
import com.atechtron.beegru.repository.UserRepository;
import com.atechtron.beegru.repository.WishListRepository;
import com.atechtron.beegru.util.Util;
import com.atechtron.beegru.website.dto.AboutUsDto;
import com.atechtron.beegru.website.dto.LocationDto;
import com.atechtron.beegru.website.dto.ResponseDto;
import com.atechtron.beegru.website.dto.TestimonialsDto;
import com.atechtron.beegru.website.model.User;

@Service
public class LoginServiceImpl implements LoginService {
	
	@Autowired
	private UserService userService;

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private PropertyRepository propertyRepository;

	@Autowired
	private ContactUsRepository contactUsRepository;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ServicesRepository servicesRepository;
	
	private ServiceDetailsRepository serviceDetailsReposotory;
	
	@Autowired
	private PostYourRequirementRepository postYourRequirementRepository;
	
	@Autowired
	private AboutUsRepository aboutUsRepository;
	
	@Autowired
	private PeopleTestimonialsRepository peopleTestimonialRepository;
	
	@Autowired
	private WishListRepository wishListRepository;
	
	@Autowired
	private PropertyTagsRepository propertyTagsRepository;
	
	@Autowired 
	private EmailTemplates emailTemplates;
	
	@Autowired
	private SmsTemplates smsTemplates;
	
	@Autowired
	private BranchRepository branchDetailsRepository;
	
	@Autowired
	private ManagerDetailsRepository managerDetailsRepository;
	
	@Autowired
	private StaffRepository staffRepository;
	
	@Autowired
	private PropertyAssignDetailsRepository propertyAssignDetailsRepository;
	
	@Autowired
	private AgentDetailsRepository agentDetailsRepository;
	//@Autowired
	//private EmployeeDetailsRepository employeeDetailsRepository;

	@Autowired
	private BeegruScoreRepository beegruScoreRepository;
	
	@Autowired
	private PropertyEnquiryRepository propertyEnquiryRepository;
	
	@Override
	public ResponseDto addProperty(Properties property) {
		ResponseDto response = new ResponseDto();
		try {
			propertyRepository.save(property);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public List<FeedbackDto> fetchFeedbacks() {
		List<FeedbackDto> feedbackDto = new ArrayList<FeedbackDto>();
		try {
			feedbackDto = contactUsRepository.fetchfeedbackDetails();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return feedbackDto;
	}

	@Override
	public ResponseDto updateContactUsStatus(String jsonString) {
		ResponseDto response = new ResponseDto();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			int result = contactUsRepository.updateContactUsStatus(Integer.valueOf(jsonObj.getString("id")),
					Integer.valueOf(jsonObj.getString("status")));
			if (result > 1) {
				response.setResultStatus(1);
			} else if (result < 1) {
				response.setResultStatus(0);
			}
			response.setHttpStatus(HttpServletResponse.SC_OK);
			return response;
		} catch (Exception e) {
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
			return response;
		}
	}

	@Override
	public ResponseDto saveProperties(Properties properties) {
		System.out.println("inside save properties");
		ResponseDto response = new ResponseDto();
		String displayPrice = "";
		List<PropertyVicinity> propertyVicinityList = new ArrayList<PropertyVicinity>();
		double price = 0.0;
		try {
			if(properties.getMinPrice()>10000000.00) {
				price = properties.getMinPrice()/10000000;
				displayPrice = price+" cr";
			}
			if(properties.getMaxPrice()>10000000.00) {
				System.out.println(properties.getMaxPrice()+" get max price");
				price = properties.getMaxPrice()/10000000;
				displayPrice = displayPrice +"-"+ price +" cr";
			}else {
				if(properties.getMaxPrice()!=0.0) {
					displayPrice = properties.getMinPrice()+"-"+properties.getMaxPrice();
				}else {
					displayPrice = properties.getMinPrice()+"";
				}
			}
			properties.setDisplayPrice(displayPrice);
			for(PropertyVicinity obj : properties.getPropertyVicinity()) {
				PropertyVicinity propertyVicinityObj = obj;
				propertyVicinityObj.setProperties(properties);
				propertyVicinityList.add(propertyVicinityObj);
			}
			
			properties.setActiveStatus(2);
			properties.setPropertyVicinity(propertyVicinityList);
			System.out.println("after add PropertyVicinity");
			System.out.println(properties.getPropertyVicinity());
			Properties propertiesResponse = propertyRepository.save(properties);
			if(propertiesResponse!=null) {
				response.setResultStatus(1);
				response.setPropertyId(propertiesResponse.getPropertyId());
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}else {
				response.setResultStatus(2);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}
			return response;
		} catch (Exception e) {
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
			return response;
		}
	}

	@Override
	public int findUserIdByMobileNumber(String mobileNumber) {
		int userId = 0;
		try {
			userId = userRepository.findUserIdByMobileNumber(mobileNumber);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userId;
	}

	@Override
	public void savePropertiesImages(String jsonString) {
		System.out.println("inside saveproperties Images");
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			for(int i = 0;i<jsonObj.getJSONArray("base64Array").length();i++) {
				System.out.println("inside image upload");
				int retval = uploadPropertiesImages(jsonObj.getJSONArray("base64Array").getString(i), "image"+(i+1)+".png", jsonObj.getString("type"), jsonObj.getInt("propertyId"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int uploadPropertiesImages(String base64String, String fileName, String type, int propertyId) throws IOException {
		BufferedOutputStream scanStream = null;
		Date date = new Date();
		System.out.println(date.toString());
		try {
			Base64.Decoder decoder = Base64.getDecoder();
			byte[] imageByte = decoder.decode(base64String.split("\\,")[1]);
			System.out.println(imageByte);
			String rootPath = System.getProperty("user.home");
			System.out.println(rootPath);
			File fileSaveDir = new File(rootPath + File.separator + "public_html" + File.separator + "assets"
					+ File.separator + "beegru" + File.separator + type + File.separator + String.valueOf(propertyId));
			// Creates the save directory if it does not existsSS
			if (!fileSaveDir.exists()) {
				fileSaveDir.mkdirs();
				// Files.createDirectories(fileSaveDir);
			}
			File scanFile = new File(fileSaveDir.getAbsolutePath() + File.separator + fileName);
			scanStream = new BufferedOutputStream(new FileOutputStream(scanFile));
			scanStream.write(imageByte);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		} finally {
			scanStream.close();
		}
	}
	

	@Override
	public List<PropertyInfoDto> fetchPropertyInfo() {
		List<PropertyInfoDto> propertyInfoList = new ArrayList<PropertyInfoDto>();
		try {
			List<Properties> properties = propertyRepository.fetchProperties();
			propertyInfoList = DtoConverter.fetchPropertyInfo(properties);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyInfoList;
	}

	@Override
	public PropertiesDto fetchSingleProperty(String jsonString,int status) {
		PropertiesDto propertiesDto = new PropertiesDto();
		User user = new User();
		try {
			System.out.println(jsonString);
			JSONObject jsonObj = new JSONObject(jsonString);
			Properties property = propertyRepository.findById(jsonObj.getInt("propertyId")).get();
			if(status == 2) {
				user = userRepository.findByMobileNumber(jsonObj.getString("mobileNumber"));
				propertiesDto = DtoConverter.propertiesDto(property,user);
			}else {
				propertiesDto = DtoConverter.propertiesDto(property);
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertiesDto;
	}

	@Override
	public User findByMobileNumber(String name) {
		User user = new User();
		try {
			user = userRepository.findByMobileNumber(name);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public ResponseDto savePostYourRequirement(PostYourRequirement postYourRequirement) {
		ResponseDto response = new ResponseDto();
		try {
			if(postYourRequirementRepository.save(postYourRequirement)!=null) {
				response.setResultStatus(1);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}else {
				response.setResultStatus(2);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}
		}catch(Exception e) {
			e.printStackTrace();
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	@Override
	public ResponseDto addServices(Services services) {
		ResponseDto response = new ResponseDto();
		try {
				if(servicesRepository.save(services)!=null) {
					response.setResultStatus(1);
					response.setHttpStatus(HttpServletResponse.SC_OK);
				}else {
					response.setResultStatus(2);
					response.setHttpStatus(HttpServletResponse.SC_OK);
				}
		}catch(Exception e) {
			e.printStackTrace();
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	@Override
	public ResponseDto saveServiceDetails(ServiceDetails serviceDetails) {
		ResponseDto response = new ResponseDto();
		try {
			if(serviceDetailsReposotory.save(serviceDetails)!=null) {
				response.setResultStatus(1);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}else {
				response.setResultStatus(2);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}
		}catch(Exception e) {
			e.printStackTrace();
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	@Override
	public ResponseDto saveAboutUsDetails(AboutUs aboutUs) {
		ResponseDto response = new ResponseDto();
		try {
			if(aboutUsRepository.save(aboutUs)!=null){
				response.setResultStatus(1);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}else {
				response.setResultStatus(1);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}
		}catch(Exception e) {
			e.printStackTrace();
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	@Override
	public ResponseDto saveTestimonials(String jsonString,int userId) {
		ResponseDto response = new ResponseDto();
		PeopleTestimonials peopleTestimonial = new PeopleTestimonials();
		Properties properties = new Properties();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			properties = propertyRepository.fetchById(jsonObj.getInt("propertyId"));
			if(properties == null) {
				response.setResultStatus(3); // invalid ProductId
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}else {
				//properties.setPropertyId(jsonObj.getInt("propertyId"));
				//peopleTestimonial.setAddedUserMobileNumber(mobileNumber);
				peopleTestimonial.setAddedUserId(userId);
				peopleTestimonial.setDate(jsonObj.getString("date"));
				peopleTestimonial.setDescription(jsonObj.getString("description"));
				peopleTestimonial.setProperties(properties);
				peopleTestimonial.setRatings(jsonObj.getInt("ratings"));
				peopleTestimonial.setSubject(jsonObj.getString("subject"));
				peopleTestimonial.setTestimonialAddress(jsonObj.getString("testimonialAddress"));
				peopleTestimonial.setTestimonialName(jsonObj.getString("testimonialName"));
				peopleTestimonial.setVideoLink(jsonObj.getString("videoLink"));
				peopleTestimonial = peopleTestimonialRepository.save(peopleTestimonial); 
				if(peopleTestimonial!=null){
					response.setResultStatus(1);
					response.setHttpStatus(HttpServletResponse.SC_OK);
					if(!jsonObj.getString("addBase64Data").equals("")) {
					//	int retVal = uploadImages(jsonObj.getString("addBase64Data"), peopleTestimonial.getPeopleTestimonialsId()+".png", "testimonialImages");
						int retval = uploadPropertiesImages(jsonObj.getString("addBase64Data"), "image1.png", "testimonials", peopleTestimonial.getPeopleTestimonialsId());
					}
			}else {
				response.setResultStatus(2); // not saved
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}
			}
		}catch(Exception e) {
			e.printStackTrace();
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		return response;
	}
	
	@Override
	public ResponseDto editTestimonials(String jsonString, int userId) {
		ResponseDto response = new ResponseDto();
		PeopleTestimonials peopleTestimonial = new PeopleTestimonials();
		Properties properties = new Properties();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			properties = propertyRepository.fetchById(jsonObj.getInt("propertyId"));
			if(properties == null) {
				response.setResultStatus(3); // invalid ProductId
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}else {
				peopleTestimonial.setPeopleTestimonialsId(jsonObj.getInt("testimonialId"));
				//peopleTestimonial.setAddedUserMobileNumber(mobileNumber);
				peopleTestimonial.setAddedUserId(userId);
				peopleTestimonial.setDate(jsonObj.getString("date"));
				peopleTestimonial.setDescription(jsonObj.getString("description"));
				peopleTestimonial.setProperties(properties);
				peopleTestimonial.setRatings(jsonObj.getInt("ratings"));
				peopleTestimonial.setSubject(jsonObj.getString("subject"));
				peopleTestimonial.setTestimonialAddress(jsonObj.getString("testimonialAddress"));
				peopleTestimonial.setTestimonialName(jsonObj.getString("testimonialName"));
				peopleTestimonial.setVideoLink(jsonObj.getString("videoLink"));
				peopleTestimonial = peopleTestimonialRepository.save(peopleTestimonial); 
				if(peopleTestimonial!=null){
					response.setResultStatus(1);
					response.setHttpStatus(HttpServletResponse.SC_OK);
					if(!jsonObj.getString("addBase64Data").equals("")) {
					//	int retVal = uploadImages(jsonObj.getString("addBase64Data"), peopleTestimonial.getPeopleTestimonialsId()+".png", "testimonialImages");
						int retval = uploadPropertiesImages(jsonObj.getString("addBase64Data"), "image1.png", "testimonials", peopleTestimonial.getPeopleTestimonialsId());
					}
					
			}else {
				response.setResultStatus(2); // not saved
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}
			}
		}catch(Exception e) {
			e.printStackTrace();
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		return response;
	}
	
	@Override
	public ResponseDto deleteTestimonialById(String jsonString) {
		ResponseDto response = new ResponseDto();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			int retVal = peopleTestimonialRepository.deleteTestimonailsById(jsonObj.getInt("testimonialId"));
			if(retVal>0) {
				response.setResultStatus(1);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}else {
			response.setResultStatus(2);
			response.setHttpStatus(HttpServletResponse.SC_OK);
		}
		}catch(Exception e) {
			e.printStackTrace();
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		return response;
	}


	@Override
	public ResponseDto updatePropertyStatus(String jsonString) {
		ResponseDto response = new ResponseDto();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			int retVal = propertyRepository.updatePropertyStatus(jsonObj.getInt("propertyId"),jsonObj.getInt("status"));
		}catch(Exception e) {
			e.printStackTrace();
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	@Override
	public ResponseDto addToWishlist(String jsonString, String mobileNumber) {
		ResponseDto response = new ResponseDto();
		WishList wishList = new WishList();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			int userId = findUserIdByMobileNumber(mobileNumber);
			List<Integer> wishListIds = wishListRepository.findwishListIfExists(jsonObj.getInt("propertyId"),userId);
			if(wishListIds.size()==0) {
				User user = new User();
				Properties properties = new Properties();
				user.setUserId(userId);
				properties.setPropertyId(jsonObj.getInt("propertyId"));
				wishList.setUser(user);
				wishList.setProperties(properties);
				if(wishListRepository.save(wishList)!=null) {
					response.setResultStatus(1); //saved successfully
					response.setHttpStatus(HttpServletResponse.SC_OK);
				}else{
					response.setResultStatus(2); //no saved
					response.setHttpStatus(HttpServletResponse.SC_OK);
				}
			}else {
				wishListRepository.deleteById(wishListIds.get(0));
				response.setResultStatus(3); //deleted successfullly
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}
		}catch(Exception e) {
			e.printStackTrace();
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	@Override
	public List<PropertyInfoDto> fetchWishList(String mobileNumber) {
		List<PropertyInfoDto> propertyInfoList = new ArrayList<PropertyInfoDto>();
		try {
			User user = userRepository.findByMobileNumber(mobileNumber);
			propertyInfoList = DtoConverter.getWishList(user);
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyInfoList;
	}

	@Override
	public List<PropertyInfoDto> fetchSearchedProperties(String jsonString) {
		List<PropertyInfoDto> propertyInfoList = new ArrayList<PropertyInfoDto>();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			propertyInfoList = propertyRepository.searchProducts(jsonObj.getString("propertyPlace"), jsonObj.getString("propertyStatus"));
			//propertyInfoList = DtoConverter.fetchPropertyInfo(properties);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyInfoList;
		
	}

	@Override
	public ResponseDto updateFeaturedPropertyStatus(String jsonString) {
		ResponseDto response = new ResponseDto();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			int retVal = propertyRepository.updateFeaturedPropertyStatus(jsonObj.getInt("propertyId"),jsonObj.getInt("status"));
			if(retVal>0) {
				response.setResultStatus(1); 
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}else {
				response.setResultStatus(2); 
				response.setHttpStatus(HttpServletResponse.SC_OK);	
			}
		}catch(Exception e) {
			e.printStackTrace();
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	@Override
	public List<UserDetails> fetchUserDetails(int role) {
		List<UserDetails> userDetailsList = new ArrayList<UserDetails>();
		List<User> usersList = new ArrayList<User>();
		try {
			if(role == 1) { 
				usersList = userRepository.findAllUser();
			}else if(role == 3) {
				usersList = userRepository.findManagerDetails();
			}else if(role == 4) {
				usersList = userRepository.findStaffDetails();
			}else if(role == 5) {
				usersList = userRepository.findAgentDetails();
			}
			System.out.println(usersList.size()+"mmmmmmmmmmmmmmmmmm");
			userDetailsList = DtoConverter.getUserDetails(usersList,role);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return userDetailsList;
	}

	@Override
	public AboutUsDto fetchAboutUs() {
		AboutUsDto aboutUs = new AboutUsDto();
		try {
			Mapper mapper = new DozerBeanMapper();
			List<AboutUs> aboutUsDetails = aboutUsRepository.findAll();
			aboutUs = mapper.map(aboutUsDetails.get(0), aboutUs.getClass());
		}catch(Exception e) {
			e.printStackTrace();
		}
		return aboutUs;
	}

	@Override
	public List<TestimonialsDto> fetchTestimonialsDto() {
		List<TestimonialsDto> testimonialsDtoList = new ArrayList<TestimonialsDto>();
		try {
			List<PeopleTestimonials> peopleTestimonials = peopleTestimonialRepository.fetchTestimonials();
			testimonialsDtoList = DtoConverter.gettestimonialList(peopleTestimonials);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return testimonialsDtoList;
	}

	@Override
	public ResponseDto saveTags(String jsonString) {
		ResponseDto response = new ResponseDto();
		PropertyTags propertyTags = new PropertyTags();
		Properties property = new Properties();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			property = propertyRepository.fetchById(jsonObj.getInt("propertyId"));
			if(property == null) {
				response.setResultStatus(1); // invalid propertyId
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}else {
				List<PropertyTags> propertyTagId = propertyTagsRepository.fetchTags(jsonObj.getInt("position"),jsonObj.getString("tagName"),jsonObj.getInt("propertyId"));
				if(propertyTagId.size()==0) {
						propertyTags.setPosition(jsonObj.getInt("position"));
					//	property.setPropertyId(jsonObj.getInt("propertyId"));
						propertyTags.setProperties(property);
						propertyTags.setActiveStatus(1);
						propertyTags.setTagName(jsonObj.getString("tagName"));
						if(propertyTagsRepository.save(propertyTags)!=null) {
							response.setResultStatus(2); // saved successfully
							response.setHttpStatus(HttpServletResponse.SC_OK);
						}else {
							response.setResultStatus(3); // not saved
							response.setHttpStatus(HttpServletResponse.SC_OK);
						}
				}else {
					response.setResultStatus(4); // already exist at same position same proprty with same tag
					response.setHttpStatus(HttpServletResponse.SC_OK);
				}
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	@Override
	public PropertyInfoDto fetchPropertyName(String jsonString) {
		String propertyName = "";
		PropertyInfoDto propertyInfoDto = new PropertyInfoDto();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			propertyInfoDto = propertyRepository.fetchPropertyInfoById(jsonObj.getInt("propertyId"));
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyInfoDto;
	}

	@Override
	public List<PropertyTagDto> fetchPropertyTags() {
		List<PropertyTagDto> propertyTagDto = new ArrayList<PropertyTagDto>();
		try {
			List<PropertyTags> propertyTagsList = propertyTagsRepository.fetchTags();  
			propertyTagDto = DtoConverter.fetchPropertyTagsInfo(propertyTagsList);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyTagDto;
	}

	@Override
	public PropertyTagDto fetchPropertyTagDto(String jsonString) {
		PropertyTagDto propertyTagDto = new PropertyTagDto();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			PropertyTags propertyTags = propertyTagsRepository.findById(jsonObj.getInt("propertyTagId")).get();
			propertyTagDto = DtoConverter.getPropertyTagDto(propertyTags);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyTagDto;
	}

	@Override
	public ResponseDto deleteTags(String jsonString) {
		ResponseDto response = new ResponseDto();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			int retVal = propertyTagsRepository.deleteTag(2,jsonObj.getInt("propertyTagsId"));
			if(retVal>0) {
				response.setResultStatus(1);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}else {
				response.setResultStatus(2);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}
		}catch(Exception e) {
			e.printStackTrace();
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	@Override
	public ResponseDto editTags(String jsonString) {
		ResponseDto response = new ResponseDto();
		PropertyTags propertyTags = new PropertyTags();
		Properties property = new Properties();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			property = propertyRepository.fetchById(jsonObj.getInt("propertyId"));
			if(property == null) {
				response.setResultStatus(1); // invalid propertyId
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}else {
				List<PropertyTags> propertyTagId = propertyTagsRepository.fetchTags(jsonObj.getInt("position"),jsonObj.getString("tagName"),jsonObj.getInt("propertyId"));
				if(propertyTagId.size()==0) {
					//List<PropertyTags> propertyTagId1 = propertyTagsRepository.fetchTags(jsonObj.getInt("position"),jsonObj.getString("tagName"));
					//if(propertyTagId1.size()>0) {
					//	response.setResultStatus(5); // this position is already occupied by other property
					//}else {
					
						propertyTags.setPropertyTagsId(jsonObj.getInt("propertyTagsId"));
						propertyTags.setActiveStatus(1);
						propertyTags.setPosition(jsonObj.getInt("position"));
						propertyTags.setProperties(property);
						propertyTags.setActiveStatus(1);
						propertyTags.setTagName(jsonObj.getString("tagName"));
						if(propertyTagsRepository.save(propertyTags)!=null) {
							response.setResultStatus(2); // saved successfully
							response.setHttpStatus(HttpServletResponse.SC_OK);
						}else {
							response.setResultStatus(3); // not saved
							response.setHttpStatus(HttpServletResponse.SC_OK);
						}
					//}
				}else {
					response.setResultStatus(4); // already exist at same position same proprty with same tag
					response.setHttpStatus(HttpServletResponse.SC_OK);
				}
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	@Override
	public ResponseDto saveUser(User user) {
		ResponseDto response = new ResponseDto();
		User finduser = new User();
		try {
			finduser = userRepository.findByEmailIdOrMobileNumber(user.getEmailId(), user.getMobileNumber());
			if (finduser == null) {
				user.setActiveStatus(3);
				user.setName(Util.nameFormat(user.getName()));
				userService.save(user);
				smsTemplates.registrationSMS(user.getName(), user.getMobileNumber(), user.getEmailId(), user.getPasswordConfirm(), "Customer");
				emailTemplates.registrationEmail(user.getName(), user.getMobileNumber(), user.getEmailId(),user.getPasswordConfirm(), "Customer");
				response.setResultStatus(1);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			} else {
				if(finduser.getMobileNumber().equals(user.getMobileNumber())) {
					response.setResultStatus(2); // mobile Number already exist
					response.setHttpStatus(HttpServletResponse.SC_OK);
				}else if(finduser.getEmailId().equals(user.getEmailId())){
					response.setResultStatus(3); // emailId already exist
					response.setHttpStatus(HttpServletResponse.SC_OK);
				}
			}
			return response;
		} catch (Exception e) {
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
			return response;
		}
	}

	@Override
	public ResponseDto updateUserStatus(String jsonString) {
		ResponseDto response = new ResponseDto();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			int retVal = userRepository.updateUserStatus(jsonObj.getInt("status"),jsonObj.getInt("userId"));
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public ResponseDto assignWorkingAreaToManager(String jsonString, String mobileNumber) {
		ResponseDto response = new ResponseDto();
		try {
			
		//	employeeDetailsRepository.save(entity)
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public ResponseDto saveBranch(String jsonString, String addedUserMobileNumber) {
		ResponseDto response = new ResponseDto();
		BranchDetails branchDetails = new BranchDetails();
		try {
			System.out.println(jsonString);
			JSONObject jsonObj = new JSONObject(jsonString);
			branchDetails.setBranchAddedBy(addedUserMobileNumber);
			branchDetails.setBranchName(jsonObj.getString("branchName"));
			branchDetails.setBranchPincode(jsonObj.getString("pincode"));
			branchDetails.setBranchStatus(1);
			if(branchDetailsRepository.save(branchDetails)!=null) {
				response.setResultStatus(1); // saved Successfully
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}else {
				response.setResultStatus(2); // not Saved
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}
		}catch(Exception e) {
			e.printStackTrace();
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	@Override
	public List<BranchDto> fetchBranchDetails() {
		List<BranchDto> branchDtoList = new ArrayList<BranchDto>();
		try {
			List<BranchDetails> branchDetailsList = branchDetailsRepository.fetchAllBranch();
			branchDtoList = DtoConverter.getBranchDetails(branchDetailsList);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return branchDtoList;
	}

	@Override
	public ResponseDto editBranch(String jsonString, String addedUserMobileNumber) {
		ResponseDto response = new ResponseDto();
		BranchDetails branchDetails = new BranchDetails();
		try {
			System.out.println(jsonString);
			JSONObject jsonObj = new JSONObject(jsonString);
			branchDetails.setBranchId(jsonObj.getInt("branchId"));
			branchDetails.setBranchAddedBy(addedUserMobileNumber);
			branchDetails.setBranchName(jsonObj.getString("branchName"));
			branchDetails.setBranchPincode(jsonObj.getString("pincode"));
			branchDetails.setBranchStatus(1);
			if(branchDetailsRepository.save(branchDetails)!=null) {
				response.setResultStatus(1); // saved Successfully
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}else {
				response.setResultStatus(2); // not Saved
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}
		}catch(Exception e) {
			e.printStackTrace();
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	@Override
	public BranchDto fetchBranchById(String jsonString) {
		BranchDto branchDto = new BranchDto();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			BranchDetails branchDetails = branchDetailsRepository.findById(jsonObj.getInt("branchId")).get();
			branchDto = DtoConverter.fetchBranchDto(branchDetails);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return branchDto;
	}

	@Override
	public ResponseDto saveManager(User user) {
		ResponseDto response = new ResponseDto();
		User finduser = new User();
		ManagerDetails managerDetails = new ManagerDetails();
		BranchDetails branchDetails = new BranchDetails();
		try {
			finduser = userRepository.findByEmailIdOrMobileNumber(user.getEmailId(), user.getMobileNumber());
			if (finduser == null) {
				user.setActiveStatus(1);
				user.setName(Util.nameFormat(user.getName()));
				branchDetails.setBranchId(user.getManagerDetails().getBranch().getBranchId());
				managerDetails.setUser(user);
				managerDetails.setBranch(branchDetails);
				user.setManagerDetails(managerDetails);
				user.setPasswordConfirm(user.getPassword());
				userService.save(user);
				smsTemplates.registrationSMS(user.getName(), user.getMobileNumber(), user.getEmailId(), user.getPasswordConfirm(), "Customer");
				emailTemplates.registrationEmail(user.getName(), user.getMobileNumber(), user.getEmailId(),user.getPasswordConfirm(), "Customer");
				response.setResultStatus(1);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			} else {
				if(finduser.getMobileNumber().equals(user.getMobileNumber())) {
					response.setResultStatus(2); // mobile Number already exist
					response.setHttpStatus(HttpServletResponse.SC_OK);
				}else if(finduser.getEmailId().equals(user.getEmailId())){
					response.setResultStatus(3); // emailId already exist
					response.setHttpStatus(HttpServletResponse.SC_OK);
				}
			}
			return response;
		} catch (Exception e) {
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
			return response;
		}
	}

	@Override
	public List<ManagerDto> fetchManagerDetails() {
		List<ManagerDto> managerDtoList = new ArrayList<ManagerDto>();
		try {
			List<ManagerDetails> managerList = managerDetailsRepository.findAll();
			managerDtoList = DtoConverter.fetchManagerDetails(managerList);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return managerDtoList;
	}

	@Override
	public ResponseDto saveStaff(User user) {
		ResponseDto response = new ResponseDto();
		User finduser = new User();
		StaffDetails staffDetails = new StaffDetails();
		ManagerDetails managerDetails = new ManagerDetails();
		try {
			finduser = userRepository.findByEmailIdOrMobileNumber(user.getEmailId(), user.getMobileNumber());
			if (finduser == null) {
				user.setActiveStatus(1);
				user.setName(Util.nameFormat(user.getName()));
				user.setPasswordConfirm(user.getPassword());
				staffDetails.setUser(user);
				managerDetails.setManagerDetailsId(user.getStaffDetails().getManagerDetails().getManagerDetailsId());
				staffDetails.setManagerDetails(managerDetails);
				System.out.println(staffDetails.getManagerDetails());
				user.setStaffDetails(staffDetails);
				userService.save(user);
				smsTemplates.registrationSMS(user.getName(), user.getMobileNumber(), user.getEmailId(), user.getPasswordConfirm(), "Customer");
				emailTemplates.registrationEmail(user.getName(), user.getMobileNumber(), user.getEmailId(),user.getPasswordConfirm(), "Customer");
				response.setResultStatus(1);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			} else {
				if(finduser.getMobileNumber().equals(user.getMobileNumber())) {
					response.setResultStatus(2); // mobile Number already exist
					response.setHttpStatus(HttpServletResponse.SC_OK);
				}else if(finduser.getEmailId().equals(user.getEmailId())){
					response.setResultStatus(3); // emailId already exist
					response.setHttpStatus(HttpServletResponse.SC_OK);
				}
			}
			return response;
		} catch (Exception e) {
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
			return response;
		}
	}
	
	@Override
	public ResponseDto editStaff(User user) {
		System.out.println(user);
		ResponseDto response = new ResponseDto();
		User finduser = new User();
		//StaffDetails staffDetails = new StaffDetails();
		//ManagerDetails managerDetails = new ManagerDetails();
		try {
			finduser = userRepository.findByEmailIdOrMobileNumber(user.getEmailId(), user.getMobileNumber(),user.getUserId());
			if (finduser == null) {
				user.setActiveStatus(1);
				user.setName(Util.nameFormat(user.getName()));
				user.getStaffDetails().setUser(user);
				user.setPasswordConfirm(user.getPassword());
				//user.setStaffDetails.setUser(user);
				//managerDetails.setManagerDetailsId(user.getStaffDetails().getManagerDetails().getManagerDetailsId());
				//staffDetails.setStaffId(user.getStaffDetails().getStaffId());
				//staffDetails.setManagerDetails(managerDetails);
				//user.setStaffDetails(staffDetails);
				userService.save(user);
			//	smsTemplates.registrationSMS(user.getName(), user.getMobileNumber(), user.getEmailId(), user.getPasswordConfirm(), "Customer");
			//	emailTemplates.registrationEmail(user.getName(), user.getMobileNumber(), user.getEmailId(),user.getPasswordConfirm(), "Customer");
				response.setResultStatus(1);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			} else {
				if(finduser.getMobileNumber().equals(user.getMobileNumber())) {
					response.setResultStatus(2); // mobile Number already exist
					response.setHttpStatus(HttpServletResponse.SC_OK);
				}else if(finduser.getEmailId().equals(user.getEmailId())){
					response.setResultStatus(3); // emailId already exist
					response.setHttpStatus(HttpServletResponse.SC_OK);
				}
			}
			return response;
		} catch (Exception e) {
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
			return response;
		}
	}
	
	@Override
	public ResponseDto saveAgent(User user) {
		ResponseDto response = new ResponseDto();
		User finduser = new User();
		try {
			finduser = userRepository.findByEmailIdOrMobileNumber(user.getEmailId(), user.getMobileNumber());
			if (finduser == null) {
				//user.setActiveStatus(1);
				user.setName(Util.nameFormat(user.getName()));
				user.getAgentDetails().setUser(user);
				userService.save(user);
				smsTemplates.registrationSMS(user.getName(), user.getMobileNumber(), user.getEmailId(), user.getPasswordConfirm(), "Customer");
				emailTemplates.registrationEmail(user.getName(), user.getMobileNumber(), user.getEmailId(),user.getPasswordConfirm(), "Customer");
				response.setResultStatus(1);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			} else {
				if(finduser.getMobileNumber().equals(user.getMobileNumber())) {
					response.setResultStatus(2); // mobile Number already exist
					response.setHttpStatus(HttpServletResponse.SC_OK);
				}else if(finduser.getEmailId().equals(user.getEmailId())){
					response.setResultStatus(3); // emailId already exist
					response.setHttpStatus(HttpServletResponse.SC_OK);
				}
			}
			return response;
		} catch (Exception e) {
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
			return response;
		}
	}
	
	@Override
	public ResponseDto editAgent(User user) {
		System.out.println(user);
		ResponseDto response = new ResponseDto();
		User finduser = new User();
	//	AgentDetails agentDetails = new AgentDetails();
	//	StaffDetails staffDetails = new StaffDetails();
		try {
			finduser = userRepository.findByEmailIdOrMobileNumber(user.getEmailId(), user.getMobileNumber(),user.getUserId());
			if (finduser == null) {
				user.setActiveStatus(1);
				user.setPasswordConfirm(user.getPassword());
				//user.getStaffDetails().setUser(user);
				user.setName(Util.nameFormat(user.getName()));
				//staffDetails.setStaffId(user.getAgentDetails().getStaffDetails().getStaffId());
			//	agentDetails.setAgentDetailsId(user.getAgentDetails().getAgentDetailsId());
				user.getAgentDetails().setUser(user);
			//	agentDetails.setStaffDetails(staffDetails);
			//	user.setAgentDetails(agentDetails);
				userService.save(user);
			//	smsTemplates.registrationSMS(user.getName(), user.getMobileNumber(), user.getEmailId(), user.getPasswordConfirm(), "Customer");
			//	emailTemplates.registrationEmail(user.getName(), user.getMobileNumber(), user.getEmailId(),user.getPasswordConfirm(), "Customer");
				response.setResultStatus(1);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			} else {
				if(finduser.getMobileNumber().equals(user.getMobileNumber())) {
					response.setResultStatus(2); // mobile Number already exist
					response.setHttpStatus(HttpServletResponse.SC_OK);
				}else if(finduser.getEmailId().equals(user.getEmailId())){
					response.setResultStatus(3); // emailId already exist
					response.setHttpStatus(HttpServletResponse.SC_OK);
				}
			}
			return response;
		} catch (Exception e) {
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
			return response;
		}
	}
	
	@Override
	public List<StaffDto> fetchStaff() {
		List<StaffDto> staffDtoList = new ArrayList<StaffDto>();
		try {
			List<StaffDetails> staffList = staffRepository.findAll();
			staffDtoList = DtoConverter.fetchStaffList(staffList);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return staffDtoList;
	}

	@Override
	public StaffDto fetchStaff(String jsonString) {
		StaffDto staffDto = new StaffDto();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			StaffDetails staffDetailsList = staffRepository.findById(jsonObj.getInt("staffId")).get();
			staffDto = DtoConverter.getStaffDto(staffDetailsList);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return staffDto;
	}

	@Override
	public ResponseDto assignProperty(String jsonString, String mobileNumber) {
		ResponseDto response = new ResponseDto();
		Properties properties = new Properties();
		StaffDetails staffDetails = new StaffDetails();
		PropertyAssignDetails propertyAssignDetails = new PropertyAssignDetails();
		
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			int userId = userRepository.findUserIdByMobileNumber(mobileNumber);
			User user = new User();
			user.setUserId(userId);
			propertyAssignDetails.setUser(user);
			properties.setPropertyId(jsonObj.getInt("propertyId"));
			staffDetails.setStaffId(jsonObj.getInt("staffId"));
			propertyAssignDetails.setProperties(properties);
			propertyAssignDetails.setStaffDetails(staffDetails);
			propertyAssignDetails.setPropertyAssignedStatus(1);
		//	propertyAssignDetails.setAddedUserMobileNumber(mobileNumber);
			if(propertyAssignDetailsRepository.save(propertyAssignDetails)==null) {
				response.setResultStatus(1);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}else {
				response.setResultStatus(2);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}
		}catch(Exception e) {
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public ResponseDto editAssignProperty(String jsonString, String mobileNumber) {
		ResponseDto response = new ResponseDto();
		Properties properties = new Properties();
		StaffDetails staffDetails = new StaffDetails();
		PropertyAssignDetails propertyAssignDetails = new PropertyAssignDetails();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			User user = new User();
			int userId = userRepository.findUserIdByMobileNumber(mobileNumber);
			user.setUserId(userId);
			propertyAssignDetails.setUser(user);
			properties.setPropertyId(jsonObj.getInt("propertyId"));
			staffDetails.setStaffId(jsonObj.getInt("staffId"));
			propertyAssignDetails.setPropertyAssignedId(jsonObj.getInt("propertyAssignedId"));
			propertyAssignDetails.setProperties(properties);
			propertyAssignDetails.setStaffDetails(staffDetails);
			propertyAssignDetails.setPropertyAssignedStatus(1);
			if(propertyAssignDetailsRepository.save(propertyAssignDetails)==null) {
				response.setResultStatus(1);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}else {
				response.setResultStatus(2);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}
		}catch(Exception e) {
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
		}
		return response;	
		}

	@Override
	public PropertyAssignedDto fetchPropertyDetails(String jsonString) {
		PropertyAssignedDto propertyAssignedDto = new PropertyAssignedDto();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			PropertyAssignDetails proepertyAssignedDetails = propertyAssignDetailsRepository.findById(jsonObj.getInt("propertyAssignedId")).get();
			propertyAssignedDto = DtoConverter.getPropertyAssignedDetails(proepertyAssignedDetails);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyAssignedDto;
	}

	@Override
	public List<PropertyAssignedDto> fetchPropertyAssignedDetails(String jsonString) {
		List<PropertyAssignedDto> propertyAssignedDto = new ArrayList<PropertyAssignedDto>();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			List<PropertyAssignDetails> propertyAssignDetails = propertyAssignDetailsRepository.fetchPropertyAssignedDetailsOfStaff(jsonObj.getInt("staffId"));
			propertyAssignedDto = DtoConverter.getPropertyAssignedToStaffDetails(propertyAssignDetails);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyAssignedDto;
	}

	@Override
	public StaffDto fetchStaffDetailsOfAgent(String jsonString) {
		StaffDto staffDto = new StaffDto();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			AgentDetails agentDetails = agentDetailsRepository.findById(jsonObj.getInt("agentId")).get();
			staffDto = DtoConverter.getStaffDetailsOfAgent(agentDetails);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return staffDto;
	}

	@Override
	public List<PropertyInfoDto> fetchPropertyInfo(String mobileNumber) {
		List<PropertyInfoDto> propertyInfoList = new ArrayList<PropertyInfoDto>();
		try {
			int userId = userRepository.findUserIdByMobileNumber(mobileNumber);
			List<Properties> properties = propertyRepository.fetchProperties(userId);
			propertyInfoList = DtoConverter.fetchPropertyInfo(properties);	
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyInfoList;
	}

	@Override
	public List<UserDetails> fetchUserDetails(String mobileNumber) {
		List<UserDetails> usersList = new ArrayList<UserDetails>();
		try {
			User user = userRepository.findByMobileNumber(mobileNumber);
			System.out.println(user.getStaffDetails().getStaffId()+"*********");
			List<AgentDetails> agentDetailsList = agentDetailsRepository.findAgentDetailsOfStaff(user.getStaffDetails().getStaffId());
			usersList = DtoConverter.fetchAgentDetailsOfStaff(agentDetailsList);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return usersList;
	}

	@Override
	public List<PropertyInfoDto> fetchPropertyAssignedToStaff(String mobileNumber) {
		List<PropertyInfoDto> propertyInfoDtoList = new ArrayList<PropertyInfoDto>();
		try {
			User user = userRepository.findByMobileNumber(mobileNumber);
			System.out.println(user.getStaffDetails().getStaffId()+"*********");
			List<PropertyAssignDetails> propertyAssignedList = propertyAssignDetailsRepository.fetchPropertyAssignedDetailsOfStaff(user.getStaffDetails().getStaffId());
			propertyInfoDtoList = DtoConverter.fetchPropertyAssignedToStaff(propertyAssignedList);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyInfoDtoList;
	}

	@Override
	public UserDetails fetchStaffAssignedUserDetails(String jsonString) {
		UserDetails userDetails = new UserDetails();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			User user = userRepository.findById(jsonObj.getInt("userId")).get();
			userDetails = DtoConverter.getSingleUserDetails(user);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return userDetails;
	}

	@Override
	public List<UserDetails> fetchStaffsOfManager(String mobileNumber) {
		List<UserDetails> staffDtoList = new ArrayList<UserDetails>();
		try {
			User user = userRepository.findByMobileNumber(mobileNumber);
			List<StaffDetails> staffDetails = staffRepository.findByManager(user.getManagerDetails().getManagerDetailsId());
			staffDtoList = DtoConverter.getStaffsOfManager(staffDetails,4);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return staffDtoList;
	}

	@Override
	public UserDetails fetchProfileDetails(String mobileNumber) {
		UserDetails userDetails = new UserDetails();
		try {
			User user = userRepository.findByMobileNumber(mobileNumber);
			userDetails = DtoConverter.getSingleUserDetails(user);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return userDetails;
	}

	@Override
	public ResponseDto updateUserDetails(String jsonString) {
		ResponseDto response = new ResponseDto();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			User user = userRepository.findById(jsonObj.getInt("userId")).get();
			if(!jsonObj.getString("currentPassword").equals("")) {							
				BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();  
				if(!encoder.matches(jsonObj.getString("currentPassword"), user.getPassword())) {
					System.out.println(jsonObj.getString("currentPassword"));
					response.setResultStatus(2);
					response.setHttpStatus(HttpServletResponse.SC_OK);
				}
			}
			if(((!jsonObj.getString("currentPassword").equals("")) || (jsonObj.getString("currentPassword").equals(""))) && (response.getResultStatus()!=2)){
				user.setEmailId(jsonObj.getString("emailId"));
				user.setMobileNumber(jsonObj.getString("mobileNumber"));
				user.setName(Util.nameFormat(jsonObj.getString("name")));
				//user.setPassword(jsonObj.getString("newPassword"));
			//	user.setPasswordConfirm(jsonObj.getString("newPassword"));
				//userService.save(user);
				if(!jsonObj.getString("newPassword").equals("")) {
					user.setPassword(jsonObj.getString("newPassword"));
					user.setPasswordConfirm(jsonObj.getString("newPassword"));
					userService.save(user);
					response.setHttpStatus(HttpServletResponse.SC_OK);
					response.setResultStatus(1);
				}else {
					userRepository.save(user);
					response.setHttpStatus(HttpServletResponse.SC_OK);
					response.setResultStatus(1);
				}
			}
			
			
		}catch(Exception e) {
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public void saveHomeScreenImage(String jsonString) {
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			int retval = uploadImages(jsonObj.getString("base64data"), jsonObj.getString("imageName")+".png", jsonObj.getString("folderName"));
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void saveBanners(String jsonString) {
		int retVal = 0;
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			retVal = uploadImages(jsonObj.getString("banner1"), "banner1.png", "bannerImages");
			retVal = uploadImages(jsonObj.getString("banner2"), "banner2.png", "bannerImages");

		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public int uploadImages(String base64String, String fileName, String folderName) throws IOException {
		BufferedOutputStream scanStream = null;
		Date date = new Date();
		System.out.println(date.toString());
		try {
			Base64.Decoder decoder = Base64.getDecoder();
			byte[] imageByte = decoder.decode(base64String.split("\\,")[1]);
			System.out.println(imageByte);
			String rootPath = System.getProperty("user.home");
			System.out.println(rootPath);
			File fileSaveDir = new File(rootPath + File.separator + "public_html" + File.separator + "assets"
					+ File.separator + "beegru" + File.separator + folderName);
			// Creates the save directory if it does not existsSS
			if (!fileSaveDir.exists()) {
				fileSaveDir.mkdirs();
				// Files.createDirectories(fileSaveDir);
			}
			File scanFile = new File(fileSaveDir.getAbsolutePath() + File.separator + fileName);
			scanStream = new BufferedOutputStream(new FileOutputStream(scanFile));
			scanStream.write(imageByte);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		} finally {
			scanStream.close();
		}
	}

	@Override
	public List<UserDetails> fetchManagerStaffDetails(String jsonString) {
		List<UserDetails> staffDtoList = new ArrayList<UserDetails>();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			List<StaffDetails> staffDetails = staffRepository.findByManager(jsonObj.getInt("managerId"));
			staffDtoList = DtoConverter.getStaffsOfManager(staffDetails,4);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return staffDtoList;
	}

	@Override
	public UserDetails getEmployeeDetails(String jsonString , int role) {
		UserDetails userDetails = new UserDetails();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			User user = userRepository.findById(jsonObj.getInt("userId")).get();
			userDetails = DtoConverter.getEmployeeDetails(user , role);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return userDetails;
	}

	@Override
	public Set<String> fetchLocations(String jsonString) {
		Set<String> locationList = new HashSet<String>();
		Set<String> locationSet = new HashSet<String>();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			locationList = propertyRepository.fetchLocation(jsonObj.getString("location"));
			for(String locationObj:locationList) {
				String str = locationObj;
				int a = str.toUpperCase().indexOf(jsonObj.getString("location").toUpperCase());
				locationSet.add(str.toUpperCase().substring(a).split(",")[0]);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return locationSet;
	}

	@Override
	public ResponseDto saveBeegruScore(BeegruScore beegruScore) {
		ResponseDto responseDto = new ResponseDto();
		Properties property = new Properties();
		try {
			property = propertyRepository.fetchById(beegruScore.getProperties().getPropertyId());
			if(property == null) {
				responseDto.setResultStatus(4); // invalid ProductId
				responseDto.setHttpStatus(HttpServletResponse.SC_OK);
			}else {
				BeegruScore findbeegruScore = beegruScoreRepository.findByPropductId(beegruScore.getProperties().getPropertyId());
				if(findbeegruScore == null) {
					if(beegruScoreRepository.save(beegruScore)!=null) {
						responseDto.setResultStatus(1); // saved successfully
						responseDto.setHttpStatus(HttpServletResponse.SC_OK);
					}else {
						responseDto.setResultStatus(2); // not saved
						responseDto.setHttpStatus(HttpServletResponse.SC_OK);
					}
				}else {
					responseDto.setResultStatus(3); // productId already exists
					responseDto.setHttpStatus(HttpServletResponse.SC_OK);
				}
			}
			
		}catch(Exception e) {
			responseDto.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
		}
		return responseDto;
	}

	@Override
	public List<BeegruScoreDto> fetchBeegruScore() {
		List<BeegruScoreDto> beegruScoreList = new ArrayList<BeegruScoreDto>();
		try {
			List<BeegruScore> beegruScore = beegruScoreRepository.findAll();
			beegruScoreList = DtoConverter.fetchBeegruScoreList(beegruScore);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return beegruScoreList;
	}

	@Override
	public BeegruScoreDto findBeegruScoreById(String jsonString) {
		BeegruScoreDto beegruScoreDto = new BeegruScoreDto();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			BeegruScore beegruScore = beegruScoreRepository.findById(jsonObj.getInt("beegruScoreId")).get();
			beegruScoreDto = DtoConverter.fetchBeegruScoreDto(beegruScore);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return beegruScoreDto;
	}

	@Override
	public ResponseDto deleteBeegruScore(String jsonString) {
		System.out.println("inside delete beegruScore");
		ResponseDto response = new ResponseDto();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			System.out.println(jsonObj.getInt("beegruScoreId"));
			beegruScoreRepository.deleteBeegruScore(jsonObj.getInt("beegruScoreId"));
			response.setHttpStatus(HttpServletResponse.SC_OK);
		}catch(Exception e) {
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public ResponseDto editBeegruScore(BeegruScore beegruScore) {
		ResponseDto responseDto = new ResponseDto();
		Properties property = new Properties();
		try {
			property = propertyRepository.fetchById(beegruScore.getProperties().getPropertyId());
			if(property == null) {
				responseDto.setResultStatus(4); // invalid ProductId
				responseDto.setHttpStatus(HttpServletResponse.SC_OK);
			}else {
				BeegruScore findbeegruScore = beegruScoreRepository.findByPropductId(beegruScore.getProperties().getPropertyId(), beegruScore.getBeegruScoreId());
				if(findbeegruScore == null) {
					if(beegruScoreRepository.save(beegruScore)!=null) {
						responseDto.setResultStatus(1); // saved successfully
						responseDto.setHttpStatus(HttpServletResponse.SC_OK);
					}else{
						responseDto.setResultStatus(2); // not saved
						responseDto.setHttpStatus(HttpServletResponse.SC_OK);
					}
				}else {
					responseDto.setResultStatus(3); // productId already exists
					responseDto.setHttpStatus(HttpServletResponse.SC_OK);
				}
				
			}
			
		}catch(Exception e) {
			responseDto.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
		}
		return responseDto;
	}

	@Override
	public List<PostYourRequirementDto> getPostYourRequirement() {
		List<PostYourRequirementDto> postYourRequireDtoList = new ArrayList<PostYourRequirementDto>();
		try {
			List<PostYourRequirement> postYourRequirement = postYourRequirementRepository.fetchPostYourRequirement();
			postYourRequireDtoList = DtoConverter.getPostYourRequirements(postYourRequirement);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return postYourRequireDtoList;
	}

	@Override
	public ResponseDto updatePostYourRequirementStatus(String jsonString) {
		ResponseDto response = new ResponseDto();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			int retVal = postYourRequirementRepository.updatePostYourRequirementStatus(jsonObj.getInt("requirementId"),jsonObj.getInt("status"));
			if(retVal>0) {
				response.setResultStatus(1); 
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}else {
				response.setResultStatus(2); 
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}
		}catch(Exception e) {
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
		}
		return response;
	}
	
	@Override
	public TestimonialsDto findTestimonialById(String jsonString) {
		System.out.println("Insdie testimonial");
		TestimonialsDto testimonialsDto = new TestimonialsDto();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			PeopleTestimonials peopletestimonials = peopleTestimonialRepository.findById(jsonObj.getInt("testimonialId")).get();
			testimonialsDto = DtoConverter.getTestimonials(peopletestimonials);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return testimonialsDto;
	}

	@Override
	public ResponseDto editManager(User user) {
		ResponseDto response = new ResponseDto();
		User finduser = new User();
		ManagerDetails managerDetails = new ManagerDetails();
		BranchDetails branchDetails = new BranchDetails();
		try {
			finduser = userRepository.findByEmailIdOrMobileNumber(user.getEmailId(), user.getMobileNumber(),user.getUserId());
			if (finduser == null) {
				user.setActiveStatus(1);
				user.setName(Util.nameFormat(user.getName()));
				branchDetails.setBranchId(user.getManagerDetails().getBranch().getBranchId());
				managerDetails.setManagerDetailsId(user.getManagerDetails().getManagerDetailsId());
				managerDetails.setUser(user);
				managerDetails.setBranch(branchDetails);
				user.setManagerDetails(managerDetails);
				user.setPasswordConfirm(user.getPassword());
				System.out.println(user.getAddedUserMobileNumber());
				userService.save(user);
				//smsTemplates.registrationSMS(user.getName(), user.getMobileNumber(), user.getEmailId(), user.getPasswordConfirm(), "Customer");
				//emailTemplates.registrationEmail(user.getName(), user.getMobileNumber(), user.getEmailId(),user.getPasswordConfirm(), "Customer");
				response.setResultStatus(1);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			} else {
				if(finduser.getMobileNumber().equals(user.getMobileNumber())) {
					response.setResultStatus(2); // mobile Number already exist
					response.setHttpStatus(HttpServletResponse.SC_OK);
				}else if(finduser.getEmailId().equals(user.getEmailId())){
					response.setResultStatus(3); // emailId already exist
					response.setHttpStatus(HttpServletResponse.SC_OK);
				}
			}
			return response;
		} catch (Exception e) {
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
			return response;
		}
	}

	@Override
	public AboutUsDto getAboutUsData() {
		AboutUsDto aboutUsDto = new AboutUsDto();
		try {
			aboutUsDto = aboutUsRepository.fetchAboutUsContent();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return aboutUsDto;
	}

	@Override
	public List<PropertyEnquiryDto> fetchPropertyEnquiryList() {
		List<PropertyEnquiryDto> propertyEnquiryList = new ArrayList<PropertyEnquiryDto>();
		try {
			 propertyEnquiryList = propertyEnquiryRepository.findAllpropertyEnquiry();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyEnquiryList;
	}

	@Override
	public ResponseDto updatePropertyEnquiry(String jsonString) {
		ResponseDto response = new ResponseDto();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			int retVal = propertyEnquiryRepository.updatePropertyEnquiryStatus(jsonObj.getInt("propertyEnquiryId"),jsonObj.getInt("status"));
			if(retVal>0) {
				response.setResultStatus(1);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}else {
				response.setResultStatus(2);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}
		}catch(Exception e) {
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public PropertyImageDto getPropertyImageDto(String jsonString) {
		PropertyImageDto propertyImageDto = new PropertyImageDto();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			propertyImageDto = propertyRepository.getPropertyImageDto(jsonObj.getInt("propertyId"));
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyImageDto;
	}
	
}
