package com.atechtron.beegru.login.service;

import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.atechtron.beegru.login.dto.BeegruScoreDto;
import com.atechtron.beegru.login.dto.BranchDto;
import com.atechtron.beegru.login.dto.FeedbackDto;
import com.atechtron.beegru.login.dto.ManagerDto;
import com.atechtron.beegru.login.dto.PostYourRequirementDto;
import com.atechtron.beegru.login.dto.PropertiesDto;
import com.atechtron.beegru.login.dto.PropertyAssignedDto;
import com.atechtron.beegru.login.dto.PropertyEnquiryDto;
import com.atechtron.beegru.login.dto.PropertyImageDto;
import com.atechtron.beegru.login.dto.PropertyInfoDto;
import com.atechtron.beegru.login.dto.PropertyTagDto;
import com.atechtron.beegru.login.dto.StaffDto;
import com.atechtron.beegru.login.dto.UserDetails;
import com.atechtron.beegru.login.model.AboutUs;
import com.atechtron.beegru.login.model.BeegruScore;
import com.atechtron.beegru.login.model.PeopleTestimonials;
import com.atechtron.beegru.login.model.PostYourRequirement;
import com.atechtron.beegru.login.model.Properties;
import com.atechtron.beegru.login.model.ServiceDetails;
import com.atechtron.beegru.login.model.Services;
import com.atechtron.beegru.website.dto.AboutUsDto;
import com.atechtron.beegru.website.dto.LocationDto;
import com.atechtron.beegru.website.dto.ResponseDto;
import com.atechtron.beegru.website.dto.TestimonialsDto;
import com.atechtron.beegru.website.model.User;

public interface LoginService {
	
	ResponseDto addProperty(Properties property);

	List<FeedbackDto> fetchFeedbacks();

	ResponseDto updateContactUsStatus(String jsonString);

	//ResponseDto saveResidentialForSale(Properties properties);

	int findUserIdByMobileNumber(String name);

	//void saveResidentailSaleImage(String jsonString);

	ResponseDto saveProperties(Properties properties);

	void savePropertiesImages(String jsonString);

	List<PropertyInfoDto> fetchPropertyInfo();

	//PropertiesDto fetchSingleProperty();

	//PropertiesDto fetchSingleProperty(String jsonString);

	User findByMobileNumber(String name);

	ResponseDto savePostYourRequirement(PostYourRequirement postYourRequirement);

	ResponseDto addServices(Services services);

	ResponseDto saveServiceDetails(ServiceDetails serviceDetails);

	ResponseDto saveAboutUsDetails(AboutUs aboutUs);

	ResponseDto saveTestimonials(String jsonString,int userId);

	ResponseDto updatePropertyStatus(String jsonString);

	ResponseDto addToWishlist(String jsonString, String name);

	List<PropertyInfoDto> fetchWishList(String mobileNumber);

	List<PropertyInfoDto> fetchSearchedProperties(String jsonString);

	ResponseDto updateFeaturedPropertyStatus(String jsonString);

	List<UserDetails> fetchUserDetails(int role);

	AboutUsDto fetchAboutUs();

	List<TestimonialsDto> fetchTestimonialsDto();

	ResponseDto saveTags(String jsonString);

	PropertyInfoDto fetchPropertyName(String jsonString);

	List<PropertyTagDto> fetchPropertyTags();

	PropertyTagDto fetchPropertyTagDto(String jsonString);

	ResponseDto deleteTags(String jsonString);

	ResponseDto editTags(String jsonString);

	ResponseDto saveUser(User user);

	ResponseDto updateUserStatus(String jsonString);

	ResponseDto assignWorkingAreaToManager(String jsonString, String mobileNumber);

	ResponseDto saveBranch(String jsonString, String name);

	List<BranchDto> fetchBranchDetails();

	ResponseDto editBranch(String jsonString, String name);

	BranchDto fetchBranchById(String jsonString);

	ResponseDto saveManager(User user);

	List<ManagerDto> fetchManagerDetails();

	ResponseDto saveStaff(User user);

	List<StaffDto> fetchStaff();

	ResponseDto saveAgent(User user);

	StaffDto fetchStaff(String jsonString);

	ResponseDto assignProperty(String jsonString,String mobileNumber);

	ResponseDto editAssignProperty(String jsonString, String name);

	PropertyAssignedDto fetchPropertyDetails(String jsonString);

	List<PropertyAssignedDto> fetchPropertyAssignedDetails(String jsonString);

	StaffDto fetchStaffDetailsOfAgent(String jsonString);

	List<PropertyInfoDto> fetchPropertyInfo(String name);

	List<UserDetails> fetchUserDetails(String name);

	List<PropertyInfoDto> fetchPropertyAssignedToStaff(String mobileNumber);

	UserDetails fetchStaffAssignedUserDetails(String jsonString);

	List<UserDetails> fetchStaffsOfManager(String name);

	UserDetails fetchProfileDetails(String moblieNumber);

	ResponseDto updateUserDetails(String jsonString);

	void saveHomeScreenImage(String jsonString);

	void saveBanners(String jsonString);

	List<UserDetails> fetchManagerStaffDetails(String jsonString);

	UserDetails getEmployeeDetails(String jsonString,int role);

	Set<String> fetchLocations(String jsonString);

	ResponseDto saveBeegruScore(BeegruScore beegruScore);

	List<BeegruScoreDto> fetchBeegruScore();

	BeegruScoreDto findBeegruScoreById(String jsonString);

	ResponseDto deleteBeegruScore(String jsonString);

	ResponseDto editBeegruScore(BeegruScore beegruScore);

	List<PostYourRequirementDto> getPostYourRequirement();

	ResponseDto updatePostYourRequirementStatus(String jsonString);

	ResponseDto editTestimonials(String jsonString, int userId);

	TestimonialsDto findTestimonialById(String jsonString);

	ResponseDto deleteTestimonialById(String jsonString);

	PropertiesDto fetchSingleProperty(String jsonString, int i);

	ResponseDto editManager(User user);

	ResponseDto editStaff(User user);

	ResponseDto editAgent(User user);

	AboutUsDto getAboutUsData();
	
	List<PropertyEnquiryDto> fetchPropertyEnquiryList();

	ResponseDto updatePropertyEnquiry(String jsonString);

	PropertyImageDto getPropertyImageDto(String jsonString);

	//ResponseDto saveTestimonials(String jsonString, String mobileNumber);

}
