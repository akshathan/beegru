package com.atechtron.beegru.login.restController;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.atechtron.beegru.login.dto.BeegruScoreDto;
import com.atechtron.beegru.login.dto.BranchDto;
import com.atechtron.beegru.login.dto.ManagerDto;
import com.atechtron.beegru.login.dto.PropertiesDto;
import com.atechtron.beegru.login.dto.PropertyAssignedDto;
import com.atechtron.beegru.login.dto.PropertyImageDto;
import com.atechtron.beegru.login.dto.PropertyInfoDto;
import com.atechtron.beegru.login.dto.PropertyTagDto;
import com.atechtron.beegru.login.dto.StaffDto;
import com.atechtron.beegru.login.dto.UserDetails;
import com.atechtron.beegru.login.model.AboutUs;
import com.atechtron.beegru.login.model.BeegruScore;
import com.atechtron.beegru.login.model.PeopleTestimonials;
import com.atechtron.beegru.login.model.PostYourRequirement;
import com.atechtron.beegru.login.model.Properties;
import com.atechtron.beegru.login.model.ServiceDetails;
import com.atechtron.beegru.login.model.Services;
import com.atechtron.beegru.login.service.LoginService;
import com.atechtron.beegru.repository.ServiceDetailsRepository;
import com.atechtron.beegru.website.dto.AboutUsDto;
import com.atechtron.beegru.website.dto.ResponseDto;
import com.atechtron.beegru.website.dto.TestimonialsDto;
import com.atechtron.beegru.website.model.User;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(value = "/login")
public class LoginController {

	@Autowired
	LoginService loginService;

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/addProperty", method = RequestMethod.POST)
	public ResponseDto addProperty(@RequestBody Properties property, Authentication authentication) {
		ResponseDto response = new ResponseDto();
		try {
			response = loginService.addProperty(property);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/updateContactUsStatus", method = RequestMethod.POST)
	public ResponseDto updateContactUsStatus(@RequestBody String jsonString) {
		ResponseDto response = new ResponseDto();
		try {
			response = loginService.updateContactUsStatus(jsonString);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/saveProperties", method = RequestMethod.POST)
	public ResponseDto saveProperties(@RequestBody Properties properties, Authentication authentication) {
		System.out.println(properties.getPropertyVicinity());
		ResponseDto response = new ResponseDto();
		try {
			User user = new User();
			int userId = loginService.findUserIdByMobileNumber(authentication.getName());
			user.setUserId(userId);
			properties.setUser(user);
			response = loginService.saveProperties(properties);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/savePropertiesImages", method = RequestMethod.POST)
	public String savePropertiesImages(@RequestBody String jsonString) {

		try {
			loginService.savePropertiesImages(jsonString);
			return jsonString;
		}catch(Exception e) {
			e.printStackTrace();
			return jsonString;
		}
		
	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/fetchProperties", method = RequestMethod.POST)
	public List<PropertyInfoDto> fetchProperties() {
		List<PropertyInfoDto> propertyInfoList = new ArrayList<PropertyInfoDto>();
		try {
			propertyInfoList = loginService.fetchPropertyInfo();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyInfoList;
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/savePostYourRequirement", method = RequestMethod.POST)
	public ResponseDto savePostYourRequirement(@RequestBody PostYourRequirement postYourRequirement, Authentication authentication) {
		ResponseDto response = new ResponseDto();
		User user = new User();
		try {
			int userId = loginService.findUserIdByMobileNumber(authentication.getName());
			user.setUserId(userId);
			postYourRequirement.setActiveStatus(2); //not viewed
			postYourRequirement.setUser(user);
			response = loginService.savePostYourRequirement(postYourRequirement);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/fetchSingleProperties", method = RequestMethod.POST)
	public PropertiesDto fetchSingleProperty(@RequestBody String jsonString) {
		PropertiesDto propertiesDto = new PropertiesDto();
		try {
			propertiesDto = loginService.fetchSingleProperty(jsonString,1);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertiesDto;
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/addServiceTypes" , method = RequestMethod.POST)
	public ResponseDto addServices(@RequestBody Services services) {
		ResponseDto response = new ResponseDto();
		try {
			response = loginService.addServices(services);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/saveServiceDetails", method = RequestMethod.POST)
	public ResponseDto saveServiceDetails(@RequestBody ServiceDetails serviceDetails, Authentication authentication) {
		ResponseDto response = new ResponseDto();
		User user = new User();
		try {
			int userId = loginService.findUserIdByMobileNumber(authentication.getName());
			user.setUserId(userId);
			serviceDetails.setUser(user);
			response = loginService.saveServiceDetails(serviceDetails);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/saveAboutUs", method = RequestMethod.POST)
	public ResponseDto saveAboutUsDetails(@RequestBody AboutUs aboutUs, Authentication authentication) {
		ResponseDto response = new ResponseDto();
		try {
			int userId = loginService.findUserIdByMobileNumber(authentication.getName());
			aboutUs.setAddedUserId(userId);
			aboutUs.setAddedUserMobileNumber(authentication.getName());
			response = loginService.saveAboutUsDetails(aboutUs);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/saveTestimonials", method = RequestMethod.POST)
	public ResponseDto saveTestimonialDetails(@RequestBody String jsonString, Authentication authentication) {
		ResponseDto response = new ResponseDto();
		try {
			//peopleTestimonial.setAddedUserMobileNumber(authentication.getName());
			int userId = loginService.findUserIdByMobileNumber(authentication.getName());
			response = loginService.saveTestimonials(jsonString,userId);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/editTestimonials", method = RequestMethod.POST)
	public ResponseDto editTestimonialDetails(@RequestBody String jsonString, Authentication authentication) {
		ResponseDto response = new ResponseDto();
		try {
			//peopleTestimonial.setAddedUserMobileNumber(authentication.getName());
			int userId = loginService.findUserIdByMobileNumber(authentication.getName());
			response = loginService.editTestimonials(jsonString,userId);
			System.out.println(response);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/updatePropertyStatus", method = RequestMethod.POST)
	public ResponseDto updatePropertyStatus(@RequestBody String jsonString) {
		ResponseDto response = new ResponseDto();
		try {
			response = loginService.updatePropertyStatus(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/saveWishList", method = RequestMethod.POST)
	public ResponseDto addToWishList(@RequestBody String jsonString, Authentication authentication) {
		ResponseDto response = new ResponseDto();
		try {
			response = loginService.addToWishlist(jsonString,authentication.getName());
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/fetchWishList", method = RequestMethod.GET)
	public List<PropertyInfoDto> FetchWishList(Authentication authentiction){
		List<PropertyInfoDto> propertyInfoList = new ArrayList<PropertyInfoDto>();
		try {
			propertyInfoList = loginService.fetchWishList(authentiction.getName());
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyInfoList;
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/searchedProperties", method = RequestMethod.POST)
	public List<PropertyInfoDto> fetchSearchedProperties(@RequestBody String jsonString){
		List<PropertyInfoDto> propertyList = new ArrayList<PropertyInfoDto>();
		try {
			propertyList = loginService.fetchSearchedProperties(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyList;
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/updateFeaturedPropertyStatus", method = RequestMethod.POST)
	public ResponseDto updateFeaturedPropertyStatus(@RequestBody String jsonString) {
		ResponseDto response = new ResponseDto();
		try {
			response = loginService.updateFeaturedPropertyStatus(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = "/saveTags", method = RequestMethod.POST)
	public ResponseDto saveTags(@RequestBody String jsonString) {
		ResponseDto response =  new ResponseDto();
		try {
			response = loginService.saveTags(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = "/fetchPropertyName", method = RequestMethod.POST)
	public PropertyInfoDto fetchPropertyName(@RequestBody String jsonString) {
		PropertyInfoDto propertyInfoDto = new PropertyInfoDto();
		try {
			propertyInfoDto = loginService.fetchPropertyName(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyInfoDto;
	}
	
	@RequestMapping(value = "/fetchTagById", method = RequestMethod.POST)
	public PropertyTagDto  fetchTagById(@RequestBody String jsonString) {
		PropertyTagDto propertyTagDto = new PropertyTagDto();
		try {
			propertyTagDto = loginService.fetchPropertyTagDto(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyTagDto;
	}

	@RequestMapping(value = "/deleteTags", method = RequestMethod.POST)
	public ResponseDto deleteTags(@RequestBody String jsonString) {
		ResponseDto response = new ResponseDto();
		try {
			response = loginService.deleteTags(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = "/editTags" , method = RequestMethod.POST)
	public ResponseDto editTags(@RequestBody String jsonString) {
		ResponseDto response = new ResponseDto();
		try {
			response = loginService.editTags(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = "/saveUser", method = RequestMethod.POST)
	public ResponseDto saveUser(@RequestBody User user, Authentication authentication) {
		ResponseDto response = new ResponseDto();
		try {
			user.setPasswordConfirm(user.getPassword());
			user.setAddedUserMobileNumber(authentication.getName());
			response = loginService.saveUser(user);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	} 
	
	@RequestMapping(value = "/saveManager", method = RequestMethod.POST)
	public ResponseDto savemanager(@RequestBody User user, Authentication authentication) {
		System.out.println("inside save Manager");
		System.out.println(user);
		ResponseDto response = new ResponseDto();
		try {
			int userId = loginService.findUserIdByMobileNumber(authentication.getName());
			user.setPasswordConfirm(user.getPassword());
			user.setAddedUserId(userId);
			user.setAddedUserMobileNumber(authentication.getName());
			response = loginService.saveManager(user);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = "/editManager", method = RequestMethod.POST)
	public ResponseDto editManager(@RequestBody User user, Authentication authentication) {
		System.out.println("inside save Manager");
		System.out.println(user);
		ResponseDto response = new ResponseDto();
		try {
			int userId = loginService.findUserIdByMobileNumber(authentication.getName());
			user.setPasswordConfirm(user.getPassword());
			user.setAddedUserId(userId);
			user.setAddedUserMobileNumber(authentication.getName());
			response = loginService.editManager(user);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	
	@RequestMapping(value = "/updateUserStatus", method = RequestMethod.POST)
	public ResponseDto updateUserStatus(@RequestBody String jsonString) {
		ResponseDto response = new ResponseDto();
		try {
			response = loginService.updateUserStatus(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = "/assignWorkingAreaToManager", method = RequestMethod.POST)
	public ResponseDto assignWorkingArea(@RequestBody String jsonString, Authentication authentication) {
		ResponseDto response = new ResponseDto();
		try {
			response = loginService.assignWorkingAreaToManager(jsonString, authentication.getName());
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = "/saveBranch", method = RequestMethod.POST)
	public ResponseDto saveBranch(@RequestBody String jsonString, Authentication authentication) {
		ResponseDto response = new ResponseDto();
		try {
			int userId = loginService.findUserIdByMobileNumber(authentication.getName());
			//user.setPasswordConfirm(user.getPassword());
			//user.setAddedUserId(userId);
			System.out.println("inside save branch");
			response = loginService.saveBranch(jsonString, authentication.getName());
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = "/editBranch", method = RequestMethod.POST)
	public ResponseDto editBranch(@RequestBody String jsonString, Authentication authentication) {
		ResponseDto response = new ResponseDto();
		try {
			int userId = loginService.findUserIdByMobileNumber(authentication.getName());
			//user.setPasswordConfirm(user.getPassword());
			//user.setAddedUserId(userId);
			response = loginService.editBranch(jsonString,authentication.getName());
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = "fetchBranchDetails", method = RequestMethod.GET)
	public List<BranchDto> fetchBranchDto(){
		List<BranchDto> branchDtoList = new ArrayList<BranchDto>();
		try {
			branchDtoList = loginService.fetchBranchDetails();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return branchDtoList;
	}
	@RequestMapping(value = "/fetchBranchById", method = RequestMethod.POST)
	public BranchDto fetchBranchById(@RequestBody String jsonString) {
		BranchDto branch = new BranchDto();
		try {
			branch = loginService.fetchBranchById(jsonString);
		}catch(Exception e){
			e.printStackTrace();
		}
		return branch;
	}
	
	@RequestMapping(value = "/fetchManagerDetails", method = RequestMethod.GET)
	public List<ManagerDto> fetchManagerDetails(){
		List<ManagerDto> managerList = new ArrayList<ManagerDto>();
		try {
			managerList = loginService.fetchManagerDetails();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return managerList;
	}
	
	@RequestMapping(value = "/saveStaff", method = RequestMethod.POST)
	public ResponseDto saveStaff(@RequestBody User user, Authentication authentication) {
		System.out.println(user.getStaffDetails());
		ResponseDto response = new ResponseDto();
		try {
			int userId = loginService.findUserIdByMobileNumber(authentication.getName());
			user.setPasswordConfirm(user.getPassword());
			user.setAddedUserId(userId);
			user.setAddedUserMobileNumber(authentication.getName());
			response = loginService.saveStaff(user);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = "/editStaff", method = RequestMethod.POST)
	public ResponseDto editStaff(@RequestBody User user, Authentication authentication) {
		System.out.println(user.getStaffDetails());
		ResponseDto response = new ResponseDto();
		try {
			int userId = loginService.findUserIdByMobileNumber(authentication.getName());
			user.setPasswordConfirm(user.getPassword());
			user.setAddedUserId(userId);
			user.setAddedUserMobileNumber(authentication.getName());
			response = loginService.editStaff(user);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = "/saveAgent", method = RequestMethod.POST)
	public ResponseDto saveAgent(@RequestBody User user, Authentication authentication) {
		ResponseDto response = new ResponseDto();
		try {
			int userId = loginService.findUserIdByMobileNumber(authentication.getName());
			user.setPasswordConfirm(user.getPassword());
			user.setAddedUserId(userId);
			user.setAddedUserMobileNumber(authentication.getName());
			response = loginService.saveAgent(user);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = "/editAgent", method = RequestMethod.POST)
	public ResponseDto editAgent(@RequestBody User user, Authentication authentication) {
		ResponseDto response = new ResponseDto();
		try {
			int userId = loginService.findUserIdByMobileNumber(authentication.getName());
			user.setPasswordConfirm(user.getPassword());
			user.setAddedUserId(userId);
			user.setAddedUserMobileNumber(authentication.getName());
			response = loginService.editAgent(user);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = "/fetchStaff", method = RequestMethod.GET)
	public List<StaffDto> fetchStaffDetails(){
		List<StaffDto> staffDtoList = new ArrayList<StaffDto>();
		try {
			staffDtoList = loginService.fetchStaff();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return staffDtoList;
	}
	
	@RequestMapping(value = "/getStaffDetails", method = RequestMethod.POST)
	public StaffDto fetchStaffDetails(@RequestBody String jsonString){
		StaffDto staffList = new StaffDto();
		try {
			staffList = loginService.fetchStaff(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return staffList;
	}
	
	@RequestMapping(value = "/assignProperty", method = RequestMethod.POST)
	public ResponseDto assignProperty(@RequestBody String jsonString, Authentication authentication) {
		ResponseDto response = new ResponseDto();
		try {
			response = loginService.assignProperty(jsonString, authentication.getName());
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	@RequestMapping(value = "editAssignProperty", method = RequestMethod.POST)
	public ResponseDto editAssignProperty(@RequestBody String jsonString, Authentication authentication) {
		ResponseDto response = new ResponseDto();
		try {
			response = loginService.editAssignProperty(jsonString,authentication.getName());
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = "/getAssignedPropertyDetails" , method = RequestMethod.POST)
	public PropertyAssignedDto fetchDetails(@RequestBody String jsonString) {
		PropertyAssignedDto propertyAssignedDto = new PropertyAssignedDto();
		try {
			propertyAssignedDto = loginService.fetchPropertyDetails(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyAssignedDto;
		
	}
	
	@RequestMapping(value = "/assignedPropertyDetailsOfStaff", method = RequestMethod.POST)
	public List<PropertyAssignedDto> assignedPropertyDetailsOfStaff(@RequestBody String jsonString){
		List<PropertyAssignedDto> propertyAssignedDtoList = new ArrayList<PropertyAssignedDto>();
		try {
			propertyAssignedDtoList = loginService.fetchPropertyAssignedDetails(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyAssignedDtoList;
	}
	
	@RequestMapping(value = "/fetchStaffDetailsOfAgent" , method = RequestMethod.POST)
	public StaffDto fetchStaffDetailsOfAgent(@RequestBody String jsonString) {
		StaffDto staffDto = new StaffDto();
		try {
			staffDto = loginService.fetchStaffDetailsOfAgent(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return staffDto;
	}
	
	@RequestMapping(value = "/fetchStaffAssignedUserDetails", method = RequestMethod.POST)
	public UserDetails fetchStaffAssignedUserDetails(@RequestBody String jsonString) {
		UserDetails userDetails = new UserDetails();
		try {
			userDetails = loginService.fetchStaffAssignedUserDetails(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return userDetails;
	}
	
	@RequestMapping(value = "/fetchProfileDetails" , method = RequestMethod.GET)
	public UserDetails fetchProfileDetails(Authentication authentication) {
		UserDetails userDetails = new UserDetails();
		try {
			userDetails = loginService.fetchProfileDetails(authentication.getName());
		}catch(Exception e) {
			e.printStackTrace();
		}
		return userDetails;
	}
	
	@RequestMapping(value = "/updateUser", method = RequestMethod.POST)
	public ResponseDto response(@RequestBody String jsonString) {
		ResponseDto response = new ResponseDto();
		try {
			response = loginService.updateUserDetails(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = "/saveHomePageImagesOrBanner", method = RequestMethod.POST)
	public int saveHomeScreenImage(@RequestBody String jsonString) {
		try {
			loginService.saveHomeScreenImage(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	@RequestMapping(value = "/saveBannerImages", method = RequestMethod.POST)
	public int saveBanners(@RequestBody String jsonString) {
		try {
			loginService.saveBanners(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	@RequestMapping(value = "/getUploadedProperties", method = RequestMethod.GET)
	public List<PropertyInfoDto> staffProperties(Authentication authentication) {
		List<PropertyInfoDto> propertyInfoList = new ArrayList<PropertyInfoDto>();
		try {
			propertyInfoList = loginService.fetchPropertyInfo(authentication.getName());
			return propertyInfoList;
		}catch(Exception e) {
			e.printStackTrace();
			return propertyInfoList;
		}
	}
	
	@RequestMapping(value = "/managerStaffDetails", method = RequestMethod.POST)
	public List<UserDetails> manager_staff_details(@RequestBody String jsonString) {
		System.out.println("inside manager staff Details");
		List<UserDetails> staffDtoList = new ArrayList<UserDetails>();
		try {
			staffDtoList = loginService.fetchManagerStaffDetails(jsonString);
			//userDetailsList = DtoConverter.getUserDetails(usersList,role);
			return staffDtoList;
		}catch(Exception e) {
			e.printStackTrace();
			return staffDtoList;
		}
	}
	
	@RequestMapping(value = "getManagerDetails", method = RequestMethod.POST)
	public UserDetails getManagerDetails(@RequestBody String jsonString) {
		UserDetails userDetails = new UserDetails();
		try {
			userDetails = loginService.getEmployeeDetails(jsonString,3);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return userDetails;
	}
	
	@RequestMapping(value = "getStaffDetailsToUpdate", method = RequestMethod.POST)
	public UserDetails getStaffDetails(@RequestBody String jsonString) {
		UserDetails userDetails = new UserDetails();
		try {
			userDetails = loginService.getEmployeeDetails(jsonString,4);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return userDetails;
	}
	
	@RequestMapping(value = "getAgentDetailsToUpdate", method = RequestMethod.POST)
	public UserDetails getAgentDetails(@RequestBody String jsonString) {
		UserDetails userDetails = new UserDetails();
		try {
			userDetails = loginService.getEmployeeDetails(jsonString,5);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return userDetails;
	}
	
	@RequestMapping(value = "/saveBeegruScore", method = RequestMethod.POST)
	public ResponseDto saveBeegruScore(@RequestBody BeegruScore beegruScore) {
		ResponseDto response = new ResponseDto();
		try {
			response = loginService.saveBeegruScore(beegruScore);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = "/editBeegruScore", method = RequestMethod.POST)
	public ResponseDto editBeegruScore(@RequestBody BeegruScore beegruScore) {
		ResponseDto response = new ResponseDto();
		try {
			response = loginService.editBeegruScore(beegruScore);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = "/fetchBeegruScore", method = RequestMethod.GET)
	public List<BeegruScoreDto> beegruScoreList(){
		List<BeegruScoreDto> beegruScore = new ArrayList<BeegruScoreDto>();
		try {
			beegruScore = loginService.fetchBeegruScore();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return beegruScore;
	}
	
	@RequestMapping(value = "/fetchBeegruScoreById", method = RequestMethod.POST)
	public BeegruScoreDto getBeegruDto(@RequestBody String jsonString) {
		BeegruScoreDto beegruScoreDto = new BeegruScoreDto();
		try {
			beegruScoreDto = loginService.findBeegruScoreById(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return beegruScoreDto;
	}
	
	@RequestMapping(value = "/deleteBeegruScore", method = RequestMethod.POST)
	public ResponseDto deleteBeegruScore(@RequestBody String jsonString) {
		System.out.println("inside delete beegru score");
		ResponseDto response = new ResponseDto();
		try {
			response = loginService.deleteBeegruScore(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = "/updatePostYourRequirementStatus", method = RequestMethod.POST)
	public ResponseDto updatePostYourRequirementStatus(@RequestBody String jsonString) {
		ResponseDto response = new ResponseDto();
		try {
			response = loginService.updatePostYourRequirementStatus(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = "/getTestimonialById", method = RequestMethod.POST)
	public TestimonialsDto gettestimonialById(@RequestBody String jsonString) {
		TestimonialsDto testimonialDto = new TestimonialsDto();
		try {
			testimonialDto = loginService.findTestimonialById(jsonString);
			System.out.println(testimonialDto);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return testimonialDto;
	}
	
	@RequestMapping(value = "/deleteTestimonialById", method = RequestMethod.POST)
	public ResponseDto deletetestimonialById(@RequestBody String jsonString) {
		ResponseDto response = new ResponseDto();
		try {
			response = loginService.deleteTestimonialById(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = "/getAboutUsContent", method = RequestMethod.POST)
	public AboutUsDto getAboutUsData() {
		AboutUsDto aboutUs = new AboutUsDto();
		try {
			aboutUs = loginService.getAboutUsData();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return aboutUs;
	}
	
	@RequestMapping(value = "/updatePropertyEnquiry", method = RequestMethod.POST)
	public ResponseDto updatePropertyEnquiry(@RequestBody String jsonString) {
		ResponseDto response = new ResponseDto();
		try {
			response = loginService.updatePropertyEnquiry(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = "/fetchPropertyImageDetails", method = RequestMethod.POST)
	public PropertyImageDto getPropertyImageDto(@RequestBody String jsonString) {
		PropertyImageDto propertyImageDto = new PropertyImageDto();
		try {
			propertyImageDto = loginService.getPropertyImageDto(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyImageDto;
	}
		
}
