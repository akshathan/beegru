package com.atechtron.beegru.login.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


import com.atechtron.beegru.website.model.User;

import lombok.Data;
import lombok.ToString;

@Data
@Entity
public class ManagerDetails {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int managerDetailsId;
	
	private String addedDate;
	
	private String addedBy;
	
//	private int addedUserId;
	
	@OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "user_user_id")
	private User user;
	
	@ManyToOne
	@ToString.Exclude
	@JoinColumn(name = "manager_working_area_id")
	private BranchDetails branch;
	
	@OneToMany(mappedBy="managerDetails" , cascade=CascadeType.ALL)
	@ToString.Exclude
//    @JoinColumn(name = "manager_details_manager_id")
	private List<StaffDetails> staffDetails;

}
