package com.atechtron.beegru.login.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
public class PropertyTags {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int propertyTagsId;
	
	@Column
	private String tagName;
	
	private int addedUserId;
	
	@Column
	private int position;
	
	@Column
	private int activeStatus;
	
	@Column
	private String addedDate;
	
	@ManyToOne(cascade= {CascadeType.ALL})
	@JoinColumn(name = "properties_property_id")
	private Properties properties;

}
