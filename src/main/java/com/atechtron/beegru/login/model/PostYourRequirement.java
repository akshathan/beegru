package com.atechtron.beegru.login.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.atechtron.beegru.website.model.User;

import lombok.Data;

@Entity
@Data
public class PostYourRequirement {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int postYourRequirementId;
	
	private String propertyLocation;
	
	private String propertyName;
	
	private String comments;
	
	private String requirement;
	
	private int activeStatus;
	
	@ManyToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "user_user_id")
	private User user;

}
