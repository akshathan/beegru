package com.atechtron.beegru.login.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.atechtron.beegru.website.model.User;

import lombok.Data;

@Data
@Entity
public class WishList {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int wishListId;
	
	@ManyToOne
	@JoinColumn(name = "properties_property_id")
	private Properties properties;
	
	@ManyToOne
	@JoinColumn(name = "user_user_id")
	private User user;

}
