package com.atechtron.beegru.login.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.Data;
import lombok.ToString;

@Entity
@Data
public class BeegruScore {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int beegruScoreId;
	
	private String legalAndBanksSupport = "NA";
	
	private String approvalStage = "NA";

	private String accesibility = "NA";
	
	private String vaastuAndLocation = "NA";
	
	private String priceValue = "NA";
	
	private String neighbourhood = "NA";
	
	private String appreciationPotential = "NA";
	
	private int addedUserId;
	
	@OneToOne
	@ToString.Exclude
	private Properties properties;
}
