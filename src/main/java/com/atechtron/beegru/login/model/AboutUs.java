package com.atechtron.beegru.login.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class AboutUs {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int aboutUsId;
	
	private String description;
	
	private String videoLink;
	
	private String addedDate;
	
	private int addedUserId;
	
	private String AddedUserMobileNumber;

}
