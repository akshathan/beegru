package com.atechtron.beegru.login.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.atechtron.beegru.website.model.User;

import lombok.Data;

@Entity
@Data
public class PropertyAssignDetails {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int propertyAssignedId;
	
	@ManyToOne
	@JoinColumn(name = "property_id")
	private Properties properties;
	
	@ManyToOne
    @JoinColumn(name = "user_user_id")
	private User user; // id who logged in
	
	//private String addedUserMobileNumber;
	
	@ManyToOne
	@JoinColumn(name = "staff_details_staff_id")
	private StaffDetails staffDetails;
	
	private int propertyAssignedStatus;
	
	private String propertyAssignedOn;
	
	private String propertyAssignedResultDate;

}
