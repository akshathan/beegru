package com.atechtron.beegru.login.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Entity
@Data
public class PropertyVicinity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int propertyVicinityId;
	
	private String propertyVicinityType;
	
	private String propertyVicinityDescription;
	
	@ManyToOne(cascade= {CascadeType.ALL})
	@JoinColumn(name = "properties_property_id")
	private Properties properties;

}
