package com.atechtron.beegru.login.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class PropertyEnquiry {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int propertyEnquiryId;
	
	private String name;
	
	private String mobileNumber;
	
	private String emailId;
	
	private String message;
	
	private int propertyId;
	
	private String addedDate;
	
	private int activeStatus;
}
