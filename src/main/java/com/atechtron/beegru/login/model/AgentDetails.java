package com.atechtron.beegru.login.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.atechtron.beegru.website.model.User;

import lombok.Data;

@Entity
@Data
public class AgentDetails {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int agentDetailsId;
	
	private String Speciality;
	
	private String location;
	
//	private int addedUserId;
	
	@OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "user_user_id")
	private User user;
	
	@ManyToOne(cascade= {CascadeType.REMOVE})
	@JoinColumn(name = "staff_details_staff_id")
	private StaffDetails staffDetails;

}
