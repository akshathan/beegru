package com.atechtron.beegru.login.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.ColumnDefault;

import com.atechtron.beegru.website.model.User;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/*rciwa-s -> rciwa for sale, rciwa-r -> rciwa for rent
 * r->Residential
 * c->commercial
 * i->industrial
 * w->warehouse
 * a->agricultural
 */ 



@Entity
@Data
public class Properties {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int propertyId; //rciwa-s, rciwa-r
	
	@Column(name = "property_name")
	private String propertyName = "NA";
	
	@Column(name = "property_location")
	private String propertyLocation = "NA";
	
	@Column(name = "property_landmark")
	private String propertyLandmark = "NA";
	
	//@Column(name = "propertySaleType", nullable = false, columnDefinition = "varchar() default NA")
	//private String propertySaleType; // apartment or land or plot or form house
	
	@Column(name = "property_type")
	private String propertyType = "NA";  // r or c or i or w or a
	
	@Column(name = "property_status")
	private String propertyStatus= "NA"; // rent or sale or find venture
	
	@Column(name = "short_description")
	private String shortDescription= "NA"; //rciwa-s, rciwa-r
	
	@Column(name = "status")
	private String status= "NA"; // new, resale, underConstruction, ready to move in, built to suit, bare shell(radio button) rciw-s,rciwa-r
	
	@Column(name = "type")
	private String type;
	
	@Column(name = "furnishing")
	private String furnishing= "NA"; // semi furnished, furnished, un furnished(radio buttons) rciwa-s, rcwa-r
	
	@Column(name = "bathrooms")
	private String bathrooms= "NA"; // number of bathrooms(drop down) can be int rciwa-s, rciwa-r
	
	@Column(name = "carpet_area")
	private int carpetArea; //  sub in Area rciwa-s, rciwa-r
	
	@Column(name = "super_builtup_area")
	private int superBuiltupArea; // sub in Area (sub for structure in agriculture also) rciwa-s, rciwa-r
	
	@Column(name = "length")
	private int length; // sub in land area rciwa-s, rciwa-r
	
	@Column(name = "breadth")
	private int breadth; // sub in land area rciwa-s, rciwa-r
	
	@Column(name = "road_facing_width")
	private String roadFacingWidth= "NA"; //rciwa-s can be int, riwa-r
	
	@Column(name = "co_living")
	private String coLiving = "NA";
	
	@Column(name = "water_supply")
	@ColumnDefault("NA")
	private String waterSupply = "NA"; // borewell, other, corporation(radiobutton) rca-s,a-r
	
	@Column(name = "ground_water_level")
	private int groundWaterLevel; //rciwa-s, rciwa-r
	
	@Column(name = "amenities")
	private String amenities= "NA"; // ac, gym, cctv, loundry, security, wifi, pool(check box) rciwa-s,rciwa-r
	
	@Column(name = "other_amenities")
	private String otherAmenities= "NA"; // rciwa-s,rciwa-r
	
	@Column(name = "about_location")
	private String aboutLocation= "NA"; // rciwa-s, rciw-r
	
	@Column(name = "about_property")
	private String aboutProperty= "NA"; // rciwa-s, rciw-r
	
	@Column(name = "other")
	private String other= "NA"; // rciwa-s, rciw-r // about others
	
	@Column(name = "pricing")
	private String pricing= "NA"; // rciwa-s, rciw-r
	 
	@Column(name = "rooms")
	private String rooms= "NA"; // ciwa-s (dropdown) can be int, rciwa-r
	
	@Column(name = "floor_number")
	private String floorNumber= "NA"; //ciwa-s (dropdown) can be int rciwa-r
	
	@Column(name = "balconies")
	private String balconies= "NA"; //ciwa-s (drop down) can be int rciwa-r
	
	@Column(name = "facing")
	private String facing= "NA"; // ciwa-s, rciwa-r
	
	@Column(name = "other_tag")
	private String otherTag= "NA"; // nri or mansion(dropdown) rciwa-s, rciwa-r (ex., sorrounded by plantation in aggriculture)
	
	@Column(name = "maintainance_charge")
	private String maintainanceCharge= "NA"; //iwa-s, iw-r
	
	@Column(name = "security_deposit")
	private String securityDeposit= "NA"; //iwa-s, iw-r
	
	@Column(name = "land_area")
	private String landArea= "NA"; // a-s can be int, a-r
	
	@Column(name = "kharad_area")
	private String kharadArea= "NA"; // a-s can be int, a-r
	
	@Column(name = "soil_type")
	private String soilType= "NA"; // a-s red or black a-r
	
	@Column(name = "land_type")
	private String landType= "NA"; // a-s dry or wet or highlands, a-r
	
	@Column(name = "irrigation_type")
	private String irrigationType= "NA"; //a-s (drip or sprikler or canal or pype), a-r
	
	@Column(name = "plantation")
	private String plantation= "NA";	//a-s, a-r
	
	@Column(name = "longi")
	private String longi= "NA"; 	//a-s sub in Elivation, a-r
	
	@Column(name = "lati")
	private String lati= "NA"; 	//a-s sub in Elivation, a-r
	
	@Column(name = "farmhouse")
	private String farmhouse= "NA"; //a-s yes or no, a-r
	
	@Column(name = "land_status")
	private String landStatus= "NA"; // a-s new or ready-to-move or built-to-suit, a-r
	
	@Column(name = "persons")
	private String persons= "NA"; // r-r (if co-living)
	
	@Column(name = "private_room")
	private String privateRoom= "NA"; // rc-r(private cabin in commmercial) 
	
	@Column(name = "shared_room")
	private String sharedRoom= "NA"; // rc-r(shared workspace in commmercial)
	
	@Column(name = "tenent_type")
	private String tenentType= "NA"; // rc-r (company, family, single, no-preference)
	
	@Column(name = "pet_friendly")
	private String petFriendly= "NA"; // rc-r(yes Or no)
	
	@Column(name = "agreement_period")
	private String agreementPeriod= "NA"; //rciw-r (3 Or 6 or 12 months)
	
	@Column(name = "preference")
	private String preference= "NA"; // rciw-r (veg or nonveg or no-preference)
	
	@Column(name = "tenent_habit_preference")
	private String tenentHabitPreference= "NA";// rciw-r (no-smoking, no-drinking, no-prefernece)
	
	@Column(name = "mezzanine")
	private String mezzanine= "NA"; // iw-r
	
	@Column(name = "farmhouse_carpet_area")
	private String farmhouseCarpetArea= "NA"; // a-r
	
	@Column(name = "farmhouse_super_builtup_arear")
	private String farmhouseSuperBuiltupArear= "NA"; //a-r
	
	@ManyToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "user_user_id")
	private User user;
	
	//@OneToMany(fetch = FetchType.LAZY,orphanRemoval = true)
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "properties_property_id")
	private List<WishList> wishList;
	 
	@OneToMany
	@JoinColumn(name = "property_id")
	private List<PropertyAssignDetails> propertyAssisgnDetails;
	
	@Column(name = "min_price")
	private double minPrice;
	
	@Column(name = "max_price")
	private double maxPrice;
	
	@Column(name = "display_price")
	private String displayPrice= "NA";
	
	@OneToMany(cascade={CascadeType.ALL})
	@JoinColumn(name = "properties_property_id")
	@ToString.Exclude
	private List<PropertyVicinity> propertyVicinity;
	
	@OneToMany(cascade={CascadeType.ALL})
	@JoinColumn(name = "properties_property_id")
	@ToString.Exclude
	private List<PropertyTags> propertyTags;
	
	@Column(name = "long_description")
	private String longDescription= "NA"; //rciwa-s, rciwa-r
	
	@Column(name = "added_date")
	private String addedDate;
	
	@Column(name = "active_status")
	private int activeStatus;
	
	@Column(name = "no_of_images")
	private int noOfImages;
	
	@Column(name = "approved_date")
	private String approvedDate;
	
	@Column(name = "featuredProperties")
	private int featuredProperties;
	
	@Column(name = "video_url")
	private String videoUrl = "NA";
	
	@ToString.Exclude
	@EqualsAndHashCode.Exclude 
	@OneToOne(mappedBy = "properties",cascade=CascadeType.ALL)
	private BeegruScore beegruScore;
	
	@OneToMany(cascade={CascadeType.ALL})
	@JoinColumn(name = "properties_property_id")
	@ToString.Exclude
	private List<PeopleTestimonials> peopleTestimonials;
}
