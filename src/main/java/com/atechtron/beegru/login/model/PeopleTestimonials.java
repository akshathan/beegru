package com.atechtron.beegru.login.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.ToString;

@Entity
@Data
public class PeopleTestimonials {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int peopleTestimonialsId;
	
	private String subject;
	
	private int ratings;
	
	private String testimonialName;
	
	private String testimonialAddress;
	
	private String date;
	
	private String videoLink;
	
	private String description;
	
	private String addedDate;
	
	private String addedUserMobileNumber;
	
	private int addedUserId;
	
	@ManyToOne
	@ToString.Exclude
	@JoinColumn(name = "properties_property_id")
	private Properties properties;
}
