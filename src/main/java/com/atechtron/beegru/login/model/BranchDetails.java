package com.atechtron.beegru.login.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.Data;
import lombok.ToString;

@Entity
@Data
public class BranchDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int branchId;
	
	private String branchName;
	
	private String branchPincode;
	
	private int branchStatus;
	
	private String branchAddedBy;
	
	private int addedUserId;
	
	private String branchAddeddate; 
	
//	@OneToOne(mappedBy = "branchDetails")
//	private EmployeeDetails employeeDetiails;
//	
	@ToString.Exclude
	@OneToMany(mappedBy = "branch")
	private List<ManagerDetails> managerDetails;
}
