package com.atechtron.beegru.login.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.atechtron.beegru.website.model.User;

import lombok.Data;

@Entity
@Data
public class ListYourProperty {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int yourPropertyId;
	
	private String propertyName;
	
	private String typeOfProperty;
	
	private String locationAddress;
	
	private String landmark;
	
	private String landmarkDescription;
	
	private String status;//(new,resale)
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_user_id")
	private User user;
}
