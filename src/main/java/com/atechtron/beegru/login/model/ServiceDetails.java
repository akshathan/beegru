package com.atechtron.beegru.login.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.atechtron.beegru.website.model.User;

import lombok.Data;

@Entity
@Data
public class ServiceDetails {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int serviceDetailsId;
	
	private String serviceType;
	
	private String serviceName;
	
	private String about;
	
	private String videoUrl;
	
	private String description;
	
	@ManyToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "user_user_id")
	private User user;
	//private String ratings;
}
