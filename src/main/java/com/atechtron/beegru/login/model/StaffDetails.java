package com.atechtron.beegru.login.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.atechtron.beegru.website.model.User;

import lombok.Data;
import lombok.ToString;

@Data
@Entity
public class StaffDetails {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int staffId;
	
	private String addedDate;
	
//	private int addedUserId;
	
	@OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "user_user_id")
	private User user;
	
	@ManyToOne(cascade=CascadeType.REMOVE)
	@JoinColumn(name = "manager_details_manager_id")
	private ManagerDetails managerDetails;
	
	@ToString.Exclude
	@OneToMany(mappedBy="staffDetails" , cascade=CascadeType.REMOVE)
   // @JoinColumn(name = "staff_details_staff_id")
	private List<AgentDetails> agentDetails;
	
	@ToString.Exclude
	@OneToMany(mappedBy="staffDetails" , cascade=CascadeType.REMOVE)
   // @JoinColumn(name = "staff_details_staff_id")
	private List<PropertyAssignDetails> propertyAssignDetails;

}
