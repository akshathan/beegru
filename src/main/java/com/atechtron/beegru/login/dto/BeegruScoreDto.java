package com.atechtron.beegru.login.dto;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.atechtron.beegru.login.model.Properties;

import lombok.Data;

@Data
public class BeegruScoreDto {
	
	private int beegruScoreId;
	
	private String legalAndBanksSupport;
	
	private String approvalStage;

	private String accesibility;
	
	private String vaastuAndLocation;
	
	private String priceValue;
	
	private String neighbourhood;
	
	private String appreciationPotential;
	
	private int  propertyId;
	
	private String propertyName;

}
