package com.atechtron.beegru.login.dto;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.atechtron.beegru.login.model.Properties;

import lombok.Data;

@Data
public class PropertyTagDto {

	private int propertyTagsId;
	
	private String tagName;
	
	private int position;
	
	private int propertyId;
	
	private String propertyName;
}
