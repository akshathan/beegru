package com.atechtron.beegru.login.dto;

import lombok.Data;

@Data
public class PropertyImageDto {
	
	private String propertyType;
	
	private String propertyStatus;

	public PropertyImageDto(String propertyType, String propertyStatus) {
		super();
		this.propertyType = propertyType;
		this.propertyStatus = propertyStatus;
	}

	public PropertyImageDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
