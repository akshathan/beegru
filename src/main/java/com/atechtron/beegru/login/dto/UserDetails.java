package com.atechtron.beegru.login.dto;

import lombok.Data;

@Data
public class UserDetails {
	
	private int userId;
	
	private String name;

    private String mobileNumber;

    private String password;
    
    private String emailId;
    
    private String location;
    
    private String address;
    
    private String role;
    
    private String addedBy;
    
    private int status;
    
    private String workingArea;
    
    private String assignedManagerName;
    
    private int numberOfPropertyUploaded;
    
    private String assignedStaff;
    
    private int staffId;
    
    private String passwordConfirm;
    
    private int workingAreaId;
    
    private int managerId;
    
    private int agentId;    
    
    private String agentSpeciality;
    
    private String agentExpectIn;
    
    private int addedUserId;

}
