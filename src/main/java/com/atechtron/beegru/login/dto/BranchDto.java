package com.atechtron.beegru.login.dto;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
public class BranchDto {
	
	private int branchId;
	
	private String branchName;
	
	private String branchPincode;
	
	private int branchStatus;
	
	private String branchAddedBy;
	
	private String branchAddeddate; 
	
	private int addedUserId;

}
