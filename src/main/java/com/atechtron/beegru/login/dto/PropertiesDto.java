package com.atechtron.beegru.login.dto;

import java.util.List;

import lombok.Data;

@Data
public class PropertiesDto {
	
	private int propertyId;

	private String propertyName;
	
	private String propertyLocation;
	
	private String propertyLandmark; 
	
	private String propertyType;
	
	private String propertyStatus;
	
	private String shortDescription;
	
	private String type;
	
	private String status;
	
	private String furnishing;
	
	private String bathrooms;
	
	private int carpetArea; 
	
	private int superBuiltupArea; 
	
	private int length; 
	
	private int breadth; 
	
	private String roadFacingWidth;
	
	private String coLiving;
	
	private String waterSupply ;

	private int groundWaterLevel; 
	
	private String amenities;
	
	private String otherAmenities;
	
	private String aboutLocation;
	
	private String aboutProperty;
	
	private String other;
	
	private String pricing;
	
	private String rooms;
	
	private String floorNumber;
	
	private String balconies;
	
	private String facing;
	
	private String otherTag;
	
	private String maintainanceCharge;
	
	private String securityDeposit;
	
	private String landArea;
	
	private String kharadArea;
	
	private String soilType;
	
	private String landType;
	
	private String irrigationType;

	private String plantation;
	
	private String longi;
	
	private String lati;
	
	private String farmhouse;
	
	private String landStatus;

	private String persons;
	
	private String privateRoom;
	
	private String sharedRoom;
	
	private String tenentType;
	
	private String petFriendly;

	private String agreementPeriod;
	
	private String preference;

	private String tenentHabitPreference;
	
	private String mezzanine;
	
	private String farmhouseCarpetArea;
	
	private String farmhouseSuperBuiltupArear;
		
	private double minPrice;
	
	private double maxPrice;
	
	private String displayPrice;
	
	private List<PropertyVicinityDto> propertyVicinity;
	
	private BeegruScoreDto beegruScore;
	
	private boolean propertyLiked;
	
	private String longDescription;
	
	private String addedDate;
	
	private int activeStatus;
	
	private String videoUrl;
	
	private int noOfImages;
	
}
