package com.atechtron.beegru.login.dto;

import java.util.List;

import lombok.Data;

@Data
public class StaffDto {
	
	private int staffId;
	
	private String staffName;
	
	private String assignedManagerName;
	
	private String managerWorkingArea;
	
	private List<PropertyInfoDto> propertyList;
	
	private String staffMobileNumber;
	
	private int propertyAssignedId;
	
	private String address;
}
