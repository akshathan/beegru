package com.atechtron.beegru.login.dto;

import lombok.Data;

@Data
public class PropertyAssignedDto {
	
	private int propertyAssignedId;
	
	private String staffName;
	
	private String managerName;
	
	private String managerWorkingArea;
	
	private String staffMobileNamber;
	
	private int staffId;
	
	private int propertyId;
	
	private String assignedUserMobileNumber;
	
	private String assignedUserName;
	
	private String assignedUserRole;

}
