package com.atechtron.beegru.login.dto;

import lombok.Data;

@Data
public class PropertyVicinityDto {
	
	private int propertyVicinityId;
	
	private String propertyVicinityType;
	
	private String propertyVicinityDescription;

}
