package com.atechtron.beegru.login.dto;

import javax.persistence.CascadeType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.atechtron.beegru.website.model.User;

import lombok.Data;

@Data
public class PostYourRequirementDto {
	
	private int postYourRequirementId;
	
	private String propertyLocation;
	
	private String propertyName;
	
	private String comments;
	
	private String requirement;
	
	private String userName;
	
	private int activeStatus;
	
	private String mobileNumber;
}

