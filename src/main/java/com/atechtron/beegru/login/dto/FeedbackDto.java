package com.atechtron.beegru.login.dto;

import lombok.Data;

@Data
public class FeedbackDto {
	
	private int contactUsId;
	
	private String name;
	
	private String mobileNumber;
	
	private String emailId;
	
	private String location;
	
	private String message;
	
	private String addedDate;
	
	private int viewStatus;

	public FeedbackDto(int contactUsId, String name, String mobileNumber, String emailId, String location,
			String message, String addedDate, int viewStatus) {
		super();
		this.contactUsId = contactUsId;
		this.name = name;
		this.mobileNumber = mobileNumber;
		this.emailId = emailId;
		this.location = location;
		this.message = message;
		this.addedDate = addedDate;
		this.viewStatus = viewStatus;
	}

	public FeedbackDto() {
		super();
	}	
}
