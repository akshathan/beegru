package com.atechtron.beegru.login.dto;

import lombok.Data;

@Data
public class PropertyInfoDto {
	
	private int propertyId;
	
	private String propertyName;
	
	private String propertyLocation;
	
	private String displayPrice;
	
	private String name; //addedUserName
	
	private String addedDate;
	
	private String mobileNumber;	
	
	private int activeStatus;
	
	private String propertyType;
	
	private String propertyStatus;
	
	private int featuredProperties;
	
	private String assignedStaffName;
	
	private int propertyAssignedId;
	
	private int staffId;
	
	private String role;
	
	private int noOfImages;
	
	private int assignedUserId; // the user who assigned staff

	public PropertyInfoDto(int propertyId, String propertyName, String propertyLocation, String displayPrice,
			String propertyType, String propertyStatus) {
		super();
		this.propertyId = propertyId;
		this.propertyName = propertyName;
		this.propertyLocation = propertyLocation;
		this.displayPrice = displayPrice;
		this.propertyType = propertyType;
		this.propertyStatus = propertyStatus;
	}



	public PropertyInfoDto() {
		super();
		// TODO Auto-generated constructor stub
	}

}
