package com.atechtron.beegru.login.dto;

import lombok.Data;

@Data
public class BeegruScore {
	
	private int beegruScoreId;
	
	private String legalAndBanksSupport;
	
	private String approvalStage;

	private String accesibility;
	
	private String vaastuAndLocation;
	
	private String priceValue;
	
	private String neighbourhood;
	
	private String appreciationPotential;

}
