package com.atechtron.beegru.login.dto;

import lombok.Data;

@Data
public class PropertyEnquiryDto {
	
	private int propertyEnquiryId;
	
	private String name;
	
	private String mobileNumber;
	
	private String emailId;
	
	private String message;
	
	private int propertyId;
	
	private String addedDate;
	
	private int activeStatus;

	public PropertyEnquiryDto(int propertyEnquiryId, String name, String mobileNumber, String emailId, String message,
			int propertyId, String addedDate, int activeStatus) {
		super();
		this.propertyEnquiryId = propertyEnquiryId;
		this.name = name;
		this.mobileNumber = mobileNumber;
		this.emailId = emailId;
		this.message = message;
		this.propertyId = propertyId;
		this.addedDate = addedDate;
		this.activeStatus = activeStatus;
	}

}
