package com.atechtron.beegru.login.dto;

import lombok.Data;

@Data
public class ManagerDto {
	
	private int managerId;
	
	private String managerName;
	
	private String workingArea;

}
