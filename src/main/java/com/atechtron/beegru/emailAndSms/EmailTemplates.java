package com.atechtron.beegru.emailAndSms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.atechtron.beegru.util.EmailUtil;

@Controller
public class EmailTemplates {

	@Autowired
	private EmailUtil emailUtil;

	public void contactUsEmail(String customerName, String customerMobileNumber, String customerEmail) {
		
		try {
			StringBuilder message = new StringBuilder();
			message.append("<div>Dear " + customerName + "," + "");
			message.append("<p></p>");
			message.append("<p>Greetings from BEEGRU.</p>");
			message.append("<p></p>");
			message.append("<p>Thank you for contacting us. Our representative will get in touch with you soon.</p>");
			message.append("<p></p>");
			message.append("<p>Warm Regards,<br>Team BEEGRU." + "</p>");
			message.append("<p></p>");
			message.append(
					"<p>This is an automatically generated email - Please do not reply to it. If you have looking for a faster response, write to us on <a href='mailto:mail@beegru.com'>mail@beegru.com</a>"
							+ "</p>");

			StringBuilder subject = new StringBuilder();
			subject.append("BEEGRU Real Estate Advisory and Marketing Firm.");

			String to[] = { customerEmail };
			String cc[] = {};
			String bcc[] = {};

			emailUtil.sendSimpleMail("", to, cc, bcc, subject.toString(), message.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void registrationEmail(String customerName, String customerMobileNumber, String customerEmail, String customerPassword, String customerRole) {
		System.out.println("inside emsil");
		try {
			StringBuilder message = new StringBuilder();
			message.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n" + 
					"<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n" + 
					"    <head>\r\n" + 
					"        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\r\n" + 
					"        <title>Beegru Email Template - Welcome</title>\r\n" + 
					"        <meta name=\"viewport\" content=\"width=device-width\" />\r\n" + 
					"       <style type=\"text/css\">\r\n" + 
					"            @media only screen and (max-width: 550px), screen and (max-device-width: 550px) {\r\n" + 
					"                body[yahoo] .buttonwrapper { background-color: transparent !important; }\r\n" + 
					"                body[yahoo] .button { padding: 0 !important; }\r\n" + 
					"                body[yahoo] .button a { background-color: #ff6b6b; padding: 15px 25px !important; }\r\n" + 
					"            }\r\n" + 
					"\r\n" + 
					"            @media only screen and (min-device-width: 601px) {\r\n" + 
					"                .content { width: 600px !important; }\r\n" + 
					"                .col387 { width: 387px !important; }\r\n" + 
					"            }\r\n" + 
					"        </style>\r\n" + 
					"    </head>\r\n" + 
					"    <body bgcolor=\"#3c3e4f\" style=\"margin: 0; padding: 0;\" yahoo=\"fix\">\r\n" + 
					"        <!--[if (gte mso 9)|(IE)]>\r\n" + 
					"        <table width=\"600\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\r\n" + 
					"          <tr>\r\n" + 
					"            <td>\r\n" + 
					"        <![endif]-->\r\n" + 
					"        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: collapse; width: 100%; max-width: 600px;\" class=\"content\">\r\n" + 
					"            <tr>\r\n" + 
					"                <td style=\"padding: 15px 10px 15px 10px;\">\r\n" + 
					"                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n" + 
					"                        <tr>\r\n" + 
					"                            <td align=\"center\" style=\"color: #aaaaaa; font-family: Arial, sans-serif; font-size: 12px;\">\r\n" + 
					"                                \r\n" + 
					"                            </td>\r\n" + 
					"                        </tr>\r\n" + 
					"                    </table>\r\n" + 
					"                </td>\r\n" + 
					"            </tr>\r\n" + 
					"            <tr>\r\n" + 
					"                <td align=\"center\" bgcolor=\"#ff6b6b\" style=\"padding: 20px 20px 20px 20px; color: #ffffff; font-family: Arial, sans-serif; font-size: 36px; font-weight: bold; background-color: black;\">\r\n" + 
					"                    <img src=\"https://www.aceimagingandvideo.com/assets/email/beegru.png\" alt=\"Beegru Logo\"  style=\"display:block;\" />\r\n" + 
					"                    Welcome\r\n" + 
					"                </td>\r\n" + 
					"            </tr>\r\n" + 
					"            <tr>\r\n" + 
					"                <td align=\"center\" bgcolor=\"#ffffff\" style=\"padding: 40px 20px 40px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;\">\r\n" + 
					"                    <b>Dear "+customerName+",</b><br/>\r\n" + 
					"					<b>Thank you very much for choosing Beegru!</b><br/>\r\n" + 
					"                    Let us show you some cool features before we get started.\r\n" + 
					"                </td>\r\n" + 
					"            </tr>\r\n" + 
					"            <tr>\r\n" + 
					"                <td bgcolor=\"#ffffff\" style=\"padding: 20px 20px 0 20px; border-bottom: 1px solid #f6f6f6;\">\r\n" + 
					"                    <table width=\"128\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n" + 
					"                        <tr>\r\n" + 
					"                            <td height=\"128\" style=\"padding: 0 20px 20px 0;\">\r\n" + 
					"                                <img src=\"https://www.aceimagingandvideo.com/assets/email/icon1.png\" alt=\"Icon #1\" width=\"128\" height=\"128\" style=\"display: block;\" />\r\n" + 
					"                            </td>\r\n" + 
					"                        </tr>\r\n" + 
					"                    </table>\r\n" + 
					"                    <!--[if (gte mso 9)|(IE)]>\r\n" + 
					"                      <table width=\"387\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\r\n" + 
					"                        <tr>\r\n" + 
					"                          <td>\r\n" + 
					"                    <![endif]-->\r\n" + 
					"                    <table class=\"col387\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; max-width: 387px;\">\r\n" + 
					"                        <tr>\r\n" + 
					"                            <td>\r\n" + 
					"                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n" + 
					"                                    <tr>\r\n" + 
					"                                        <td style=\"padding: 0 0 20px 0; color: #555555; font-family: Arial, sans-serif; font-size: 15px; line-height: 24px;\">\r\n" + 
					"                                            Beegru wants to connect real estate clients worldwide by creating a powerful online platform for experiencing properties. We will leverage our connections with real estate experts, strategist, builders, agents and lawyers to simplify realty transactions. We will tap the power of tools like social media, digital marketing, online portals and community.\r\n" + 
					"                                        </td>\r\n" + 
					"                                    </tr>\r\n" + 
					"                                </table>\r\n" + 
					"                            </td>\r\n" + 
					"                        </tr>\r\n" + 
					"                    </table>\r\n" + 
					"                    <!--[if (gte mso 9)|(IE)]>\r\n" + 
					"                          </td>\r\n" + 
					"                        </tr>\r\n" + 
					"                    </table>\r\n" + 
					"                    <![endif]-->\r\n" + 
					"                </td>\r\n" + 
					"            </tr>\r\n" + 
					"            \r\n" + 
					"            <tr>\r\n" + 
					"                <td bgcolor=\"#ffffff\" style=\"padding: 20px 20px 0 20px; border-bottom: 1px solid #f6f6f6;\">\r\n" + 
					"                    <table width=\"128\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n" + 
					"                        <tr>\r\n" + 
					"                            <td height=\"128\" style=\"padding: 0 20px 20px 0;\">\r\n" + 
					"                                <img src=\"https://www.aceimagingandvideo.com/assets/email/icon3.png\" alt=\"Icon #3\" width=\"128\" height=\"128\" style=\"display: block;\" />\r\n" + 
					"                            </td>\r\n" + 
					"                        </tr>\r\n" + 
					"                    </table>\r\n" + 
					"                    <!--[if (gte mso 9)|(IE)]>\r\n" + 
					"                      <table width=\"387\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\r\n" + 
					"                        <tr>\r\n" + 
					"                          <td>\r\n" + 
					"                    <![endif]-->\r\n" + 
					"                    <table class=\"col387\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; max-width: 387px;\">\r\n" + 
					"                        <tr>\r\n" + 
					"                            <td>\r\n" + 
					"                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n" + 
					"                                    <tr>\r\n" + 
					"                                        <td style=\"padding: 0 0 20px 0; color: #555555; font-family: Arial, sans-serif; font-size: 15px; line-height: 24px;\">\r\n" + 
					"                                            Beegru wants to become the most customer centric platform for facilitating seamless real estate transactions worldwide.\r\n" + 
					"                                        </td>\r\n" + 
					"                                    </tr>\r\n" + 
					"                                </table>\r\n" + 
					"                            </td>\r\n" + 
					"                        </tr>\r\n" + 
					"                    </table>\r\n" + 
					"                    <!--[if (gte mso 9)|(IE)]>\r\n" + 
					"                          </td>\r\n" + 
					"                        </tr>\r\n" + 
					"                    </table>\r\n" + 
					"                    <![endif]-->\r\n" + 
					"                </td>\r\n" + 
					"            </tr>\r\n" + 
					"            <tr>\r\n" + 
					"                <td align=\"center\" bgcolor=\"#f9f9f9\" style=\"padding: 30px 20px 30px 20px; font-family: Arial, sans-serif;\">\r\n" + 
					"                    <table bgcolor=\"#ff6b6b\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"buttonwrapper\">\r\n" + 
					"                        <tr>\r\n" + 
					"                            <td align=\"center\" height=\"50\" style=\" padding: 0 25px 0 25px; font-family: Arial, sans-serif; font-size: 16px; font-weight: bold;\" class=\"button\">\r\n" + 
					"                                <a href=\"http://beegru.com/\" target=\"_blank\" style=\"color: #ffffff; text-align: center; text-decoration: none;\">Get Started</a>\r\n" + 
					"                            </td>\r\n" + 
					"                        </tr>\r\n" + 
					"                    </table>\r\n" + 
					"                </td>\r\n" + 
					"            </tr>\r\n" + 
					"            <tr>\r\n" + 
					"                <td align=\"center\" bgcolor=\"#dddddd\" style=\"padding: 15px 10px 15px 10px; color: #555555; font-family: Arial, sans-serif; font-size: 12px; line-height: 18px;\">\r\n" + 
					"                    <b>Company Inc.</b><br/>1090I, 3rd Floor, 18th Cross Rd, 3rd Sector, HSR Layout, Bengaluru, Karnataka 560102\r\n" + 
					"                </td>\r\n" + 
					"            </tr>\r\n" + 
					"            <tr>\r\n" + 
					"                <td style=\"padding: 15px 10px 15px 10px;\">\r\n" + 
					"                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n" + 
					"                        <tr>\r\n" + 
					"                            <td align=\"center\" width=\"100%\" style=\"color: #999999; font-family: Arial, sans-serif; font-size: 12px;\">\r\n" + 
					"                                2020 - 21&copy; <a href=\"http://beegru.com/\" style=\"color: #ff6b6b;\">Beegru</a>\r\n" + 
					"                            </td>\r\n" + 
					"                        </tr>\r\n" + 
					"                    </table>\r\n" + 
					"                </td>\r\n" + 
					"            </tr>\r\n" + 
					"        </table>\r\n" + 
					"        <!--[if (gte mso 9)|(IE)]>\r\n" + 
					"                </td>\r\n" + 
					"            </tr>\r\n" + 
					"        </table>\r\n" + 
					"        <![endif]-->\r\n" + 
					"    </body>\r\n" + 
					"</html>");
			StringBuilder subject = new StringBuilder();
			subject.append("Welcome to BEEGRU Real Estate Advisory and Marketing Firm.");

			String to[] = { customerEmail };
			String cc[] = {};
			String bcc[] = {};

			emailUtil.sendSimpleMail("akshatha.n15@gmail.com", to, cc, bcc, subject.toString(), message.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void forgetPasswordEmail(String customerName, String customerMobileNumber, String customerEmail, String customerPassword) {
		try {
			StringBuilder message = new StringBuilder();
			message.append("<div>Dear " + customerName + "," + "");
			message.append("<p></p>");
			message.append("<p>Greetings from BEEGRU.</p>");
			message.append("<p></p>");
			message.append("<p>Please login with your updated Login Credentials:</p>");
			message.append("<p></p>");
			message.append("<p>Login Id: "+customerMobileNumber+"</p>");
			message.append("<p></p>");
			message.append("<p>Password: "+customerPassword+"</p>");
			message.append("<p></p>");
			message.append("<p>Best Regards,<br>Team BEEGRU." + "</p>");
			message.append("<p></p>");
			message.append(
					"<p>This is an automatically generated email - Please do not reply to it. Should you have any questions regarding your updated login credentials, write to us on <a href='mailto:mail@beegru.com'>mail@beegru.com</a>"
							+ "</p>");

			StringBuilder subject = new StringBuilder();
			subject.append("BEEGRU UPDATED LOGIN CREDENTIALS");

			String to[] = { customerEmail };
			String cc[] = {};
			String bcc[] = {};

			emailUtil.sendSimpleMail("akshatha.n15@gmail.com", to, cc, bcc, subject.toString(), message.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void afterPostEquirementEmail(String customerName, String customerEmail) {
		
	}
	
	public void afterUploadPropertyEmail(String customerName, String customerEmail) {
		
	}
}
