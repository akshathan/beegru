package com.atechtron.beegru.emailAndSms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.atechtron.beegru.util.SmsUtil;
@Component
public class SmsTemplates {

	@Autowired
	private SmsUtil smsUtil;
	
	public void contactUsSMS(String customerName, String customerMobileNumber, String customerEmail) {
		smsUtil.sendSms(customerMobileNumber,
				"Thank you for contacting us. Our representative will get in touch with you soon.Team BEEGRU.");
	}
	
	public void registrationSMS(String customerName, String customerMobileNumber, String customerEmail, String customerPassword, String customerRole) {
		smsUtil.sendSms(customerMobileNumber,
				"Welcome to BEEGRU family, You can login from our portal www.beegru.com Your user id "+customerMobileNumber+" and password "+customerPassword+" for\n"
						+ " Team BEEGRU");
	}
	
	public void forgetPasswordSMS(String customerMobileNumber, String customerPassword) {
		smsUtil.sendSms(customerMobileNumber, "Your new password request for BEEGRU web application is processed successfully."
				+ " Your new password is " +customerPassword+"\n"
						+ " Team BEEGRU.");
	}
	
	public void sendSmsForOtp(String mobileNumber, String otp) {
		System.out.println("inside send otp");
		try {
			smsUtil.sendSms(mobileNumber, "" + otp
					+ " is the One Time Password for registering yourself for Beegru. Please do not share this with others.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public void afterPostEquirementSMS(String customerName, String customerEmail) {
		
	}
	
	public void afterUploadPropertySMS(String customerName, String customerEmail) {
		
	}
}
