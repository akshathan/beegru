package com.atechtron.beegru.website.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class ContactUs {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int contactUsId;
	
	private String name;
	
	private String mobileNumber;
	
	private String emailId;
	
	private String location;
	
	private String message;
	
	private String addedDate;
	
	private int viewStatus;

}
