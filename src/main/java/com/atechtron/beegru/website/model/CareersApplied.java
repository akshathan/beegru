package com.atechtron.beegru.website.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
public class CareersApplied {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int careersApplicationId;

	private String name;
	
	private String qualification;
	
	private String experience;
	
	private String email;
	
	private String contactNumber;
	
	private String comment;
	
	private String addedDate;
	
	@ManyToOne
	@JoinColumn(name = "careersAvailable_career_id")
	private CareersAvailable careersAvailable;
}
