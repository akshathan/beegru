package com.atechtron.beegru.website.model;

import lombok.Data;

@Data
public class PropertyPosition {
	
	private int propertyPositionId;
	
	private int propertyId;
	
	private int position;
	
	private String tagName;

}
