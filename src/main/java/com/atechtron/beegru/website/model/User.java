package com.atechtron.beegru.website.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.atechtron.beegru.login.model.AgentDetails;
import com.atechtron.beegru.login.model.ListYourProperty;
import com.atechtron.beegru.login.model.ManagerDetails;
import com.atechtron.beegru.login.model.PostYourRequirement;
import com.atechtron.beegru.login.model.Properties;
import com.atechtron.beegru.login.model.PropertyAssignDetails;
import com.atechtron.beegru.login.model.StaffDetails;
import com.atechtron.beegru.login.model.WishList;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name = "user")
@Data
public class User {
	 
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
	    private int userId;
		
		private String name;

	    private String mobileNumber;

	    private String password;
	    
	    private String emailId;
	    
	    private String location;
	    
	    private String aboutme;
	    
	    private String message;
	    
	    private String address;
	    
	    private String passwordConfirm;
	    
	    private String addedUserMobileNumber;
	    
	    private int addedUserId;
	    
	    @ToString.Exclude
	    @EqualsAndHashCode.Exclude 
	    @OneToOne(mappedBy = "user",cascade=CascadeType.ALL)
	    private ManagerDetails managerDetails;
	    
	    @ToString.Exclude
	    @EqualsAndHashCode.Exclude 
	    @OneToOne(mappedBy = "user",cascade=CascadeType.ALL)
	    private StaffDetails staffDetails;
	    
	    @ToString.Exclude
	    @EqualsAndHashCode.Exclude 
	    @OneToOne(mappedBy = "user",cascade=CascadeType.ALL)
	    private AgentDetails agentDetails;

	    @ToString.Exclude
	    @EqualsAndHashCode.Exclude 
		@ManyToMany
	    private Set<Role> roles;
	   
	    @OneToMany(cascade={CascadeType.REMOVE})
	    @JoinColumn(name = "user_user_id")
	    private List<Properties> property;
	    
	    @OneToMany
	    @JoinColumn(name = "user_user_id")
	    private List<ListYourProperty> listYourProrety;
	    
	    @OneToMany
	    @JoinColumn(name = "user_user_id")
	    private List<WishList> wishList;
	    
	    private int activeStatus;
	    
	    private String addedDate;
	    	    
	    @OneToMany
	    @JoinColumn(name = "user_user_id")
	    private List<PropertyAssignDetails> propertyAssisgnDetails;
	    
	    @OneToMany
	    @JoinColumn(name = "user_user_id")
	    private List<PostYourRequirement> postYourRequirement;
    
}
