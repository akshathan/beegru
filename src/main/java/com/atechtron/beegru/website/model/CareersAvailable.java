package com.atechtron.beegru.website.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import lombok.Data;

@Entity
@Data
public class CareersAvailable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int careerId;
	
	private String careerTitle;
	
	private String careerDescription;
	
	private String CareerType;
	
	@OneToMany
	@JoinColumn(name = "careersAvailable_career_id")
	private List<CareersApplied> careersApplied;
}
