package com.atechtron.beegru.website.restController;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.atechtron.beegru.login.dto.PropertiesDto;
import com.atechtron.beegru.login.dto.PropertyInfoDto;
import com.atechtron.beegru.login.model.PropertyEnquiry;
import com.atechtron.beegru.login.service.LoginService;
import com.atechtron.beegru.website.dto.LocationDto;
import com.atechtron.beegru.website.dto.LoginResponseDto;
import com.atechtron.beegru.website.dto.ResponseDto;
import com.atechtron.beegru.website.dto.WebsiteIndexDto;
import com.atechtron.beegru.website.model.ContactUs;
import com.atechtron.beegru.website.model.User;
import com.atechtron.beegru.website.service.WebsiteService;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(value = "/website")
public class RestFullController {

	@Autowired
	private WebsiteService websiteService;
	
	@Autowired
	LoginService loginService;
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/sendOtp" , method = RequestMethod.POST)
	public ResponseDto saveOtp(@RequestBody String jsonString)
	{
		ResponseDto response = new ResponseDto();
		try {
			response = websiteService.saveOtp(jsonString);
			return response;
		}catch(Exception e){
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
			return response;
		}
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/verifyOtp" , method = RequestMethod.POST)
	public ResponseDto verifyOtp(@RequestBody String jsonString)
	{
		ResponseDto response = new ResponseDto();
		try {
			response = websiteService.verifyOtp(jsonString);
			return response;
		}catch(Exception e){
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
			return response;
		}
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/resendOtp" , method = RequestMethod.POST)
	public String resendOtp(@RequestBody String jsonString)
	{
		try {
			websiteService.resendOtp(jsonString);
		}catch(Exception e){
			e.printStackTrace();
		}
		return jsonString;
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public ResponseDto register(@RequestBody User user){
		ResponseDto response = new ResponseDto();
		System.out.println(user);
		try {
			user.setActiveStatus(3);
			user.setPasswordConfirm(user.getPassword());
			response = websiteService.saveUser(user);
			return response;
		}catch(Exception e) {
			e.printStackTrace();
			return response;
		}
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public LoginResponseDto userLogin(@RequestBody String jsonString) {
		LoginResponseDto response = new LoginResponseDto();
		try {
			response = websiteService.login(jsonString);
			return response;
		}catch(Exception e) {
			e.printStackTrace();
			return response;
		}
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "saveContactUs", method = RequestMethod.POST)
	public ResponseDto saveContactUs(@RequestBody ContactUs contactus) {
	//	System.out.println(jsonString);
		ResponseDto response = new ResponseDto();
		try {
			response = websiteService.saveContactUs(contactus);
			return response;
		}catch(Exception e) {
			e.printStackTrace();
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return response;
		}
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "resetPassword", method = RequestMethod.POST)
	public ResponseDto resetPassword(@RequestBody String jsonString) {
		ResponseDto response = new ResponseDto();
		try {
			response = websiteService.resetPassword(jsonString);
		}catch(Exception e) {
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
			return response;
		}
		return response;
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/fetchIndexInfo", method = RequestMethod.POST)
	public WebsiteIndexDto fetchIndexInfo(@RequestBody String jsonString) {
		WebsiteIndexDto websiteIndexDto = new WebsiteIndexDto();
		try {
			
			websiteIndexDto = websiteService.fetchIndexInfo(jsonString);
			System.out.println("inside authentiaction null");
		}catch(Exception e) {
			e.printStackTrace();
		}
		return websiteIndexDto;
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/loginWithOtp", method = RequestMethod.POST)
	public ResponseDto loginWithOtp(@RequestBody String jsonString) {
		ResponseDto response = new ResponseDto();
		try {
			response = websiteService.loginWithOtp(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/propertiesBasedOnSearch", method = RequestMethod.POST)
	public List<PropertiesDto> fetchSearchedProperties(@RequestBody String jsonString){
		List<PropertiesDto> propertyList = new ArrayList<PropertiesDto>();
		try {
			propertyList = websiteService.fetchSearchedProperties(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyList;
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/propertiesBasedOnFilters", method = RequestMethod.POST)
	public List<PropertiesDto> fetchPropertiesBasedOnFilters(@RequestBody String jsonString){
		System.out.println("inside properties Based ON filters");
		System.out.println(jsonString);
		List<PropertiesDto> propertiesDtoList = new ArrayList<PropertiesDto>();
		try {
			propertiesDtoList = websiteService.fetchPropertiesBasedOnFilters(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertiesDtoList;
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/propertiesBasedOnPincode", method = RequestMethod.POST)
	public List<PropertiesDto> fetchpropertiesBasedOnPincode(@RequestBody String jsonString){
		List<PropertiesDto> propertyList = new ArrayList<PropertiesDto>();
		try {
			propertyList = websiteService.fetchpropertiesBasedOnPincode(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyList;
	}
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/fetchSingleProperties", method = RequestMethod.POST)
	public PropertiesDto fetchSingleProperty(@RequestBody String jsonString) {
		PropertiesDto propertiesDto = new PropertiesDto();
		try {
			propertiesDto = loginService.fetchSingleProperty(jsonString,2);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertiesDto;
	}
	
	@RequestMapping(value = "/fetchLocation", method = RequestMethod.POST)
	public Set<String> locationList(@RequestBody String jsonString){
		Set<String> locationList = new HashSet<String>();
		Set<String> locationSet = new HashSet<String>();
		try {
			locationList = loginService.fetchLocations(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return locationList;
	}
	
	@RequestMapping(value = "/savePropertyEnquiry", method = RequestMethod.POST)
	public ResponseDto savePropertyEnquire(@RequestBody PropertyEnquiry propertyEnquiry) {
		ResponseDto response = new ResponseDto();
		try {
			response =  websiteService.savePropertyEnquiry(propertyEnquiry);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = "/saveAgent", method = RequestMethod.POST)
	public ResponseDto saveAgent(@RequestBody User user) {
		ResponseDto response = new ResponseDto();
		try {
			//int userId = loginService.findUserIdByMobileNumber(authentication.getName());
			user.setPasswordConfirm(user.getPassword());
			//user.setAddedUserId(userId);
			//user.setAddedUserMobileNumber(authentication.getName());
			response = loginService.saveAgent(user);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = "/getSimilerProperties", method = RequestMethod.POST)
	public List<PropertiesDto> getSimilerProperties(@RequestBody String jsonString){
		List<PropertiesDto> propertiesDto = new ArrayList<PropertiesDto>();
		try {
			propertiesDto = websiteService.getSimilerProperties(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertiesDto;
	}
	
	@RequestMapping(value = "/getPropertiesOnlyBasedOnLocation", method = RequestMethod.POST)
	public List<PropertiesDto> getPropertiesOnlyBasedOnLocation(@RequestBody String jsonString){
		List<PropertiesDto> propertiesDto = new ArrayList<PropertiesDto>();
		try {
			propertiesDto = websiteService.getPropertiesOnlyBasedOnLocation(jsonString);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertiesDto;
	}
}
