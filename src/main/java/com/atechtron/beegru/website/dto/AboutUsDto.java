package com.atechtron.beegru.website.dto;

import lombok.Data;

@Data
public class AboutUsDto {
	
	private int aboutUsId;
	
	private String description;
	
	private String videoLink;

	public AboutUsDto(int aboutUsId, String description, String videoLink) {
		super();
		this.aboutUsId = aboutUsId;
		this.description = description;
		this.videoLink = videoLink;
	}

	public AboutUsDto() {
		super();
		// TODO Auto-generated constructor stub
	}
}
