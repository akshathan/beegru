package com.atechtron.beegru.website.dto;

import java.util.ArrayList;
import java.util.List;

import com.atechtron.beegru.login.dto.PropertyInfoDto;

import lombok.Data;

@Data
public class WebsiteIndexDto {
	
	private List<PropertyDetailsDto> featuredPropertiesList = new ArrayList<PropertyDetailsDto>();
	
	private List<TestimonialsDto> testimonialsList = new ArrayList<TestimonialsDto>();
	
	private AboutUsDto aboutUsDto;
	
	private List<PropertyDetailsDto> newPropertiesList = new ArrayList<PropertyDetailsDto>();
}
