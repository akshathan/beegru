package com.atechtron.beegru.website.dto;

import lombok.Data;

@Data
public class ResponseDto {

	private int resultStatus;
	
	private int httpStatus;
	
	private String userName;
	
	private int propertyId;
}
