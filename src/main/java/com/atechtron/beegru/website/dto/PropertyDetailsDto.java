package com.atechtron.beegru.website.dto;

import lombok.Data;

@Data
public class PropertyDetailsDto {
	private int propertyId;
	private String propertyType;
	private boolean propertyLiked;
	private String propertyAmount;
	private String propertyName;
	private String propertyAddress;
	private String squareFeet;
	private String rooms;
	private String propertyStatus;
	// Still more fields are pending
}
