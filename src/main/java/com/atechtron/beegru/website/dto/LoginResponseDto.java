package com.atechtron.beegru.website.dto;

import lombok.Data;

@Data
public class LoginResponseDto {
	
	private int resultStatus;
	
	private int httpStatus;
	
	private String userName;
	
	private String mobileNumber;

}
