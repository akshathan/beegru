package com.atechtron.beegru.website.dto;

import lombok.Data;

@Data
public class TestimonialsDto {

	private int peopleTestimonialsId;
	
	private String subject;
	
	private int ratings;
	
	private String testimonialName;
	
	private String testimonialAddress;
	
	private String date;
	
	private String videoLink;
	
	private String description;
	
	private int propertyId;
	
	private String propertyName;
	
	private String addedDate;

	public TestimonialsDto(int peopleTestimonialsId, String subject, int ratings, String testimonialName,
			String testimonialAddress, String videoLink, String description, String addedDate) {
		super();
		this.peopleTestimonialsId = peopleTestimonialsId;
		this.subject = subject;
		this.ratings = ratings;
		this.testimonialName = testimonialName;
		this.testimonialAddress = testimonialAddress;
		this.videoLink = videoLink;
		this.description = description;
		this.addedDate = addedDate;
	}

	public TestimonialsDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
