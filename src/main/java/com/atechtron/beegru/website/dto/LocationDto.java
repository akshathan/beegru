package com.atechtron.beegru.website.dto;

import lombok.Data;

@Data
public class LocationDto {
	
	private int productId;
	
	private String locationName;

}
