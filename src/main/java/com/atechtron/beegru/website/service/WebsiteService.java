package com.atechtron.beegru.website.service;

import java.util.List;

import com.atechtron.beegru.login.dto.FeedbackDto;
import com.atechtron.beegru.login.dto.PropertiesDto;
import com.atechtron.beegru.login.model.PropertyEnquiry;
import com.atechtron.beegru.website.dto.LoginResponseDto;
import com.atechtron.beegru.website.dto.ResponseDto;
import com.atechtron.beegru.website.dto.WebsiteIndexDto;
import com.atechtron.beegru.website.model.ContactUs;
import com.atechtron.beegru.website.model.User;

public interface WebsiteService {


	ResponseDto saveUser(User user);

	LoginResponseDto login(String jsonString);

	ResponseDto saveContactUs(ContactUs contactUs);

	ResponseDto resetPassword(String jsonString);

	List<FeedbackDto> fetchFeedbacks();

	ResponseDto saveOtp(String jsonString);

	ResponseDto verifyOtp(String jsonString);

	List<String> resendOtp(String jsonString);

	ResponseDto findByEmailId(String emailId);

	WebsiteIndexDto fetchIndexInfo(String string);

	ResponseDto loginWithOtp(String jsonString);

	List<PropertiesDto> fetchSearchedProperties(String jsonString);

	List<PropertiesDto> fetchPropertiesBasedOnFilters(String jsonString);

	ResponseDto savePropertyEnquiry(PropertyEnquiry propertyEnquiry);

	List<PropertiesDto> fetchpropertiesBasedOnPincode(String jsonString);

	List<PropertiesDto> getSimilerProperties(String jsonString);

	List<PropertiesDto> getPropertiesOnlyBasedOnLocation(String jsonString);

}
