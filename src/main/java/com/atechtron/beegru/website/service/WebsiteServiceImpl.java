package com.atechtron.beegru.website.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import com.atechtron.beegru.auth.service.UserService;
import com.atechtron.beegru.emailAndSms.EmailTemplates;
import com.atechtron.beegru.emailAndSms.SmsTemplates;
import com.atechtron.beegru.entitytodtoconverter.DtoConverter;
import com.atechtron.beegru.login.dto.FeedbackDto;
import com.atechtron.beegru.login.dto.PropertiesDto;
import com.atechtron.beegru.login.dto.PropertyImageDto;
import com.atechtron.beegru.login.model.PeopleTestimonials;
import com.atechtron.beegru.login.model.Properties;
import com.atechtron.beegru.login.model.PropertyEnquiry;
import com.atechtron.beegru.repository.AboutUsRepository;
import com.atechtron.beegru.repository.ContactUsRepository;
import com.atechtron.beegru.repository.OtpRepository;
import com.atechtron.beegru.repository.PeopleTestimonialsRepository;
import com.atechtron.beegru.repository.PropertyEnquiryRepository;
import com.atechtron.beegru.repository.PropertyRepository;
import com.atechtron.beegru.repository.UserRepository;
import com.atechtron.beegru.util.Util;
import com.atechtron.beegru.website.dto.AboutUsDto;
import com.atechtron.beegru.website.dto.LoginResponseDto;
import com.atechtron.beegru.website.dto.PropertyDetailsDto;

import com.atechtron.beegru.website.dto.ResponseDto;
import com.atechtron.beegru.website.dto.TestimonialsDto;
import com.atechtron.beegru.website.dto.WebsiteIndexDto;
import com.atechtron.beegru.website.model.ContactUs;
import com.atechtron.beegru.website.model.OtpVerification;
import com.atechtron.beegru.website.model.User;

@Service
public class WebsiteServiceImpl implements WebsiteService {

	@Autowired
	private UserService userService;

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ContactUsRepository contactUsRepository;
	
	@Autowired 
	private EmailTemplates emailTemplates;
	
	@Autowired
	private SmsTemplates smsTemplates;
	
	@Autowired
	private OtpRepository otpRepository;
	
	@Autowired
	private PropertyRepository propertyRepository;
	
	@Autowired
	private AboutUsRepository aboutUsRepository;
	
	@Autowired
	private PeopleTestimonialsRepository peopleTestimonialRepository;
	
	@Autowired
	private PropertyEnquiryRepository propertyEnquiryRepository;
	
	@Override
	public ResponseDto saveOtp(String jsonString) {
		ResponseDto response = new ResponseDto();
		OtpVerification OtpVerification = new OtpVerification();
		JSONObject jsonObj = null;
		try {
			jsonObj = new JSONObject(jsonString);
			User user = userRepository.findByMobileNumber(jsonObj.getString("mobileNumber"));
			if(user == null) {
				OtpVerification.setMobileNumber(jsonObj.getString("mobileNumber"));
				String otp = Util.randomNumberForOtp();
				OtpVerification.setOtp(otp);
				otpRepository.save(OtpVerification);
				smsTemplates.sendSmsForOtp(jsonObj.getString("mobileNumber"), otp);
				response.setResultStatus(1);
			}else {
				response.setResultStatus(2);
			}
			response.setHttpStatus(HttpServletResponse.SC_OK);
			return response;
		}catch(Exception e) {
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
			return response;
		}
	}
	
	@Override
	public ResponseDto verifyOtp(String jsonString) {
		JSONObject jsonObj = null;
		ResponseDto response = new ResponseDto();
		try {
			jsonObj = new JSONObject(jsonString);
			List<String> otp = otpRepository.findOtpByMobileNumber(jsonObj.getString("mobileNumber"));
			if(otp.size()!=0) {
				if(otp.get(0).equals(jsonObj.getString("recievedOtp"))) {
					response.setResultStatus(1);
					response.setHttpStatus(HttpServletResponse.SC_OK);
				}else {
					response.setResultStatus(2);
					response.setHttpStatus(HttpServletResponse.SC_OK);
				}
			}else {
				response.setResultStatus(2);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}
			return response;
		}catch(Exception e){
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
			return response;
		}
	}
	
	@Override
	public List<String> resendOtp(String jsonString) {
		List<String> otp=null;
		try {
			JSONObject jsonObj=new JSONObject(jsonString);
			otp  = otpRepository.findOtpByMobileNumber(jsonObj.getString("mobileNumber"));
			smsTemplates.sendSmsForOtp(jsonObj.getString("mobileNumber"), otp.get(0));
			return otp;
		}catch(Exception e){
			e.printStackTrace();
			return otp;
		}
	}

	@Override
	public ResponseDto saveUser(User user) {
		ResponseDto response = new ResponseDto();
		//String password = user.getPassword();
		try {
			User finduser = userRepository.findByEmailId(user.getEmailId());
			if (finduser == null) {
				user.setName(Util.nameFormat(user.getName()));
				userService.save(user);
				//userRepository.saveAndFlush(user);
				smsTemplates.registrationSMS(user.getName(), user.getMobileNumber(), user.getEmailId(), user.getPasswordConfirm(), "Customer");
				emailTemplates.registrationEmail(user.getName(), user.getMobileNumber(), user.getEmailId(),user.getPasswordConfirm(), "Customer");
				response.setResultStatus(1);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			} else {
				response.setResultStatus(2);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}
			return response;
		} catch (Exception e) {
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
			return response;
		}
	}

	@Override
	public LoginResponseDto login(String jsonString) {
		LoginResponseDto response = new LoginResponseDto();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			UserDetails userDetails = userDetailsService.loadUserByUsername(jsonObj.getString("mobileNumber")); // find
																												// by
																												// mobileNumber
			UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
					userDetails, jsonObj.getString("password"), userDetails.getAuthorities());
			authenticationManager.authenticate(usernamePasswordAuthenticationToken);
			if (usernamePasswordAuthenticationToken.isAuthenticated()) {
				SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
				response.setResultStatus(1); // valid
				User user = userRepository.findUserByMobileNumber(jsonObj.getString("mobileNumber"));
				response.setUserName(user.getName());
				response.setMobileNumber(user.getMobileNumber());
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}
		} catch (Exception e) {
			System.out.println("user Not found exp"+e.getMessage());
			if(e.getMessage().equals("invalid MobileNumber")) {
				response.setResultStatus(2);
			}else if(e.getMessage().equals("Bad credentials")) {
				response.setResultStatus(2);
			}else if(e.getMessage().equals("Not a Valid User")) {
				response.setResultStatus(3);
			}
			System.out.println(e.getMessage());
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
		}
		System.out.println(response);
		return response;
	}
	
	// fileName-> name of the file for example image1.jpg, image2.jpg, image3.jpg
	// type->for example propertyImage or aboutus image

	public int uploadFile(String base64String, String fileName, String type, int propertyId) throws IOException {
		BufferedOutputStream scanStream = null;
		Date date = new Date();
		System.out.println(date.toString());
		try {
			Base64.Decoder decoder = Base64.getDecoder();
			byte[] imageByte = decoder.decode(base64String.split("\\,")[1]);
			System.out.println(imageByte);
			String rootPath = System.getProperty("user.home");
			System.out.println(rootPath);
			File fileSaveDir = new File(rootPath + File.separator + "public_html" + File.separator + "assets"
					+ File.separator + type + File.separator + String.valueOf(propertyId));
			// Creates the save directory if it does not existsSS
			if (!fileSaveDir.exists()) {
				fileSaveDir.mkdirs();
				// Files.createDirectories(fileSaveDir);
			}
			File scanFile = new File(fileSaveDir.getAbsolutePath() + File.separator + fileName);
			scanStream = new BufferedOutputStream(new FileOutputStream(scanFile));
			scanStream.write(imageByte);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		} finally {
			scanStream.close();
		}
	}

	@Override
	public ResponseDto saveContactUs(ContactUs contactUs) {
		ResponseDto response = new ResponseDto();
		try {
			contactUs.setName(Util.nameFormat(contactUs.getName()));
			contactUs.setViewStatus(1);
			contactUsRepository.save(contactUs);
			smsTemplates.contactUsSMS(contactUs.getName(), contactUs.getMobileNumber(), contactUs.getEmailId());
			emailTemplates.contactUsEmail(contactUs.getName(), contactUs.getMobileNumber(), contactUs.getEmailId());
			response.setHttpStatus(HttpServletResponse.SC_OK);
			return response;
		}catch(Exception e) {
			e.printStackTrace();
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return response;
		}
	}

	@Override
	public ResponseDto resetPassword(String jsonString) {
		ResponseDto response = new ResponseDto();
		String password = "";
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			User finduser = userRepository.findByMobileNumberOrEmailId(jsonObj.getString("mobileNumber"));
			if(finduser!=null) {
				password = Util.randomNumberForOtp();
				System.out.println(password);
				User user = userRepository.findByMobileNumberOrEmailId(jsonObj.getString("mobileNumber"));
				user.setPassword(password);
				userService.save(user);
				//securityService.autoLogin(user.getMobileNumber(), password);
				smsTemplates.forgetPasswordSMS(finduser.getMobileNumber(), password);
				emailTemplates.forgetPasswordEmail(user.getName(), user.getMobileNumber(), user.getEmailId(), password);
				response.setResultStatus(2);
			}else {
				response.setResultStatus(1);
			}
			response.setHttpStatus(HttpServletResponse.SC_OK);
			return response;
		}catch(Exception e) {
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
			return response;
		}
	}

	@Override
	public List<FeedbackDto> fetchFeedbacks() {
		List<FeedbackDto> feedbackDto = new ArrayList<FeedbackDto>();
		try {
			feedbackDto = contactUsRepository.fetchfeedbackDetails();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return feedbackDto;
	}

	@Override
	public ResponseDto findByEmailId(String emailId) {
		ResponseDto response = new ResponseDto();
		try {
			User user = userRepository.findByEmailId(emailId);
			if(user == null) {
				response.setResultStatus(1); // not exist valid
			}else {
				response.setResultStatus(2); // already Exist
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public WebsiteIndexDto fetchIndexInfo(String jsonString) {
		WebsiteIndexDto websiteIndexDto = new WebsiteIndexDto();
		User user = new User();
		AboutUsDto aboutUsDto = new AboutUsDto();
		try {
			System.out.println(jsonString);
			JSONObject jsonObj = new JSONObject(jsonString);
			List<Properties> properties = propertyRepository.fetchFeaturedProperties();
			websiteIndexDto.setAboutUsDto(aboutUsRepository.fetchAboutUsContent());
			//peopleTestimonialRepository.fetchTestimonials());
			List<Properties> newProperties = propertyRepository.fetchNewProperties();
			if(!jsonObj.getString("mobileNumber").equals("null")) {
				System.out.println("inside login");
				user = userRepository.findByMobileNumber(jsonObj.getString("mobileNumber"));
				List<PropertyDetailsDto> propertyInfo = DtoConverter.propertiesWithWishList(properties,user);
				List<PropertyDetailsDto> newpropertyInfo = DtoConverter.propertiesWithWishList(newProperties,user);
				websiteIndexDto.setNewPropertiesList(newpropertyInfo);
				websiteIndexDto.setFeaturedPropertiesList(propertyInfo);
			}else {
				System.out.println("inside not login");
				List<PropertyDetailsDto> propertyInfo = DtoConverter.propertiesWithoutWishList(properties);
				List<PropertyDetailsDto> newpropertyInfo = DtoConverter.propertiesWithoutWishList(newProperties);
				websiteIndexDto.setFeaturedPropertiesList(propertyInfo);
				websiteIndexDto.setNewPropertiesList(newpropertyInfo);
			}
			List<PeopleTestimonials> peopleTestimonials = peopleTestimonialRepository.fetchTestimonials();
			websiteIndexDto.setTestimonialsList(DtoConverter.gettestimonialList(peopleTestimonials));
		}catch(Exception e) {
			e.printStackTrace();
		}
		return websiteIndexDto;
	}

	@Override
	public ResponseDto loginWithOtp(String jsonString) {
		ResponseDto response = new ResponseDto();
		String password = "";
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			User finduser = userRepository.findByMobileNumberOrEmailId(jsonObj.getString("mobileNumber"));
			if(finduser!=null) {
				password = Util.randomNumberForOtp();
				System.out.println(password);
				User user = userRepository.findByMobileNumberOrEmailId(jsonObj.getString("mobileNumber"));
				user.setPassword(password);
				userService.save(user);
				//securityService.autoLogin(user.getMobileNumber(), password);
				// same as forgotPassword
				smsTemplates.forgetPasswordSMS(finduser.getMobileNumber(), password);
				emailTemplates.forgetPasswordEmail(user.getName(), user.getMobileNumber(), user.getEmailId(), password);
				response.setResultStatus(2);
			}else {
				response.setResultStatus(1);
			}
			response.setHttpStatus(HttpServletResponse.SC_OK);
			return response;
		}catch(Exception e) {
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
			return response;
		}
	}

	@Override
	public List<PropertiesDto> fetchSearchedProperties(String jsonString) {
		System.out.println("inside searched properties");
		List<PropertiesDto> propertiesList = new ArrayList<PropertiesDto>();
		User user = new User();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			user = userRepository.findByMobileNumber(jsonObj.getString("mobileNumber"));
			if(!jsonObj.getString("propertyStatus").equals("")) {
				System.out.println("inside searched properties......");
				List<Properties> properties = propertyRepository.fetchAllProperties(jsonObj.getString("propertyPlace"), jsonObj.getString("propertyStatus"));
				propertiesList = DtoConverter.fetchSearchedProperties(properties,user);
			}else {
				System.out.println("inside searched properties/////");
				List<Properties> properties = propertyRepository.fetchAllProperties(jsonObj.getString("propertyPlace"));
				propertiesList = DtoConverter.fetchSearchedProperties(properties,user);
			}
			System.out.println(propertiesList);	
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertiesList;
	}
	
	@Override
	public List<PropertiesDto> fetchpropertiesBasedOnPincode(String jsonString) {
		List<PropertiesDto> propertiesList = new ArrayList<PropertiesDto>();
		User user = new User();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			user = userRepository.findByMobileNumber(jsonObj.getString("mobileNumber"));
			List<Properties> properties = propertyRepository.fetchAllPropertiesBasedOnPincode(jsonObj.getString("pincode"));
			propertiesList = DtoConverter.fetchSearchedProperties(properties,user);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertiesList;
	}	

	@Override
	public List<PropertiesDto> fetchPropertiesBasedOnFilters(String jsonString) {
		Set<String> propertyTypeSet = new HashSet<String>();
		Set<String> collectionSet = new HashSet<String>();
		List<Properties> propertiesList = new ArrayList<Properties>();
		List<PropertyDetailsDto> propertyInfo = new ArrayList<PropertyDetailsDto>();
		List<PropertiesDto> propertiesDtoList = new ArrayList<PropertiesDto>();
		User user = new User();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			user = userRepository.findByMobileNumber(jsonObj.getString("mobileNumber"));
		//	if(jsonObj.getDouble("minPrice")!=0.0 && jsonObj.getJSONArray("propertyType").length() == 0 && jsonObj.getJSONArray("collection").length() == 0) {
			
			//	List<Properties> propertiesList = propertyRepository.fetchPropertiesBasedMinPrice(jsonObj.getDouble("minPrice"),jsonObj.getString("propertyPlace"),jsonObj.getString("propertyStatus"));
			
			//}else if(jsonObj.getDouble("maxPrice")!=0.0 && jsonObj.getJSONArray("propertyType").length() == 0 && jsonObj.getJSONArray("collection").length() == 0) {
				
			//	List<Properties> propertiesList = propertyRepository.fetchPropertiesBasedMaxPrice(jsonObj.getDouble("maxPrice"),jsonObj.getString("propertyPlace"),jsonObj.getString("propertyStatus"));
			
			//}
			
			//Query for Relavant 
			if(jsonObj.getInt("priceRangeStatus") == 1) {
				System.out.println("inside relavant");
				System.out.println(jsonObj.getJSONArray("propertyType").length()+","+jsonObj.getJSONArray("collection").length());
				if(jsonObj.getJSONArray("propertyType").length() == 0 && jsonObj.getJSONArray("collection").length() == 0) {
					if(!jsonObj.getString("propertyStatus").equals("")) {
						propertiesList = propertyRepository.fetchPropertiesBasedMaxPriceAndMinPrice(jsonObj.getDouble("minPrice"),jsonObj.getDouble("maxPrice"),jsonObj.getString("propertyPlace"),jsonObj.getString("propertyStatus"));
					}else {
						propertiesList = propertyRepository.fetchPropertiesBasedMaxPriceAndMinPriceWithoutPropertyStatus(jsonObj.getDouble("minPrice"),jsonObj.getDouble("maxPrice"),jsonObj.getString("propertyPlace"));
					}
				}else if(jsonObj.getJSONArray("propertyType").length()!=0 && jsonObj.getJSONArray("collection").length() != 0) {
					for(int i = 0;i<jsonObj.getJSONArray("collection").length();i++) {
						collectionSet.add(jsonObj.getJSONArray("collection").getString(i));
					}
				
					for(int i = 0;i<jsonObj.getJSONArray("propertyType").length();i++) {
						propertyTypeSet.add(jsonObj.getJSONArray("propertyType").getString(i));
					}
					if(!jsonObj.getString("propertyStatus").equals("")) {
						propertiesList = propertyRepository.fetchPropertiesBasedPropertyTypeAndCollection(jsonObj.getDouble("minPrice"),jsonObj.getDouble("maxPrice"),jsonObj.getString("propertyPlace"),
						jsonObj.getString("propertyStatus"),collectionSet, propertyTypeSet);
					}else {
						propertiesList = propertyRepository.fetchPropertiesBasedPropertyTypeAndCollectionWithoutPropertyStatus(jsonObj.getDouble("minPrice"),jsonObj.getDouble("maxPrice"),jsonObj.getString("propertyPlace"),
						collectionSet, propertyTypeSet);

					}
				
				}else if(jsonObj.getJSONArray("propertyType").length() == 0 && jsonObj.getJSONArray("collection").length() != 0) {
					for(int i = 0;i<jsonObj.getJSONArray("collection").length();i++) {
						collectionSet.add(jsonObj.getJSONArray("collection").getString(i));
					}
					if(!jsonObj.getString("propertyStatus").equals("")) {
						propertiesList = propertyRepository.fetchPropertiesBasedCollection(jsonObj.getDouble("minPrice"),jsonObj.getDouble("maxPrice"),
						jsonObj.getString("propertyPlace"),jsonObj.getString("propertyStatus"), collectionSet);
					}else {
						propertiesList = propertyRepository.fetchPropertiesBasedCollectionWithoutPropertyStatus(jsonObj.getDouble("minPrice"),jsonObj.getDouble("maxPrice"),
						jsonObj.getString("propertyPlace"),collectionSet);
					}
				}else if(jsonObj.getJSONArray("propertyType").length() != 0 && jsonObj.getJSONArray("collection").length() == 0) {
					for(int i = 0;i<jsonObj.getJSONArray("propertyType").length();i++) {
						propertyTypeSet.add(jsonObj.getJSONArray("propertyType").getString(i));
					}
					if(!jsonObj.getString("propertyStatus").equals("")) {
						propertiesList = propertyRepository.fetchPropertiesBasedPropertyType(jsonObj.getDouble("minPrice"),jsonObj.getDouble("maxPrice"),jsonObj.getString("propertyPlace")
						,jsonObj.getString("propertyStatus"),propertyTypeSet);
					}else {
						propertiesList = propertyRepository.fetchPropertiesBasedPropertyTypeWithoutPropertyStatus(jsonObj.getDouble("minPrice"),jsonObj.getDouble("maxPrice"),jsonObj.getString("propertyPlace")
						,propertyTypeSet);
					}
				}
			
			//Query for High to low
			}else if(jsonObj.getInt("priceRangeStatus") == 2) {
				System.out.println("inside high to low");
				System.out.println(jsonObj.getJSONArray("propertyType").length()+","+jsonObj.getJSONArray("collection").length());
				if(jsonObj.getJSONArray("propertyType").length() == 0 && jsonObj.getJSONArray("collection").length() == 0) {
					if(!jsonObj.getString("propertyStatus").equals("")) {	
						propertiesList = propertyRepository.fetchPropertiesBasedMaxPriceAndMinPriceDesc(jsonObj.getDouble("minPrice"),jsonObj.getDouble("maxPrice"),jsonObj.getString("propertyPlace"),jsonObj.getString("propertyStatus"));
					}else {
						propertiesList = propertyRepository.fetchPropertiesBasedMaxPriceAndMinPriceDescWithoutPropertyStatus(jsonObj.getDouble("minPrice"),jsonObj.getDouble("maxPrice"),jsonObj.getString("propertyPlace"));
					}
				
				}else if(jsonObj.getJSONArray("propertyType").length()!=0 && jsonObj.getJSONArray("collection").length() != 0) {
					System.out.println("inside property Type, collection");
					for(int i = 0;i<jsonObj.getJSONArray("collection").length();i++) {
						collectionSet.add(jsonObj.getJSONArray("collection").getString(i));
					}
				
					for(int i = 0;i<jsonObj.getJSONArray("propertyType").length();i++) {
						propertyTypeSet.add(jsonObj.getJSONArray("propertyType").getString(i));
					}
					if(!jsonObj.getString("propertyStatus").equals("")) {
						propertiesList = propertyRepository.fetchPropertiesBasedPropertyTypeAndCollectionDesc(jsonObj.getDouble("minPrice"),jsonObj.getDouble("maxPrice"),jsonObj.getString("propertyPlace"),
						jsonObj.getString("propertyStatus"),collectionSet, propertyTypeSet);
					}else {
						propertiesList = propertyRepository.fetchPropertiesBasedPropertyTypeAndCollectionDescWithoutPropertyStatus(jsonObj.getDouble("minPrice"),jsonObj.getDouble("maxPrice"),jsonObj.getString("propertyPlace"),
						collectionSet, propertyTypeSet);
					}
				
				}else if(jsonObj.getJSONArray("propertyType").length() == 0 && jsonObj.getJSONArray("collection").length() != 0) {
					System.out.println("inside collection");
					for(int i = 0;i<jsonObj.getJSONArray("collection").length();i++) {
						collectionSet.add(jsonObj.getJSONArray("collection").getString(i));
					}
					if(!jsonObj.getString("propertyStatus").equals("")) {
						propertiesList = propertyRepository.fetchPropertiesBasedCollectionDesc(jsonObj.getDouble("minPrice"),jsonObj.getDouble("maxPrice"),
						jsonObj.getString("propertyPlace"),jsonObj.getString("propertyStatus"), collectionSet);
					}else {
						propertiesList = propertyRepository.fetchPropertiesBasedCollectionDescWithoutPropertyStatus(jsonObj.getDouble("minPrice"),jsonObj.getDouble("maxPrice"),
						jsonObj.getString("propertyPlace"),collectionSet);
					}
				
				}else if(jsonObj.getJSONArray("propertyType").length() != 0 && jsonObj.getJSONArray("collection").length() == 0) {
					System.out.println("inside property Type");
					for(int i = 0;i<jsonObj.getJSONArray("propertyType").length();i++) {
						propertyTypeSet.add(jsonObj.getJSONArray("propertyType").getString(i));
					}
					if(!jsonObj.getString("propertyStatus").equals("")) {
						propertiesList = propertyRepository.fetchPropertiesBasedPropertyTypeDesc(jsonObj.getDouble("minPrice"),jsonObj.getDouble("maxPrice"),jsonObj.getString("propertyPlace")
						,jsonObj.getString("propertyStatus"),propertyTypeSet);
					}else {
						propertiesList = propertyRepository.fetchPropertiesBasedPropertyTypeDescWithoutPropertyStatus(jsonObj.getDouble("minPrice"),jsonObj.getDouble("maxPrice"),jsonObj.getString("propertyPlace")
						,propertyTypeSet);
					}
				}
			
				//Query for low to high
			}else if(jsonObj.getInt("priceRangeStatus") == 3) {
				System.out.println("inside low to high");
				System.out.println(jsonObj.getJSONArray("propertyType").length()+","+jsonObj.getJSONArray("collection").length());
				if(jsonObj.getJSONArray("propertyType").length() == 0 && jsonObj.getJSONArray("collection").length() == 0) {
					System.out.println("inside minprice And maxprice");
					if(!jsonObj.getString("propertyStatus").equals("")) {
						propertiesList = propertyRepository.fetchPropertiesBasedMaxPriceAndMinPriceAsc(jsonObj.getDouble("minPrice"),jsonObj.getDouble("maxPrice"),jsonObj.getString("propertyPlace"),jsonObj.getString("propertyStatus"));
					}else {
						propertiesList = propertyRepository.fetchPropertiesBasedMaxPriceAndMinPriceAscWithoutPropertyStatus(jsonObj.getDouble("minPrice"),jsonObj.getDouble("maxPrice"),jsonObj.getString("propertyPlace"));
					}
					
				}else if(jsonObj.getJSONArray("propertyType").length()!=0 && jsonObj.getJSONArray("collection").length() != 0) {
					System.out.println("inside property Type, collection");
					for(int i = 0;i<jsonObj.getJSONArray("collection").length();i++) {
						collectionSet.add(jsonObj.getJSONArray("collection").getString(i));
					}
				
					for(int i = 0;i<jsonObj.getJSONArray("propertyType").length();i++) {
						propertyTypeSet.add(jsonObj.getJSONArray("propertyType").getString(i));
					}
					if(!jsonObj.getString("propertyStatus").equals("")) {
						propertiesList = propertyRepository.fetchPropertiesBasedPropertyTypeAndCollectionAsc(jsonObj.getDouble("minPrice"),jsonObj.getDouble("maxPrice"),jsonObj.getString("propertyPlace"),
						jsonObj.getString("propertyStatus"),collectionSet, propertyTypeSet);
					}else {
						propertiesList = propertyRepository.fetchPropertiesBasedPropertyTypeAndCollectionAscWithoutPropertyStatus(jsonObj.getDouble("minPrice"),jsonObj.getDouble("maxPrice"),jsonObj.getString("propertyPlace"),
						collectionSet, propertyTypeSet);
					}
				
				}else if(jsonObj.getJSONArray("propertyType").length() == 0 && jsonObj.getJSONArray("collection").length() != 0) {
					System.out.println("inside collection");
					for(int i = 0;i<jsonObj.getJSONArray("collection").length();i++) {
						collectionSet.add(jsonObj.getJSONArray("collection").getString(i));
					}
					if(!jsonObj.getString("propertyStatus").equals("")) {
						propertiesList = propertyRepository.fetchPropertiesBasedCollectionAsc(jsonObj.getDouble("minPrice"),jsonObj.getDouble("maxPrice"),
						jsonObj.getString("propertyPlace"),jsonObj.getString("propertyStatus"), collectionSet);
					}else {
						propertiesList = propertyRepository.fetchPropertiesBasedCollectionAscWithoutPropertyStatus(jsonObj.getDouble("minPrice"),jsonObj.getDouble("maxPrice"),
						jsonObj.getString("propertyPlace"),collectionSet);
					}
				
				}else if(jsonObj.getJSONArray("propertyType").length() != 0 && jsonObj.getJSONArray("collection").length() == 0) {
					System.out.println("inside property Type");
					for(int i = 0;i<jsonObj.getJSONArray("propertyType").length();i++) {
						propertyTypeSet.add(jsonObj.getJSONArray("propertyType").getString(i));
					}
					if(!jsonObj.getString("propertyStatus").equals("")) {
						propertiesList = propertyRepository.fetchPropertiesBasedPropertyTypeAsc(jsonObj.getDouble("minPrice"),jsonObj.getDouble("maxPrice"),jsonObj.getString("propertyPlace")
						,jsonObj.getString("propertyStatus"),propertyTypeSet);
					}else {
						propertiesList = propertyRepository.fetchPropertiesBasedPropertyTypeAscWithoutPropertyStatus(jsonObj.getDouble("minPrice"),jsonObj.getDouble("maxPrice"),jsonObj.getString("propertyPlace")
						,propertyTypeSet);
					}
				}
			}
			propertiesDtoList = DtoConverter.fetchSearchedProperties(propertiesList,user);
			
			//propertyInfo = DtoConverter.propertiesWithoutWishList(propertiesList);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertiesDtoList;
	}
	@Override
	public ResponseDto savePropertyEnquiry(PropertyEnquiry propertyEnquiry) {
		ResponseDto response = new ResponseDto();
		try {
			propertyEnquiry.setActiveStatus(2);
			if(propertyEnquiryRepository.save(propertyEnquiry)!=null) {
				response.setResultStatus(1);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}else {
				response.setResultStatus(2);
				response.setHttpStatus(HttpServletResponse.SC_OK);
			}
		}catch(Exception e) {
			response.setHttpStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public List<PropertiesDto> getSimilerProperties(String jsonString) {
		List<PropertiesDto> propertiesDto = new ArrayList<PropertiesDto>();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			User user = userRepository.findByMobileNumber(jsonObj.getString("mobileNumber"));
			PropertyImageDto propertyImageDto = propertyRepository.getPropertyImageDto(jsonObj.getInt("propertyId"));
			List<Properties> propertiesList = propertyRepository.getSimilerProperties(propertyImageDto.getPropertyType(),propertyImageDto.getPropertyStatus());
			propertiesDto = DtoConverter.fetchSearchedProperties(propertiesList,user);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertiesDto;
	}

	@Override
	public List<PropertiesDto> getPropertiesOnlyBasedOnLocation(String jsonString) {
		List<PropertiesDto> propertiesDto = new ArrayList<PropertiesDto>();
		try {
			JSONObject jsonObj = new JSONObject(jsonString);
			User user = userRepository.findByMobileNumber(jsonObj.getString("mobileNumber"));
			List<Properties> propertiesList	= propertyRepository.getPropertiesOnlyBasedOnLocation(jsonObj.getString("propertyLocation"));
			propertiesDto = DtoConverter.fetchSearchedProperties(propertiesList,user);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertiesDto;
	}
}
