package com.atechtron.beegru.staff.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.atechtron.beegru.login.dto.PropertyInfoDto;
import com.atechtron.beegru.login.dto.UserDetails;
import com.atechtron.beegru.login.service.LoginService;

@Controller
@RequestMapping(value = "/staff")
public class StaffController {

	@Autowired
	private LoginService loginService;
	
	@RequestMapping(value = "/staff_agent_details", method = RequestMethod.GET)
	public String staff_agent_details(Model model,Authentication authentication) {
		System.out.println("Inside staff agent details");
		try {
			List<UserDetails> userDetails = loginService.fetchUserDetails(authentication.getName());
			System.out.println(userDetails);
			model.addAttribute("userDetails",userDetails);
			return "staff_agent_details";
		}catch(Exception e) {
			e.printStackTrace();
			return "staff_agent_details";
		}
	}	
	
	@RequestMapping(value = "/assigned_property_details", method = RequestMethod.GET)
	public String staff_dashboard(Model model, Authentication authentication) {
		List<PropertyInfoDto> propertyInfoList = new ArrayList<PropertyInfoDto>();
		try {
			propertyInfoList = loginService.fetchPropertyAssignedToStaff(authentication.getName());
			model.addAttribute("propertyInfoList",propertyInfoList);
			return "staff_assigned_properties";
		}catch(Exception e) {
			e.printStackTrace();
			return "staff_assigned_properties";
		}
	}
	
	@RequestMapping(value = "/staff_dashboard", method = RequestMethod.GET)
	public String staff_dashboard() {
		try {
			return "staff_dashboard";
		}catch(Exception e) {
			e.printStackTrace();
			return "staff_dashboard";
		}
	}	
	
	@RequestMapping(value = "/staff_profile", method = RequestMethod.GET)
	public String staff_profile() {
		try {
			return "staff_profile";
		}catch(Exception e) {
			e.printStackTrace();
			return "staff_profile";
		}
	}	
	
	@RequestMapping(value = "/staff_report_issue", method = RequestMethod.GET)
	public String staff_report_issue() {
		try {
			return "staff_report_issue";
		}catch(Exception e) {
			e.printStackTrace();
			return "staff_report_issue";
		}
	}	
	
	@RequestMapping(value = "/staff_upload_properties", method = RequestMethod.GET)
	public String staff_upload_properties() {
		try {
			return "staff_upload_properties";
		}catch(Exception e) {
			e.printStackTrace();
			return "staff_upload_properties";
		}
	}	
	
	@RequestMapping(value = "/staff_uploaded_properties", method = RequestMethod.GET)
	public String staff_uploaded_properties(Authentication authentication, Model model) {
		List<PropertyInfoDto> propertyInfoList = new ArrayList<PropertyInfoDto>();
		try {
			propertyInfoList = loginService.fetchPropertyInfo(authentication.getName());
			model.addAttribute("propertyInfoList",propertyInfoList);
			return "staff_uploaded_properties";
		}catch(Exception e) {
			e.printStackTrace();
			return "staff_uploaded_properties";
		}
	}	
	
	@RequestMapping(value = "/staff_user_details", method = RequestMethod.GET)
	public String staff_user_details() {
		try {
			return "staff_user_details";
		}catch(Exception e) {
			e.printStackTrace();
			return "staff_user_details";
		}
	}
}
