package com.atechtron.beegru.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.atechtron.beegru.website.model.OtpVerification;

public interface OtpRepository extends JpaRepository<OtpVerification,Integer>{

	@Query(value = "SELECT otp FROM otp_verification WHERE mobile_number=:mobileNumber ORDER BY otp_id DESC LIMIT 1",nativeQuery = true)
	List<String> findOtpByMobileNumber(String mobileNumber);

}
