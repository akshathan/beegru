package com.atechtron.beegru.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.atechtron.beegru.login.dto.FeedbackDto;
import com.atechtron.beegru.website.model.ContactUs;

public interface ContactUsRepository extends JpaRepository<ContactUs,Integer>{

	@Query(value="Select new com.atechtron.beegru.login.dto.FeedbackDto(f.contactUsId,f.name,f.mobileNumber,f.emailId,f.location,f.message,f.addedDate,f.viewStatus) FROM ContactUs f WHERE f.viewStatus!=3")
	List<FeedbackDto> fetchfeedbackDetails();
	
	@Transactional
    @Modifying
	@Query(value = "update ContactUs l set l.viewStatus = :status where l.contactUsId = :id")
	int updateContactUsStatus(Integer id, Integer status);
}
