package com.atechtron.beegru.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.atechtron.beegru.login.model.BeegruScore;

public interface BeegruScoreRepository extends JpaRepository<BeegruScore, Integer>{

	@Query(value = "FROM BeegruScore b WHERE b.properties.propertyId = :propertyId")
	BeegruScore findByPropductId(int propertyId);

	@Query(value = "FROM BeegruScore b WHERE b.properties.propertyId = :propertyId AND b.beegruScoreId!=:beegruScoreId")
	BeegruScore findByPropductId(int propertyId, int beegruScoreId);

	@Transactional
    @Modifying
	@Query(value = "DELETE FROM BeegruScore WHERE beegruScoreId = :beegruScoreId")
	void deleteBeegruScore(int beegruScoreId);

}
