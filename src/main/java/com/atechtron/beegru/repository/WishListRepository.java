package com.atechtron.beegru.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.atechtron.beegru.login.model.WishList;

public interface WishListRepository extends JpaRepository<WishList,Integer>{

	@Query(value = "SELECT wish_list_id FROM wish_list WHERE properties_property_id = :propertyId AND user_user_id = :userId", nativeQuery = true)
	List<Integer> findwishListIfExists(int propertyId, int userId);

}
