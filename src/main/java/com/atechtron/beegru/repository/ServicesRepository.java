package com.atechtron.beegru.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.atechtron.beegru.login.model.Services;

public interface ServicesRepository extends JpaRepository<Services,Integer>{

}
