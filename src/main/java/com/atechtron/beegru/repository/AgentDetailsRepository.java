package com.atechtron.beegru.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.atechtron.beegru.login.model.AgentDetails;

public interface AgentDetailsRepository extends JpaRepository<AgentDetails, Integer>{

	@Query(value = "FROM AgentDetails a WHERE a.staffDetails.staffId = :staffId")
	List<AgentDetails> findAgentDetailsOfStaff(int staffId);

}
