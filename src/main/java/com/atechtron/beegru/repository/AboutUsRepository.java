package com.atechtron.beegru.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.atechtron.beegru.login.model.AboutUs;
import com.atechtron.beegru.website.dto.AboutUsDto;

public interface AboutUsRepository extends JpaRepository<AboutUs, Integer>{

	@Query(value="Select new com.atechtron.beegru.website.dto.AboutUsDto(a.aboutUsId,a.description,a.videoLink) FROM AboutUs a")
	AboutUsDto fetchAboutUsContent();

}
