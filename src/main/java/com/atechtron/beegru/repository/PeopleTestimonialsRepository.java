package com.atechtron.beegru.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.atechtron.beegru.login.model.PeopleTestimonials;
import com.atechtron.beegru.website.dto.TestimonialsDto;

public interface PeopleTestimonialsRepository extends JpaRepository<PeopleTestimonials,Integer>{

//	@Query(value="Select new com.atechtron.beegru.website.dto.TestimonialsDto(a.peopleTestimonialsId,a.description,a.ratings,a.testimonialName,a.testimonialAddress,a.videoLink,a.description,a.addedDate) FROM PeopleTestimonials a")
//	List<TestimonialsDto> fetchTestimonials();
	
	@Query(value="FROM PeopleTestimonials")
	List<PeopleTestimonials> fetchTestimonials();

	@Transactional
    @Modifying
	@Query(value = "DELETE FROM PeopleTestimonials WHERE peopleTestimonialsId = :id")
	int deleteTestimonailsById(int id);

}
