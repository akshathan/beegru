package com.atechtron.beegru.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.atechtron.beegru.login.model.ManagerDetails;

public interface ManagerDetailsRepository extends JpaRepository<ManagerDetails, Integer>{

}
