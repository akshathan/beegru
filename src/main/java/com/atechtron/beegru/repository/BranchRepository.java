package com.atechtron.beegru.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.atechtron.beegru.login.model.BranchDetails;

public interface BranchRepository extends JpaRepository<BranchDetails,Integer>{

	@Query(value = "FROM BranchDetails WHERE branchStatus!=0")
	List<BranchDetails> fetchAllBranch();

}
