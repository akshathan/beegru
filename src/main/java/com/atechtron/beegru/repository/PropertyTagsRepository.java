package com.atechtron.beegru.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.atechtron.beegru.login.model.PropertyTags;

public interface PropertyTagsRepository extends JpaRepository<PropertyTags,Integer> {

	@Query(value = "SELECT * FROM property_tags p WHERE p.position = :position AND p.tag_name=:tagName AND properties_property_id = :propertyId AND p.active_status!=2", nativeQuery = true)
	List<PropertyTags> fetchTags(int position, String tagName, int propertyId);

	@Transactional
    @Modifying
	@Query(value = "update PropertyTags l set l.activeStatus = :status where l.propertyTagsId = :propertyTagId")
	int deleteTag(int status , int propertyTagId);

	@Query(value = "FROM PropertyTags p WHERE p.activeStatus!=2")
	List<PropertyTags> fetchTags();

//	@Query(value = "SELECT * FROM property_tags p WHERE p.position = :position AND p.tag_name=:tagName", nativeQuery = true)
//	List<PropertyTags> fetchTags(int position, String tagName);

}
