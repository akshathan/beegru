package com.atechtron.beegru.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.atechtron.beegru.website.model.Test;

public interface TestRepository extends JpaRepository<Test, Integer>{

}
