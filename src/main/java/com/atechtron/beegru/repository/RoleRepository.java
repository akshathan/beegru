package com.atechtron.beegru.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.atechtron.beegru.website.model.Role;

public interface RoleRepository extends JpaRepository<Role,Integer>{

}
