package com.atechtron.beegru.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.atechtron.beegru.website.model.User;

public interface UserRepository extends JpaRepository<User,Integer>{

	
	User findByMobileNumber(String username);

	@Query(value = "FROM User WHERE mobileNumber = :mobileNumber OR emailId = :mobileNumber")
	User findUserByMobileNumber(String mobileNumber); // find by mobileNumber or emailId

	@Query(value = "SELECT user_id FROM user WHERE mobile_number =:mobileNumber", nativeQuery = true)
	int findUserIdByMobileNumber(String mobileNumber);

	User findByEmailId(String username);

	@Query(value = "FROM User WHERE emailId = :username OR mobileNumber = :username")
	User findByMobileNumberOrEmailId(String username);

	@Query(value = "FROM User WHERE emailId = :emailId OR mobileNumber = :mobileNumber")
	User findByEmailIdOrMobileNumber(String emailId, String mobileNumber);

	@Query(value = "FROM User")
	List<User> findAllUser();

	@Transactional
    @Modifying
	@Query(value = "update User l set l.activeStatus = :status where l.userId = :userId")
	int updateUserStatus(int status, int userId);

	@Query(value = "FROM User u JOIN u.roles r WHERE r.roleId = '3'")
	List<User> findManagerDetails();

	@Query(value = "FROM User u JOIN u.roles r WHERE r.roleId = '4'")
	List<User> findStaffDetails();

	@Query(value = "FROM User u JOIN u.roles r WHERE r.roleId = '5'")
	List<User> findAgentDetails();

	@Query(value = "FROM User WHERE (emailId = :emailId OR mobileNumber = :mobileNumber) AND (userId!=:userId)")
	User findByEmailIdOrMobileNumber(String emailId, String mobileNumber, int userId);

//	@Query(value = "FROM User u, StaffDetails s JOIN s.agentDetails a WHERE u.userId = s.user.userId And a.staffDetails.staffId = :userId")
//	List<User> findAgentDetailsOfStaff(int userId);

}
