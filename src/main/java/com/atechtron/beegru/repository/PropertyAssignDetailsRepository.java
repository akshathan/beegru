package com.atechtron.beegru.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.atechtron.beegru.login.dto.PropertyAssignedDto;
import com.atechtron.beegru.login.model.PropertyAssignDetails;

public interface PropertyAssignDetailsRepository extends JpaRepository<PropertyAssignDetails, Integer>{

	@Query(value = "FROM PropertyAssignDetails p WHERE p.staffDetails.staffId = :staffId")
	List<PropertyAssignDetails> fetchPropertyAssignedDetailsOfStaff(int staffId);

}
