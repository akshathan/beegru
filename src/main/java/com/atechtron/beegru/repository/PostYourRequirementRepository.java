package com.atechtron.beegru.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.atechtron.beegru.login.model.PostYourRequirement;

public interface PostYourRequirementRepository extends JpaRepository<PostYourRequirement,Integer>{

	@Query(value = "FROM PostYourRequirement")
	List<PostYourRequirement> fetchPostYourRequirement();

	@Transactional
    @Modifying
	@Query(value = "update PostYourRequirement p set p.activeStatus = :status where p.postYourRequirementId = :requirementId")
	int updatePostYourRequirementStatus(int requirementId, int status);

}
