package com.atechtron.beegru.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.atechtron.beegru.login.dto.PropertiesDto;
import com.atechtron.beegru.login.dto.PropertyImageDto;
import com.atechtron.beegru.login.dto.PropertyInfoDto;
import com.atechtron.beegru.login.model.Properties;

public interface PropertyRepository extends JpaRepository<Properties,Integer>{

//	@Query(value = "FROM Properties WHERE activeStatus=1")
//	List<Properties> fetchIndexInfo();
	
	@Transactional
    @Modifying
	@Query(value = "update properties p set p.approved_date = NOW(), p.active_status = :status where p.property_id = :propertyId", nativeQuery = true)
	int updatePropertyStatus(int propertyId, int status);

	@Query(value = "FROM Properties p JOIN p.propertyTags t WHERE t.properties.propertyId=p.propertyId AND t.tagName = 'New' AND p.activeStatus=1 AND t.activeStatus!=2 ORDER BY t.position ASC")
	List<Properties> fetchNewProperties();

	@Query(value="Select new com.atechtron.beegru.login.dto.PropertyInfoDto(p.propertyId,p.propertyName,p.propertyLocation,p.displayPrice,p.propertyType,p.propertyStatus) FROM Properties p  WHERE p.propertyStatus = :propertyStatus AND p.propertyLocation LIKE %:propertyPlace%")
	//@Query(value = "FROM Properties p WHERE p.propertyStatus = :propertyStatus AND propertyLocation = %:propertyPlace%")
	List<PropertyInfoDto> searchProducts(String propertyPlace, String propertyStatus);

	@Transactional
    @Modifying
	@Query(value = "update properties p set p.featured_properties = :status where p.property_id = :propertyId", nativeQuery = true)
	int updateFeaturedPropertyStatus(int propertyId, int status);

	@Query(value = "FROM Properties p JOIN p.propertyTags t WHERE t.properties.propertyId=p.propertyId AND t.tagName = 'Featured' AND p.activeStatus=1 AND t.activeStatus!=2 ORDER BY t.position ASC")
	List<Properties> fetchFeaturedProperties();

	@Query(value = "FROM Properties WHERE activeStatus=1")
	List<PropertiesDto> fetchAllProperties();

	@Query(value="FROM Properties p WHERE p.activeStatus=1 AND p.propertyStatus = :propertyStatus AND p.propertyLocation LIKE %:propertyPlace%")
//	@Query(value = "FROM Properties p JOIN p.propertyTags t WHERE t.properties.propertyId=p.propertyId AND t.tagName = 'Featured' AND p.activeStatus=1 AND t.activeStatus!=2 ORDER BY t.position ASC")
	List<Properties> fetchAllProperties(String propertyPlace, String propertyStatus);

	@Query(value="Select new com.atechtron.beegru.login.dto.PropertyInfoDto(p.propertyId,p.propertyName,p.propertyLocation,p.displayPrice,p.propertyType,p.propertyStatus) FROM Properties p  WHERE p.propertyId = :propertyId")
	PropertyInfoDto fetchPropertyInfoById(int propertyId);

	@Query(value = "FROM Properties p WHERE p.propertyId = :propertyId")
	Properties fetchById(int propertyId);

	@Query(value = "FROM Properties p ORDER BY p.propertyId DESC")
	List<Properties> fetchProperties();

	
	//filters queries starts for relavent
	@Query(value = "FROM Properties p WHERE p.minPrice>=:minPrice "
			+ "AND p.maxPrice<=:maxPrice "
			+ "AND p.activeStatus=1 "
			+ "AND p.propertyStatus = :propertyStatus "
			+ "AND p.propertyLocation LIKE %:propertyPlace%")
	List<Properties> fetchPropertiesBasedMaxPriceAndMinPrice(double minPrice, double maxPrice, String propertyPlace,
			String propertyStatus);

	@Query(value = "FROM Properties p WHERE p.minPrice>=:minPrice "
			+ "AND p.maxPrice<=:maxPrice "
			+ "AND p.activeStatus=1 "
			+ "AND p.propertyLocation LIKE %:propertyPlace%")
	List<Properties> fetchPropertiesBasedMaxPriceAndMinPriceWithoutPropertyStatus(double minPrice, double maxPrice,
			String propertyPlace);
	
	@Query(value = "FROM Properties p WHERE p.minPrice>=:minPrice "
			+ "AND p.maxPrice<=:maxPrice "
			+ "AND p.propertyStatus = :propertyStatus "
			+ "AND p.activeStatus=1 "
			+ "AND p.type IN (:propertyTypeSet) "
			+ "AND p.propertyLocation LIKE %:propertyPlace%")
	List<Properties> fetchPropertiesBasedPropertyType(double minPrice, double maxPrice, String propertyPlace, String propertyStatus,
			Set<String> propertyTypeSet);

	@Query(value = "FROM Properties p WHERE p.minPrice>=:minPrice "
			+ "AND p.maxPrice<=:maxPrice "
			+ "AND p.activeStatus=1 "
			+ "AND p.type IN (:propertyTypeSet) "
			+ "AND p.propertyLocation LIKE %:propertyPlace%")
	List<Properties> fetchPropertiesBasedPropertyTypeWithoutPropertyStatus(double minPrice, double maxPrice,
			String propertyPlace, Set<String> propertyTypeSet);

	
	
	@Query(value = "FROM Properties p JOIN p.propertyTags t WHERE t.properties.propertyId=p.propertyId "
			+ "AND t.tagName IN (:collectionSet) "
			+ "AND p.activeStatus=1 "
			+ "AND p.minPrice>=:minPrice "
			+ "AND p.maxPrice<=:maxPrice "
			+ "AND p.propertyStatus = :propertyStatus "
			+ "AND p.propertyLocation LIKE %:propertyPlace% "
			+ "AND t.activeStatus!=2 GROUP BY t.properties.propertyId")
	List<Properties> fetchPropertiesBasedCollection(double minPrice, double maxPrice, String propertyPlace, String propertyStatus,
			Set<String> collectionSet);

	@Query(value = "FROM Properties p JOIN p.propertyTags t WHERE t.properties.propertyId=p.propertyId "
			+ "AND t.tagName IN (:collectionSet) "
			+ "AND p.activeStatus=1 "
			+ "AND p.minPrice>=:minPrice "
			+ "AND p.maxPrice<=:maxPrice "
			+ "AND p.propertyLocation LIKE %:propertyPlace% "
			+ "AND t.activeStatus!=2 GROUP BY t.properties.propertyId")
	List<Properties> fetchPropertiesBasedCollectionWithoutPropertyStatus(double minPrice, double maxPrice, String propertyPlace,
			Set<String> collectionSet);

	
	
	@Query(value = "FROM Properties p JOIN p.propertyTags t WHERE t.properties.propertyId=p.propertyId "
			+ "AND t.tagName IN (:collectionSet) AND p.activeStatus=1 "
			+ "AND p.minPrice>=:minPrice "
			+ "AND p.maxPrice<=:maxPrice "
			+ "AND p.type IN (:propertyTypeSet)"
			+ "AND p.propertyStatus = :propertyStatus "
			+ "AND p.propertyLocation LIKE %:propertyPlace% "
			+ "AND t.activeStatus!=2 GROUP BY t.properties.propertyId ")
	List<Properties> fetchPropertiesBasedPropertyTypeAndCollection(double minPrice, double maxPrice, String propertyPlace,
			String propertyStatus, Set<String> collectionSet, Set<String> propertyTypeSet);

	@Query(value = "FROM Properties p JOIN p.propertyTags t WHERE t.properties.propertyId=p.propertyId "
			+ "AND t.tagName IN (:collectionSet) AND p.activeStatus=1 "
			+ "AND p.minPrice>=:minPrice "
			+ "AND p.maxPrice<=:maxPrice "
			+ "AND p.type IN (:propertyTypeSet)"
			+ "AND p.propertyLocation LIKE %:propertyPlace% "
			+ "AND t.activeStatus!=2 GROUP BY t.properties.propertyId ")
	List<Properties> fetchPropertiesBasedPropertyTypeAndCollectionWithoutPropertyStatus(double minPrice, double maxPrice,
			String propertyPlace, Set<String> collectionSet, Set<String> propertyTypeSet);

	//filters queries end for relavent
	
	//filters queries starts for low to high

	@Query(value = "FROM Properties p WHERE p.minPrice>=:minPrice "
			+ "AND p.maxPrice<=:maxPrice "
			+ "AND p.activeStatus=1 "
			+ "AND p.propertyStatus = :propertyStatus "
			+ "AND p.propertyLocation LIKE %:propertyPlace% ORDER BY p.maxPrice ASC")
	List<Properties> fetchPropertiesBasedMaxPriceAndMinPriceAsc(double minPrice, double maxPrice, String propertyPlace,
			String propertyStatus);

	@Query(value = "FROM Properties p WHERE p.minPrice>=:minPrice "
			+ "AND p.maxPrice<=:maxPrice "
			+ "AND p.activeStatus=1 "
			+ "AND p.propertyLocation LIKE %:propertyPlace% ORDER BY p.maxPrice ASC")
	List<Properties> fetchPropertiesBasedMaxPriceAndMinPriceAscWithoutPropertyStatus(double minPrice, double maxPrice,
			String propertyPlace);

	
	@Query(value = "FROM Properties p WHERE p.minPrice>=:minPrice "
			+ "AND p.maxPrice<=:maxPrice "
			+ "AND p.propertyStatus = :propertyStatus "
			+ "AND p.activeStatus=1 "
			+ "AND p.type IN (:propertyTypeSet) "
			+ "AND p.propertyLocation LIKE %:propertyPlace% ORDER BY p.maxPrice ASC")
	List<Properties> fetchPropertiesBasedPropertyTypeAsc(double minPrice, double maxPrice, String propertyPlace, String propertyStatus,
			Set<String> propertyTypeSet);

	@Query(value = "FROM Properties p WHERE p.minPrice>=:minPrice "
			+ "AND p.maxPrice<=:maxPrice "
			+ "AND p.activeStatus=1 "
			+ "AND p.type IN (:propertyTypeSet) "
			+ "AND p.propertyLocation LIKE %:propertyPlace% ORDER BY p.maxPrice ASC")
	List<Properties> fetchPropertiesBasedPropertyTypeAscWithoutPropertyStatus(double minPrice, double maxPrice,
			String propertyPlace, Set<String> propertyTypeSet);

	
	@Query(value = "FROM Properties p JOIN p.propertyTags t WHERE t.properties.propertyId=p.propertyId "
			+ "AND t.tagName IN (:collectionSet) "
			+ "AND p.activeStatus=1 "
			+ "AND p.minPrice>=:minPrice "
			+ "AND p.maxPrice<=:maxPrice "
			+ "AND p.propertyStatus = :propertyStatus "
			+ "AND p.propertyLocation LIKE %:propertyPlace% "
			+ "AND t.activeStatus!=2 GROUP BY t.properties.propertyId ORDER BY p.maxPrice ASC")
	List<Properties> fetchPropertiesBasedCollectionAsc(double minPrice, double maxPrice, String propertyPlace, String propertyStatus,
			Set<String> collectionSet);

	@Query(value = "FROM Properties p JOIN p.propertyTags t WHERE t.properties.propertyId=p.propertyId "
			+ "AND t.tagName IN (:collectionSet) "
			+ "AND p.activeStatus=1 "
			+ "AND p.minPrice>=:minPrice "
			+ "AND p.maxPrice<=:maxPrice "
			+ "AND p.propertyLocation LIKE %:propertyPlace% "
			+ "AND t.activeStatus!=2 GROUP BY t.properties.propertyId ORDER BY p.maxPrice ASC")
	List<Properties> fetchPropertiesBasedCollectionAscWithoutPropertyStatus(double minPrice, double maxPrice,
			String propertyPlace, Set<String> collectionSet);

	
	@Query(value = "FROM Properties p JOIN p.propertyTags t WHERE t.properties.propertyId=p.propertyId "
			+ "AND t.tagName IN (:collectionSet) AND p.activeStatus=1 "
			+ "AND p.minPrice>=:minPrice "
			+ "AND p.maxPrice<=:maxPrice "
			+ "AND p.type IN (:propertyTypeSet)"
			+ "AND p.propertyStatus = :propertyStatus "
			+ "AND p.propertyLocation LIKE %:propertyPlace% "
			+ "AND t.activeStatus!=2 GROUP BY t.properties.propertyId ORDER BY p.maxPrice ASC")
	List<Properties> fetchPropertiesBasedPropertyTypeAndCollectionAsc(double minPrice, double maxPrice, String propertyPlace,
			String propertyStatus, Set<String> collectionSet, Set<String> propertyTypeSet);

	@Query(value = "FROM Properties p JOIN p.propertyTags t WHERE t.properties.propertyId=p.propertyId "
			+ "AND t.tagName IN (:collectionSet) AND p.activeStatus=1 "
			+ "AND p.minPrice>=:minPrice "
			+ "AND p.maxPrice<=:maxPrice "
			+ "AND p.type IN (:propertyTypeSet)"
			+ "AND p.propertyLocation LIKE %:propertyPlace% "
			+ "AND t.activeStatus!=2 GROUP BY t.properties.propertyId ORDER BY p.maxPrice ASC")
	List<Properties> fetchPropertiesBasedPropertyTypeAndCollectionAscWithoutPropertyStatus(double minPrice,
			double maxPrice, String propertyPlace, Set<String> collectionSet, Set<String> propertyTypeSet);

	//filters queries end for low to high
		
		
		//filters queries starts for high to low
		@Query(value = "FROM Properties p WHERE p.minPrice>=:minPrice "
				+ "AND p.maxPrice<=:maxPrice "
				+ "AND p.activeStatus=1 "
				+ "AND p.propertyStatus = :propertyStatus "
				+ "AND p.propertyLocation LIKE %:propertyPlace% ORDER BY p.maxPrice DESC")
		List<Properties> fetchPropertiesBasedMaxPriceAndMinPriceDesc(double minPrice, double maxPrice, String propertyPlace,
				String propertyStatus);

		//without Property Status		
		@Query(value = "FROM Properties p WHERE p.minPrice>=:minPrice "
				+ "AND p.maxPrice<=:maxPrice "
				+ "AND p.activeStatus=1 "
				+ "AND p.propertyLocation LIKE %:propertyPlace% ORDER BY p.maxPrice DESC")
		List<Properties> fetchPropertiesBasedMaxPriceAndMinPriceDescWithoutPropertyStatus(double minPrice, double maxPrice,
				String propertyPlace);

		
		@Query(value = "FROM Properties p WHERE p.minPrice>=:minPrice "
				+ "AND p.maxPrice<=:maxPrice "
				+ "AND p.propertyStatus = :propertyStatus "
				+ "AND p.activeStatus=1 "
				+ "AND p.type IN (:propertyTypeSet) "
				+ "AND p.propertyLocation LIKE %:propertyPlace% ORDER BY p.maxPrice DESC")
		List<Properties> fetchPropertiesBasedPropertyTypeDesc(double minPrice, double maxPrice, String propertyPlace, String propertyStatus,
				Set<String> propertyTypeSet);

		//without Property Status
		@Query(value = "FROM Properties p WHERE p.minPrice>=:minPrice "
				+ "AND p.maxPrice<=:maxPrice "
				+ "AND p.activeStatus=1 "
				+ "AND p.type IN (:propertyTypeSet) "
				+ "AND p.propertyLocation LIKE %:propertyPlace% ORDER BY p.maxPrice DESC")
		List<Properties> fetchPropertiesBasedPropertyTypeDescWithoutPropertyStatus(double minPrice, double maxPrice,
				String propertyPlace, Set<String> propertyTypeSet);

		
		
		@Query(value = "FROM Properties p JOIN p.propertyTags t WHERE t.properties.propertyId=p.propertyId "
				+ "AND t.tagName IN (:collectionSet) "
				+ "AND p.activeStatus=1 "
				+ "AND p.minPrice>=:minPrice "
				+ "AND p.maxPrice<=:maxPrice "
				+ "AND p.propertyStatus = :propertyStatus "
				+ "AND p.propertyLocation LIKE %:propertyPlace% "
				+ "AND t.activeStatus!=2 GROUP BY t.properties.propertyId ORDER BY p.maxPrice DESC")
		List<Properties> fetchPropertiesBasedCollectionDesc(double minPrice, double maxPrice, String propertyPlace, String propertyStatus,
				Set<String> collectionSet);

		//without Property Status
		@Query(value = "FROM Properties p JOIN p.propertyTags t WHERE t.properties.propertyId=p.propertyId "
				+ "AND t.tagName IN (:collectionSet) "
				+ "AND p.activeStatus=1 "
				+ "AND p.minPrice>=:minPrice "
				+ "AND p.maxPrice<=:maxPrice "
				+ "AND p.propertyLocation LIKE %:propertyPlace% "
				+ "AND t.activeStatus!=2 GROUP BY t.properties.propertyId ORDER BY p.maxPrice DESC")
		List<Properties> fetchPropertiesBasedCollectionDescWithoutPropertyStatus(double minPrice, double maxPrice,
				String propertyPlace, Set<String> collectionSet);

		@Query(value = "FROM Properties p JOIN p.propertyTags t WHERE t.properties.propertyId=p.propertyId "
				+ "AND t.tagName IN (:collectionSet) AND p.activeStatus=1 "
				+ "AND p.minPrice>=:minPrice "
				+ "AND p.maxPrice<=:maxPrice "
				+ "AND p.type IN (:propertyTypeSet)"
				+ "AND p.propertyStatus = :propertyStatus "
				+ "AND p.propertyLocation LIKE %:propertyPlace% "
				+ "AND t.activeStatus!=2 GROUP BY t.properties.propertyId ORDER BY p.maxPrice DESC")
		List<Properties> fetchPropertiesBasedPropertyTypeAndCollectionDesc(double minPrice, double maxPrice, String propertyPlace,
				String propertyStatus, Set<String> collectionSet, Set<String> propertyTypeSet);

		//without Property Status
		@Query(value = "FROM Properties p JOIN p.propertyTags t WHERE t.properties.propertyId=p.propertyId "
				+ "AND t.tagName IN (:collectionSet) AND p.activeStatus=1 "
				+ "AND p.minPrice>=:minPrice "
				+ "AND p.maxPrice<=:maxPrice "
				+ "AND p.type IN (:propertyTypeSet)"
				+ "AND p.propertyLocation LIKE %:propertyPlace% "
				+ "AND t.activeStatus!=2 GROUP BY t.properties.propertyId ORDER BY p.maxPrice DESC")
		List<Properties> fetchPropertiesBasedPropertyTypeAndCollectionDescWithoutPropertyStatus(double minPrice,
				double maxPrice, String propertyPlace, Set<String> collectionSet, Set<String> propertyTypeSet);
		//filters queries end for high to low

	@Query(value = "FROM Properties p WHERE p.user.userId = :userId ORDER BY p.propertyId DESC")
	List<Properties> fetchProperties(int userId);

	@Query(value = "Select p.propertyLocation FROM Properties p WHERE p.propertyLocation LIKE %:propertyPlace%")
	Set<String> fetchLocation(String propertyPlace);

	@Query(value="FROM Properties p WHERE p.activeStatus=1 AND p.propertyLocation LIKE %:propertyPlace%")
	List<Properties> fetchAllPropertiesBasedOnPincode(String propertyPlace);

	@Query(value="Select new com.atechtron.beegru.login.dto.PropertyImageDto(p.propertyType,p.propertyStatus) FROM Properties p  WHERE p.propertyId = :propertyId")
	PropertyImageDto getPropertyImageDto(int propertyId);

	@Query(value = "FROM Properties p WHERE p.propertyType = :propertyType AND p.propertyStatus = :propertyStatus AND p.activeStatus=1")
	List<Properties> getSimilerProperties(String propertyType, String propertyStatus);

	@Query(value="FROM Properties p WHERE p.activeStatus=1 AND p.propertyLocation LIKE %:propertyPlace%")
	List<Properties> getPropertiesOnlyBasedOnLocation(String propertyPlace);

	@Query(value="FROM Properties p WHERE p.activeStatus=1 AND p.propertyLocation LIKE %:propertyPlace%")
	List<Properties> fetchAllProperties(String propertyPlace);
}
