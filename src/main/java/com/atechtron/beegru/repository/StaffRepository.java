package com.atechtron.beegru.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.atechtron.beegru.login.model.StaffDetails;

import lombok.Data;

public interface StaffRepository extends JpaRepository<StaffDetails, Integer>{

	@Query(value = "FROM StaffDetails s WHERE s.managerDetails.managerDetailsId = :managerDetailsId")
	List<StaffDetails> findByManager(int managerDetailsId);

}
