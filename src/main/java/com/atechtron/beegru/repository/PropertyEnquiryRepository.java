package com.atechtron.beegru.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.atechtron.beegru.login.dto.PropertyEnquiryDto;
import com.atechtron.beegru.login.model.PropertyEnquiry;

public interface PropertyEnquiryRepository extends JpaRepository<PropertyEnquiry, Integer>{

	@Query(value="Select new com.atechtron.beegru.login.dto.PropertyEnquiryDto(a.propertyEnquiryId,a.name,a.mobileNumber,a.emailId,a.message,a.propertyId,a.addedDate,a.activeStatus) FROM PropertyEnquiry a")
	List<PropertyEnquiryDto> findAllpropertyEnquiry();

	@Transactional
    @Modifying
	@Query(value = "update PropertyEnquiry l set l.activeStatus = :status where l.propertyEnquiryId = :propertyEnquiryId")
	int updatePropertyEnquiryStatus(int propertyEnquiryId, int status);

}
