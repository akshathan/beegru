package com.atechtron.beegru.auth.service;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class UserAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest arg0, HttpServletResponse arg1,
			Authentication authentication) throws IOException, ServletException {
		System.out.println("inside redirect");
		boolean hasUserRole = false;
		boolean hasManagerRole = false;
		boolean hasStaffRole = false;
		boolean hasSuperAdminRole = false;
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
		for (GrantedAuthority grantedAuthority : authorities) {
			System.out.println(grantedAuthority.getAuthority());
			if (grantedAuthority.getAuthority().equals("Customer")) {
				hasUserRole = true;
				break;
			} else if (grantedAuthority.getAuthority().equals("Manager")) {
				hasManagerRole = true;
				break;
			}else if (grantedAuthority.getAuthority().equals("Staff")) {
				hasStaffRole = true;
				break;
			}else if (grantedAuthority.getAuthority().equals("SuperAdmin")) {
				hasSuperAdminRole = true;
				break;
			}
			
		}

		if (hasUserRole) {
			redirectStrategy.sendRedirect(arg0, arg1, "/user/index");
		} else if (hasManagerRole) {
			System.out.println("INside manager");
			redirectStrategy.sendRedirect(arg0, arg1, "/manager/manager_dashboard");
		}else if (hasStaffRole) {
			redirectStrategy.sendRedirect(arg0, arg1, "/staff/staff_dashboard");
		} else if (hasSuperAdminRole) {
			redirectStrategy.sendRedirect(arg0, arg1, "/admin/superadmin_dashboard");
		} else {
			throw new IllegalStateException();
		}
	}

}