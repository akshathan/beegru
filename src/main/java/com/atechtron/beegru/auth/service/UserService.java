package com.atechtron.beegru.auth.service;

import com.atechtron.beegru.website.model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
