package com.atechtron.beegru.auth.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.atechtron.beegru.repository.UserRepository;
import com.atechtron.beegru.website.model.Role;
import com.atechtron.beegru.website.model.User;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{
    @Autowired
    private UserRepository userRepository;
    
    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) {
    	User user = userRepository.findByMobileNumberOrEmailId(username);
    	
    	if (user == null)  
    	//	user = userRepository.findByEmailId(username);
    	//System.out.println(user);
    //	if(user ==null)
    		throw new UsernameNotFoundException("invalid mobileNumber");
    	
    	if(user.getActiveStatus()!=1) {
			throw new UsernameNotFoundException("Not a Valid User");
		}
        
    	Set<GrantedAuthority> grantedAuthorities = new HashSet<>();   
        for (Role role : user.getRoles()){
        	//System.out.println(role.getUserRoleName()+"ssdsd");
           grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
        }
        return new org.springframework.security.core.userdetails.User(user.getMobileNumber(), user.getPassword(), grantedAuthorities);
    }
}
