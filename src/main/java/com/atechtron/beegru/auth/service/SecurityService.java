package com.atechtron.beegru.auth.service;

public interface SecurityService {
    String findLoggedInUsername();

    void autoLogin(String string, String password);


}
