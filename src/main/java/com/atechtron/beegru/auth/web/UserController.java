package com.atechtron.beegru.auth.web;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping(value = "/user")
public class UserController {
	

    @GetMapping("/login")
    public String login(Model model, String error, String logout) throws UsernameNotFoundException{
    	String url = "";
    	try {
    		if (error != null)
    			System.out.println(error +"inside error");
    			model.addAttribute("error", "Your username and password is invalid.");
    			url = "admin_login";

    		if (logout != null)
            	model.addAttribute("message", "You have been logged out successfully.");
            	url = "admin_login";
            	return url;
    	}catch (Exception e) {
    		System.out.println(e.getMessage()+" inside user Controller");
    		e.printStackTrace();
    		return url;
    	}
    }
    
    @RequestMapping(value = "forget_password", method = RequestMethod.GET)
    public String forgetPassword() {
    	return "forgot_password";
    }
    
}
