package com.atechtron.beegru;

import java.text.DecimalFormat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@ComponentScan(basePackages = {"com.atechtron.beegru"})
@SpringBootApplication
public class BeegruApplication {

	public static void main(String[] args) {
		SpringApplication.run(BeegruApplication.class, args);
//		double a = 999999999.19999999;
////		if(a>10000000.00) {
//			System.out.println(Math.floor(a));
//			System.out.println(Math.ceil(a));
//			System.out.println(Math.round(a));
//			String f1 = new DecimalFormat("##.##").format(a);
//			System.out.println(f1 +"true");
////		}else {
//			System.out.println("false");
////		}
		
		String str = "1234abcd";
	//	String[] part = str.split("(?<!=\\D)(?!=\\d)");
		String numberOnly= str.replaceAll("[^0-9]", "");
		System.out.println(numberOnly);
	//	System.out.println(part[1]);
		
	}
	
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		System.out.println("dffdfdfdfsaaaaaaaaaa");
		return new WebMvcConfigurer() {
			@Override
		    public void addCorsMappings(CorsRegistry registry) {
		        registry.addMapping("/**")
		                .allowedOrigins("http://localhost:3000")
		                .allowedOrigins("http://localhost:3001")
		                .allowedMethods("GET","POST");
		    }
		};
	}

}
