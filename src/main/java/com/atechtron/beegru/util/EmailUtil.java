package com.atechtron.beegru.util;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;

@Component
public class EmailUtil {
	
	//@Autowired
	//private SimpleMailMessage preConfiguredMessage;
	
	@Autowired
	private JavaMailSender mailSender;

	/**
	 * This method will send compose and send the message
	 * @throws Exception 
	 * */
	public void sendMail(final String from, final String[] to, final String[] cc,
			final String[] bcc, final String subject, final String body, final String attachmentPath) throws Exception {
		try{
//		System.out.println(CommonUtil.isEmptyString(attachmentPath));
//		if (!CommonUtil.isEmptyString(attachmentPath)) {
//			sendAttachmentMail(to, cc, bcc, subject, body, attachmentPath);
//		} else {
//			sendSimpleMail(from, to, cc, bcc, subject, body);
//		}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void sendSimpleMail(final String from, final String[] to, final String[] cc,
			final String[] bcc, final String subject, final String body) throws Exception {
		if (to.length > 0) {
			/*SimpleMailMessage message = new SimpleMailMessage();
			message.setTo(to);
			if (null != cc && cc.length > 0) {
				message.setCc(cc);
			}
			if (null != bcc && bcc.length > 0) {
				message.setBcc(bcc);
			}
			message.setSubject(subject);
			message.setText(body);*/
			
			MimeMessagePreparator preparator = new MimeMessagePreparator() {
		        
	            public void prepare(MimeMessage mimeMessage) throws Exception {
	            	Address [] addTo = new InternetAddress[to.length];
	    			for (int i = 0; i < to.length; i++) {
	    				addTo[i] = new InternetAddress(to[i]);
	    			}
	    			Address [] addCc = new InternetAddress[cc.length];
	    			if (cc.length > 0) {		    			
		    			for (int i = 0; i < cc.length; i++) {
		    				addCc[i] = new InternetAddress(cc[i]);
		    			}		    			
	    			}
	    			Address [] addBcc = new InternetAddress[bcc.length];
	    			if (bcc.length > 0) {	    				
		    			for (int i = 0; i < bcc.length; i++) {
		    				addBcc[i] = new InternetAddress(bcc[i]);
		    			}		    			
	    			}
	    			mimeMessage.setFrom(new InternetAddress(from));
	                mimeMessage.setRecipients(Message.RecipientType.TO, addTo);	
	                mimeMessage.setRecipients(Message.RecipientType.CC, addCc);
	                mimeMessage.setRecipients(Message.RecipientType.BCC, addBcc);
	                mimeMessage.setText(body, "UTF-8", "html");
	                mimeMessage.setSubject(subject);
	            }
	        };
			try{
			mailSender.send(preparator);
//			mailSender.send(message);
			}catch(Exception e){
//				System.out.println("EMAILEXCEPtion"+e.getMessage());
//				throw e;
			}
		
		
		}
	}
	
	public void sendSimpleMailTo(final String from, final String[] to, final String[] cc,
			final String[] bcc, final String subject, final String body) throws Exception {
		if (to.length > 0) {
			/*SimpleMailMessage message = new SimpleMailMessage();
			message.setTo(to);
			if (null != cc && cc.length > 0) {
				message.setCc(cc);
			}
			if (null != bcc && bcc.length > 0) {
				message.setBcc(bcc);
			}
			message.setSubject(subject);
			message.setText(body);*/
			
			MimeMessagePreparator preparator = new MimeMessagePreparator() {
		        
	            public void prepare(MimeMessage mimeMessage) throws Exception {
//	            	System.out.println("inside preparator");  
	            	Address [] addTo = new InternetAddress[to.length];
	    			for (int i = 0; i < to.length; i++) {
	    				addTo[i] = new InternetAddress(to[i]);
	    			}
	    			Address [] addCc = new InternetAddress[cc.length];
	    			if (cc.length > 0) {		    			
		    			for (int i = 0; i < cc.length; i++) {
		    				addCc[i] = new InternetAddress(cc[i]);
		    			}		    			
	    			}
	    			Address [] addBcc = new InternetAddress[bcc.length];
	    			if (bcc.length > 0) {	    				
		    			for (int i = 0; i < bcc.length; i++) {
		    				addBcc[i] = new InternetAddress(bcc[i]);
		    			}		    			
	    			}
	    			mimeMessage.setFrom(new InternetAddress(from));
	                mimeMessage.setRecipients(Message.RecipientType.TO, addTo);	
	                mimeMessage.setRecipients(Message.RecipientType.CC, addCc);
	                mimeMessage.setRecipients(Message.RecipientType.BCC, addBcc);
	                mimeMessage.setText(body, "UTF-8", "html");
	                mimeMessage.setSubject(subject);
	            }
	        };
			try{
			mailSender.send(preparator);
//			mailSender.send(message);
			}catch(Exception e){
			//System.out.println("EMAILEXCEPtion"+e.getMessage());
			e.printStackTrace();
				throw e;
			}
		}
	}
	
	@SuppressWarnings("unused")
	private void sendAttachmentMail(final String[] to, final String[] cc,
			final String[] bcc, final String subject, final String body, final String attachmentPath) {
		if (to.length > 0) {
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			try {
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true);
				if (null != attachmentPath && !"".equals(attachmentPath.trim())) {
					FileSystemResource file = new FileSystemResource(attachmentPath);
					message.addAttachment(file.getFilename(), file);
				}
				message.setTo(to);
				if (null != cc && cc.length > 0) {
					message.setCc(cc);
				}
				if (null != bcc && bcc.length > 0) {
					message.setBcc(bcc);
				}
				message.setSubject(subject);
				message.setText(body);
			} catch (MessagingException e) {
				//CommonUtil.logException(e, EmailUtil.class);
			}
			mailSender.send(mimeMessage);
		}
	}
	
	public void sendMail(final String from, final String to, final String subject, final String body, final String attachmentPath) throws Exception {
		String [] arr = { to };
		sendMail(from, arr, null, null, subject, body, attachmentPath);
	}
	
	public void sendMail(final String from, final String to, final String cc, final String bcc, final String subject, final String body, final String attachmentPath) throws Exception {
		String [] toArr = { to }, ccArr = { cc }, bccArr = { bcc };
		sendMail(from, toArr, ccArr, bccArr, subject, body, attachmentPath);
	}

	/**
	 * This method will send a pre-configured message
	 * */
//	public void sendPreConfiguredMail(String message) {
//		SimpleMailMessage mailMessage = new SimpleMailMessage(
//				preConfiguredMessage);
//		mailMessage.setText(message);
//		mailSender.send(mailMessage);
//	}

}
