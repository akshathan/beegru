package com.atechtron.beegru.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.Collection;
import java.util.Date;
import java.util.Random;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

public class Util {

	public static String randomNumberForOtp() {
		StringBuilder sb = new StringBuilder();
		try {
			Random rndm_method = new Random();
			sb.append(String.valueOf(rndm_method.nextInt(999999)));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	public static int uploadFile(String base64String, String fileName, String type, int productId) throws IOException {
		BufferedOutputStream scanStream = null;
		System.out.println("inside uploadfiles");
		Date date = new Date();
		System.out.println(date.toString());
		try {
			Base64.Decoder decoder = Base64.getDecoder();
			byte[] imageByte = decoder.decode(base64String.split("\\,")[1]);
			System.out.println(imageByte);
			String rootPath = System.getProperty("user.home");
			System.out.println(rootPath);
			File fileSaveDir = new File(rootPath + File.separator + "public_html" + File.separator + "assets"
					+ File.separator + type + File.separator + String.valueOf(productId));
			// Creates the save directory if it does not existsSS
			if (!fileSaveDir.exists()) {
				fileSaveDir.mkdirs();
				// Files.createDirectories(fileSaveDir);
			}
			File scanFile = new File(fileSaveDir.getAbsolutePath() + File.separator + fileName);
			scanStream = new BufferedOutputStream(new FileOutputStream(scanFile));
			scanStream.write(imageByte);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
			// System.out.println("inside utils");
			return 0;
		} finally {
			scanStream.close();
			System.out.println(date.toString() + " Inside Finally");
		}
	}

	public static String nameFormat(String name) {
		System.out.println(name.substring(1));
		String[] arr = name.split(" ");
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < arr.length; i++) {
			sb.append(Character.toUpperCase(arr[i].charAt(0))).append(arr[i].substring(1).toLowerCase()).append(" ");
		}
		return sb.toString();
	}

	public static String CurrentDateTime() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy");
		LocalDateTime now = LocalDateTime.now();
		System.out.println(dtf.format(now));
		return dtf.format(now);
	}

	public static int getRole() {
		@SuppressWarnings("unchecked")
		Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) SecurityContextHolder.getContext()
				.getAuthentication().getAuthorities();
		int role = 0;
		boolean hasRole = false;
		for (GrantedAuthority authority : authorities) {
			hasRole = authority.getAuthority().equals("Customer");
			if (hasRole) {
				role = 1;
				// return role;
			}
			hasRole = authority.getAuthority().equals("Retailer");
			if (hasRole) {
				role = 2;
				// return role;
			}
			hasRole = authority.getAuthority().equals("WholeSaler");
			if (hasRole) {
				role = 3;
				// return role;
			}
			hasRole = authority.getAuthority().equals("Distributer");
			if (hasRole) {
				role = 4;
				// return role;
			}
			hasRole = authority.getAuthority().equals("Export");
			if (hasRole) {
				role = 5;
				// return role;
			}
			hasRole = authority.getAuthority().equals("Purchase");
			if (hasRole) {
				role = 6;
				// return role;
			}

		}
		return role;
	}

	public static final String[] units = { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine",
			"Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen",
			"Nineteen" };

	public static final String[] tens = { "", // 0
			"", // 1
			"Twenty", // 2
			"Thirty", // 3
			"Forty", // 4
			"Fifty", // 5
			"Sixty", // 6
			"Seventy", // 7
			"Eighty", // 8
			"Ninety" // 9
	};

	public static String convert(final double d) {
		if (d < 0) {
			return "Minus " + convert(-d);
		}

		if (d < 20) {
			return units[(int) d];
		}

		if (d < 100) {
			return tens[(int) (d / 10)] + ((d % 10 != 0) ? " " : "") + units[(int) (d % 10)];
		}

		if (d < 1000) {
			return units[(int) (d / 100)] + " Hundred" + ((d % 100 != 0) ? " " : "") + convert(d % 100);
		}

		if (d < 100000) {
			return convert(d / 1000) + " Thousand" + ((d % 10000 != 0) ? " " : "") + convert(d % 1000);
		}

		if (d < 10000000) {
			return convert(d / 100000) + " Lakh" + ((d % 100000 != 0) ? " " : "") + convert(d % 100000);
		}
		return convert(d / 10000000) + " Crore" + ((d % 10000000 != 0) ? " " : "") + convert(d % 10000000);
	}

}
