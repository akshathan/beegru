package com.atechtron.beegru.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;

public class GeneratePdfReport1 {
	public static ByteArrayInputStream citiesReport() throws IOException {

		
//		Rectangle rect = new Rectangle(
//			    rectangle.getLeft(), rectangle.getTop() - 1,
//			    rectangle.getRight(), rectangle.getBottom() + 1);
//			TextField text = new TextField(writer, rect, String.format("text1_%s",tf));
//		
		Rectangle pageSize = new Rectangle(900,900);
		//pageSize.setBackgroundColor(new BaseColor(0xFF, 0xFF, 0xDE));
//		 rectangle.getLeft(), rectangle.getTop() - 1,
//		    rectangle.getRight(), rectangle.getBottom() + 1);
	//	TextField text = new TextField(writer, pageSize, String.format("text1_%s",tf));
		Document document = new Document(pageSize);
		//Document document = new Document();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {

			PdfPTable table = new PdfPTable(3);
			table.setWidthPercentage(60);
			table.setWidths(new int[] { 1, 3, 3 });

			Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);

			PdfPCell hcell;
			hcell = new PdfPCell(new Phrase("Id", headFont));
			hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Name", headFont));
			hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(hcell);

			hcell = new PdfPCell(new Phrase("Population", headFont));
			hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(hcell);

			StringBuilder htmlString = new StringBuilder();
			htmlString.append(new String("<html>\r\n" + 
					"<head>\r\n" + 
					"\r\n" + 
					"<title>ee</title>\r\n" + 
					"\r\n" + 
					"  <meta charset=\"utf-8\"/>\r\n" + 
					"  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>\r\n" + 
					"  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css\"/>\r\n" + 
					"  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>\r\n" + 
					"  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js\"></script>\r\n" + 
					"<style>\r\n" + 
					"label {\r\n" + 
					"    color: #475F7B;\r\n" + 
					"    font-size: 0.8rem;\r\n" + 
					"    text-transform: uppercase;\r\n" + 
					"    font-weight: 500;\r\n" + 
					"}\r\n" + 
					"label {\r\n" + 
					"    display: inline-block;\r\n" + 
					"    margin-bottom: 0.2rem;\r\n" + 
					"}\r\n" + 
					"*, *::before, *::after {\r\n" + 
					"    box-sizing: border-box;\r\n" + 
					"}\r\n" + 
					"user agent stylesheet\r\n" + 
					"label {\r\n" + 
					"    cursor: default;\r\n" + 
					"}\r\n" + 
					"\r\n" + 
					"\r\n" + 
					"        table {\r\n" + 
					"            width: 100%;\r\n" + 
					"        }\r\n" + 
					"\r\n" + 
					"        thead, tbody, tr, td, th { display: block; }\r\n" + 
					"\r\n" + 
					"        tr:after {\r\n" + 
					"            content: ' ';\r\n" + 
					"            display: block;\r\n" + 
					"            visibility: hidden;\r\n" + 
					"            clear: both;\r\n" + 
					"        }\r\n" + 
					"\r\n" + 
					"        thead th {\r\n" + 
					"            height: 30px;\r\n" + 
					"\r\n" + 
					"            /*text-align: left;*/\r\n" + 
					"        }\r\n" + 
					"\r\n" + 
					"        tbody {\r\n" + 
					"            height: 120px;\r\n" + 
					"            overflow-y: auto;\r\n" + 
					"        }\r\n" + 
					"\r\n" + 
					"        thead {\r\n" + 
					"            /* fallback */\r\n" + 
					"        }\r\n" + 
					"\r\n" + 
					"\r\n" + 
					"        tbody td, thead th {\r\n" + 
					"            width: 19.2%;\r\n" + 
					"            float: left;\r\n" + 
					"        }\r\n" + 
					"</style>\r\n" + 
					"</head>\r\n" + 
					"<body>\r\n" + 
					"\r\n" + 
					"			<label style=\"font-size: 1.0rem;\">Property Name:</label><br/>\r\n" + 
					"				<p id=\"propertyName\">3BHK LUXURY APARTMENTS</p>\r\n" + 
					"			<label style=\"font-size: 1.0rem;\">Property Price:</label><br/>\r\n" + 
					"				<p id=\"propertyPrice\">24.0 cr-27.0 cr</p>	\r\n" + 
					"				\r\n" + 
					"			<label style=\"font-size: 1.0rem;\">Property Location:</label><br/>\r\n" + 
					"				<p id=\"propertyLocation\">Bannerghatta Main Road, Dollars Colony, Phase 4, J P Nagar, Bengaluru, Karnataka\r\n" + 
					"</p>\r\n" + 
					"			<label style=\"font-size: 1.0rem;\">Property Description:</label><br/>\r\n" + 
					"				<p id=\"propertyDescription\">NA</p>\r\n" + 
					"			<label style=\"font-size: 1.0rem;\">Property Type:</label><br/>\r\n" + 
					"				<p id=\"propertyType\">resedentialForrent</p>\r\n" + 
					"			<label style=\"font-size: 1.0rem;\">Land Area:</label><br/>\r\n" + 
					"				<p id=\"landArea\">1200Sqft</p>\r\n" + 
					"			<label style=\"font-size: 1.0rem;\">About Project:</label><br/>\r\n" + 
					"				\r\n" + 
					"</body>\r\n" + 
					"</html>"));
			//htmlString.append(new String("<tr><td> Google Here </td> <td><a href='www.google.com'>Google</a> </td></tr></table></body></html>"));
			  
			
			// document.close();
			XMLWorkerHelper worker = XMLWorkerHelper.getInstance();
			
			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			 
			document.open();
			worker.parseXHtml(pdfWriter, document, new StringReader(htmlString.toString()));			
//			String imageUrl = "https://www.atechtron.com/assets/camera/68/image5.png";
//			//String imageUrl = "/resources/websiteAssets/img/carousel/2.jpg";
//			//imageUrl.setAbsolutePosition(450f, 10f);
//		            Image image2 = Image.getInstance(new URL(imageUrl));
//		            image2.setBorderWidth(40f);
//		           // image2.set
//		            image2.setAbsolutePosition(200f, 10f);
		//        document.add(image2);
			////document.add(table);
			//document.addAuthor(htmlString.toString());
			document.close();

		} catch (DocumentException ex) {

			Logger.getLogger(GeneratePdfReport1.class.getName()).log(Level.SEVERE, null, ex);
		}

		return new ByteArrayInputStream(out.toByteArray());
	}
}
