package com.atechtron.beegru.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class SmsUtil {

	String username = "9886600621";
	// password that is to be used along with username
	String password = "UURKN";
	// Message content that is to be transmitted
	
	// Sender Id to be used for submitting the message
	String senderId = "SMSTST";

	public void sendSms(String destination, String message) {
		try {
			// Url that will be called to submit the message
			URL sendUrl = new URL("http://smsidea.co.in/sendsms.aspx");
			HttpURLConnection httpConnection = (HttpURLConnection) sendUrl.openConnection();
			// This method sets the method type to POST so that will be send as a POST
			//request.
			httpConnection.setRequestMethod("POST");
			// This method is set as true wince we intend to send input to the server.
			httpConnection.setDoInput(true);
			// This method implies that we intend to receive data from server.
			httpConnection.setDoOutput(true);
			// Implies do not use cached data
			httpConnection.setUseCaches(false);
			// Data that will be sent over the stream to the server.
			DataOutputStream dataStreamToServer = new DataOutputStream(httpConnection.getOutputStream());
			dataStreamToServer.writeBytes("mobile=" + URLEncoder.encode(username, "UTF-8") + "&pass="
					+ URLEncoder.encode(password, "UTF-8") + "&senderid="
					+ URLEncoder.encode(senderId, "UTF-8") + "&to=" + URLEncoder.encode(destination, "UTF-8")
					+ "&msg=" + URLEncoder.encode(message, "UTF-8"));
			dataStreamToServer.flush();
			dataStreamToServer.close();
			// Here take the output value of the server.
			BufferedReader dataStreamFromUrl = new BufferedReader(
					new InputStreamReader(httpConnection.getInputStream()));
			String dataFromUrl = "", dataBuffer = "";
			// Writing information from the stream to the buffer
			while ((dataBuffer = dataStreamFromUrl.readLine()) != null) {
				dataFromUrl += dataBuffer;
			}
			/**
			 * Now dataFromUrl variable contains the Response received from the server so we
			 * can parse the response and process it accordingly.
			 */
			System.out.println("Response: " + dataFromUrl);
			dataStreamFromUrl.close();
			RestTemplate rt = new RestTemplate();
			
			URI uri = UriComponentsBuilder.fromHttpUrl("http://smsidea.co.in/sendsms.aspx").build().toUri();

			ResponseEntity<String> response = rt.getForEntity(uri, String.class);
				System.out.println(response.getStatusCode());
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

//	public static void main(String[] args) {
//		try {
//			// Below example is for sending Plain text
//			SmsUtil s = new SmsUtil("9886600621", "UURKN", "" + 111111
//					+ " is the One Time Password for registering yourself for GK Vale. Please do not share this with others.", "8970685540", "SMSBUZ");
//			s.submitMessage();
//		} catch (Exception ex) {
//		}
//	}
}
