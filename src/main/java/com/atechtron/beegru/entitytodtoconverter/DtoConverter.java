package com.atechtron.beegru.entitytodtoconverter;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

import com.atechtron.beegru.login.dto.BeegruScoreDto;
import com.atechtron.beegru.login.dto.BranchDto;
import com.atechtron.beegru.login.dto.ManagerDto;
import com.atechtron.beegru.login.dto.PostYourRequirementDto;
import com.atechtron.beegru.login.dto.PropertiesDto;
import com.atechtron.beegru.login.dto.PropertyAssignedDto;
import com.atechtron.beegru.login.dto.PropertyInfoDto;
import com.atechtron.beegru.login.dto.PropertyTagDto;
import com.atechtron.beegru.login.dto.PropertyVicinityDto;
import com.atechtron.beegru.login.dto.StaffDto;
import com.atechtron.beegru.login.dto.UserDetails;
import com.atechtron.beegru.login.model.AgentDetails;
import com.atechtron.beegru.login.model.BeegruScore;
import com.atechtron.beegru.login.model.BranchDetails;
import com.atechtron.beegru.login.model.ManagerDetails;
import com.atechtron.beegru.login.model.PeopleTestimonials;
import com.atechtron.beegru.login.model.PostYourRequirement;
import com.atechtron.beegru.login.model.Properties;
import com.atechtron.beegru.login.model.PropertyAssignDetails;
import com.atechtron.beegru.login.model.PropertyTags;
import com.atechtron.beegru.login.model.StaffDetails;
import com.atechtron.beegru.login.model.WishList;
import com.atechtron.beegru.website.dto.PropertyDetailsDto;
import com.atechtron.beegru.website.dto.TestimonialsDto;
import com.atechtron.beegru.website.model.Role;
import com.atechtron.beegru.website.model.User;

public class DtoConverter {

	public static List<PropertyInfoDto> fetchPropertyInfo(List<Properties> propertiesList) {
		List<PropertyInfoDto> propertyList = new ArrayList<PropertyInfoDto>();
		try {
			for(Properties propertyObj:propertiesList) {
				PropertyInfoDto propertyInfoDto = new PropertyInfoDto();
				propertyInfoDto.setPropertyId(propertyObj.getPropertyId());
				propertyInfoDto.setPropertyName(propertyObj.getPropertyName());
				propertyInfoDto.setPropertyLocation(propertyObj.getPropertyLocation());
				propertyInfoDto.setName(propertyObj.getUser().getName());
				propertyInfoDto.setMobileNumber(propertyObj.getUser().getMobileNumber());
				propertyInfoDto.setDisplayPrice(propertyObj.getDisplayPrice());
				propertyInfoDto.setAddedDate(propertyObj.getAddedDate());
				propertyInfoDto.setActiveStatus(propertyObj.getActiveStatus());
				propertyInfoDto.setPropertyStatus(propertyObj.getPropertyStatus());
				propertyInfoDto.setPropertyType(propertyObj.getPropertyType());
				propertyInfoDto.setFeaturedProperties(propertyObj.getFeaturedProperties());
				propertyInfoDto.setNoOfImages(propertyObj.getNoOfImages());
				if(propertyObj.getPropertyAssisgnDetails().size()!=0) {
					for(PropertyAssignDetails propertyAssignDetailsObj:propertyObj.getPropertyAssisgnDetails()) {
						propertyInfoDto.setStaffId(propertyAssignDetailsObj.getStaffDetails().getStaffId());
						propertyInfoDto.setAssignedStaffName(propertyAssignDetailsObj.getStaffDetails().getUser().getName());
						propertyInfoDto.setPropertyAssignedId(propertyAssignDetailsObj.getPropertyAssignedId());
						propertyInfoDto.setAssignedUserId(propertyAssignDetailsObj.getUser().getUserId());
					}
				}else {
					propertyInfoDto.setAssignedStaffName("NA");
				}
				for(Role roleObj:propertyObj.getUser().getRoles()) {
					propertyInfoDto.setRole(roleObj.getName());
				}
				propertyList.add(propertyInfoDto);
			}
			return propertyList;
		}catch(Exception e) {
			e.printStackTrace();
			return propertyList;
		}
	}

	public static PropertiesDto propertiesDto(Properties property, User user) {
		PropertiesDto propertiesDto = new PropertiesDto();
		try {
			Mapper mapper = new DozerBeanMapper();
			propertiesDto = mapper.map(property, propertiesDto.getClass());
			if(user!=null) {
				if(user.getWishList().size()!=0) {
					for(WishList wishListObj:user.getWishList()) {
						if(wishListObj.getProperties().getPropertyId() == property.getPropertyId()) {
							propertiesDto.setPropertyLiked(true);
						}
					}
				}
			}
			System.out.println("inside beegru score");
			if(propertiesDto.getBeegruScore() == null) {
				BeegruScoreDto beegruScore = new BeegruScoreDto();
				beegruScore.setAccesibility("0");
				beegruScore.setAppreciationPotential("0");
				beegruScore.setApprovalStage("0");
				beegruScore.setLegalAndBanksSupport("0");
				beegruScore.setNeighbourhood("0");
				beegruScore.setPriceValue("0");
				beegruScore.setVaastuAndLocation("0");
				propertiesDto.setBeegruScore(beegruScore);
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertiesDto;
	}
	
	public static PropertiesDto propertiesDto(Properties property) {
		PropertiesDto propertiesDto = new PropertiesDto();
		try {
			Mapper mapper = new DozerBeanMapper();
			propertiesDto = mapper.map(property, propertiesDto.getClass());
			System.out.println("inside beegru score");
			System.out.println(propertiesDto.getBeegruScore());		
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertiesDto;
	}


	public static List<PropertyDetailsDto> propertiesWithWishList(List<Properties> properties, User user) {
		List<PropertyDetailsDto> propertyDetailsList = new ArrayList<PropertyDetailsDto>();
		try {
			for(Properties propertiesObj : properties) {
				System.out.println(propertiesObj.getPropertyId()+"propertyId.......");
				PropertyDetailsDto propertyDetailsDto = new PropertyDetailsDto();
				propertyDetailsDto.setPropertyAddress(propertiesObj.getPropertyLocation());
				propertyDetailsDto.setPropertyAmount(propertiesObj.getDisplayPrice());
				propertyDetailsDto.setPropertyId(propertiesObj.getPropertyId());
				//propertyDetailsDto.setPropertyLiked(propertyLiked);
				propertyDetailsDto.setPropertyName(propertiesObj.getPropertyName());
				propertyDetailsDto.setPropertyType(propertiesObj.getPropertyType());
				propertyDetailsDto.setSquareFeet(propertiesObj.getLength()*propertiesObj.getBreadth()+" ");
				propertyDetailsDto.setRooms(propertiesObj.getRooms());
				propertyDetailsDto.setPropertyStatus(propertiesObj.getPropertyStatus());
				propertyDetailsDto.setPropertyType(propertiesObj.getPropertyType());
				if(user!=null) {
					if(user.getWishList().size()!=0) {
						for(WishList wishListObj:user.getWishList()) {
							if(wishListObj.getProperties().getPropertyId() == propertiesObj.getPropertyId()) {
								propertyDetailsDto.setPropertyLiked(true);
							}
						}
					}
//					System.out.println(user.getWishList().get(0).getWishListId()+"sdsdsd");
//					if(user.getWishList().contains(propertiesObj.getPropertyId())) {
//						propertyDetailsDto.setPropertyLiked(true);
//					}
				}
				propertyDetailsList.add(propertyDetailsDto);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyDetailsList;
	}

	public static List<PropertyDetailsDto> propertiesWithoutWishList(List<Properties> properties) {
		List<PropertyDetailsDto> propertyDetailsList = new ArrayList<PropertyDetailsDto>();
		try {
			for(Properties propertiesObj : properties) {
				PropertyDetailsDto propertyDetailsDto = new PropertyDetailsDto();
				propertyDetailsDto.setPropertyAddress(propertiesObj.getPropertyLocation());
				propertyDetailsDto.setPropertyAmount(propertiesObj.getDisplayPrice());
				propertyDetailsDto.setPropertyId(propertiesObj.getPropertyId());
				//propertyDetailsDto.setPropertyLiked(propertyLiked);
				propertyDetailsDto.setPropertyName(propertiesObj.getPropertyName());
				propertyDetailsDto.setPropertyType(propertiesObj.getPropertyType());
				propertyDetailsDto.setSquareFeet(propertiesObj.getLength()*propertiesObj.getBreadth()+"");
				propertyDetailsDto.setRooms(propertiesObj.getRooms());
				propertyDetailsDto.setPropertyStatus(propertiesObj.getPropertyStatus());
				propertyDetailsDto.setPropertyType(propertiesObj.getPropertyType());
				propertyDetailsList.add(propertyDetailsDto);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyDetailsList;
	}

	public static List<PropertyInfoDto> getWishList(User user) {
		List<PropertyInfoDto> propertyList = new ArrayList<PropertyInfoDto>();
		try {
			for(WishList wishListObj : user.getWishList()){
				PropertyInfoDto propertyInfoDto = new PropertyInfoDto();
				propertyInfoDto.setActiveStatus(wishListObj.getProperties().getActiveStatus());
				propertyInfoDto.setAddedDate(wishListObj.getProperties().getAddedDate());
				propertyInfoDto.setDisplayPrice(wishListObj.getProperties().getDisplayPrice());
				propertyInfoDto.setPropertyName(wishListObj.getProperties().getPropertyName());
				propertyInfoDto.setPropertyLocation(wishListObj.getProperties().getPropertyLocation());
				propertyInfoDto.setPropertyId(wishListObj.getProperties().getPropertyId());
				propertyInfoDto.setPropertyStatus(wishListObj.getProperties().getPropertyStatus());
				propertyInfoDto.setPropertyType(wishListObj.getProperties().getPropertyType());
				propertyList.add(propertyInfoDto);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyList;
	}

	public static List<PropertiesDto> fetchSearchedProperties(List<Properties> properties, User user) {
		List<PropertiesDto> propertiesList = new ArrayList<PropertiesDto>();
		try {
				for(Properties propertiesObj:properties) {
					propertiesList.add(propertiesDto(propertiesObj,user));
				}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertiesList;
	}

	public static List<PropertyTagDto> fetchPropertyTagsInfo(List<PropertyTags> propertyTagsList) {
		List<PropertyTagDto> propertyTagList = new ArrayList<PropertyTagDto>();
		try {
			for(PropertyTags propertyTagObj:propertyTagsList) {
				PropertyTagDto propertyTagDto = new PropertyTagDto();
				propertyTagDto.setPosition(propertyTagObj.getPosition());
				propertyTagDto.setPropertyName(propertyTagObj.getProperties().getPropertyName());
				propertyTagDto.setPropertyTagsId(propertyTagObj.getPropertyTagsId());
				propertyTagDto.setTagName(propertyTagObj.getTagName());
				propertyTagDto.setPropertyId(propertyTagObj.getProperties().getPropertyId());
				propertyTagList.add(propertyTagDto);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyTagList;
	}

	public static PropertyTagDto getPropertyTagDto(PropertyTags propertyTags) {
		PropertyTagDto propertyTagDto = new PropertyTagDto();
		try {
			propertyTagDto.setPosition(propertyTags.getPosition());
			propertyTagDto.setPropertyId(propertyTags.getProperties().getPropertyId());
			propertyTagDto.setPropertyName(propertyTags.getProperties().getPropertyName());
			propertyTagDto.setPropertyTagsId(propertyTags.getPropertyTagsId());
			propertyTagDto.setTagName(propertyTags.getTagName());
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyTagDto;
	}

	public static List<UserDetails> getUserDetails(List<User> user, int role) {
		//role 1->customer
		//	2->superAdmin
		//	3->manager
		//	4->staff
		//	5->agent
		System.out.println("inside dto converter");
		List<UserDetails> userDetailsList = new ArrayList<UserDetails>();
		try {
			for(User userObj : user) {
				System.out.println(userObj.getManagerDetails() == null);
				UserDetails userDetails = new UserDetails();
				userDetails.setUserId(userObj.getUserId());
				userDetails.setName(userObj.getName());
				userDetails.setMobileNumber(userObj.getMobileNumber());
				userDetails.setEmailId(userObj.getEmailId());
				userDetails.setAddedBy(userObj.getAddedUserMobileNumber());
				userDetails.setAddress(userObj.getAddress());
				userDetails.setLocation(userObj.getLocation());
				userDetails.setAddedUserId(userObj.getAddedUserId());
				for(Role roleObj:userObj.getRoles()) {
					userDetails.setRole(roleObj.getName());
				}
				if(role == 3) {
					userDetails.setManagerId(userObj.getManagerDetails().getManagerDetailsId());
					System.out.println("inside manager");
					userDetails.setWorkingArea(userObj.getManagerDetails().getBranch().getBranchName()+","+userObj.getManagerDetails().getBranch().getBranchPincode());
				}else if(role == 4) {
					userDetails.setStaffId(userObj.getStaffDetails().getStaffId());
					userDetails.setAssignedManagerName(userObj.getStaffDetails().getManagerDetails().getUser().getName());
				}else if(role == 5) {
					userDetails.setAgentId(userObj.getAgentDetails().getAgentDetailsId());
					if(userObj.getAgentDetails().getStaffDetails()!=null) {
						userDetails.setAssignedStaff(userObj.getAgentDetails().getStaffDetails().getUser().getName());
					}
				}
				System.out.println("inside manager1111111");
				userDetails.setStatus(userObj.getActiveStatus());
				userDetails.setNumberOfPropertyUploaded(userObj.getProperty().size());
				userDetailsList.add(userDetails);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return userDetailsList;
	}

	public static List<BranchDto> getBranchDetails(List<BranchDetails> branchDetailsList) {
		List<BranchDto> branchList = new ArrayList<BranchDto>();
		try {
			for(BranchDetails branchDetailsObj: branchDetailsList) {
				BranchDto branchDto = new BranchDto();
				branchDto.setBranchName(branchDetailsObj.getBranchName());
				branchDto.setBranchId(branchDetailsObj.getBranchId());
				branchDto.setBranchPincode(branchDetailsObj.getBranchPincode());
				branchDto.setBranchStatus(branchDetailsObj.getBranchStatus());
				branchDto.setBranchAddedBy(branchDetailsObj.getBranchAddedBy());
				branchList.add(branchDto);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return branchList;
		
	}

	public static BranchDto fetchBranchDto(BranchDetails branchDetails) {
		BranchDto branchDto = new BranchDto();
		try {
			branchDto.setBranchAddedBy(branchDetails.getBranchAddedBy());
			branchDto.setBranchId(branchDetails.getBranchId());
			branchDto.setBranchName(branchDetails.getBranchName());
			branchDto.setBranchPincode(branchDetails.getBranchPincode());
			branchDto.setBranchStatus(branchDetails.getBranchStatus());
		}catch(Exception e) {
			e.printStackTrace();
		}
		return branchDto;
	}

//	public static List<ManagerDto> fetchManagerDetails(List<User> userList) {
//		List<ManagerDto> managerList = new ArrayList<ManagerDto>();
//		try {
//			for(User userObj:userList) {
//				ManagerDto managerDto = new ManagerDto();
//				managerDto.setManagerId(userObj.getManagerDetails().getManagerDetailsId());
//				managerDto.setManagerName(userObj.getName());
//				managerDto.setWorkingArea(userObj.getManagerDetails().getBranch().getBranchName()+"-"+userObj.getManagerDetails().getBranch().getBranchId());
//			}
//		}catch(Exception e) {
//			e.printStackTrace();
//		}
//		return managerList;
//	}

	public static List<ManagerDto> fetchManagerDetails(List<ManagerDetails> managerList) {
		List<ManagerDto> managerDtoList = new ArrayList<ManagerDto>();
		try {
			for(ManagerDetails managerdetailsObj: managerList) {
				ManagerDto managerDto = new ManagerDto();
				managerDto.setManagerId(managerdetailsObj.getManagerDetailsId());
				managerDto.setManagerName(managerdetailsObj.getUser().getName());
				managerDto.setWorkingArea(managerdetailsObj.getBranch().getBranchName()+","+managerdetailsObj.getBranch().getBranchPincode());;
				managerDtoList.add(managerDto);
			}
		}catch(Exception e) {
			e.printStackTrace();
	}
		return managerDtoList;
	}

	public static List<StaffDto> fetchStaffList(List<StaffDetails> staffList) {
		List<StaffDto> staffDtoList = new ArrayList<StaffDto>();
		try {
			for(StaffDetails staffDetailsObj:staffList) {
				StaffDto staffDto = new StaffDto();
				staffDto.setStaffId(staffDetailsObj.getStaffId());
				staffDto.setStaffName(staffDetailsObj.getUser().getName());
				staffDtoList.add(staffDto);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return staffDtoList;
	}

	public static StaffDto getStaffDto(StaffDetails staffDetails) {
		StaffDto staffDto = new StaffDto();
		List<PropertyInfoDto> propertyList = new ArrayList<PropertyInfoDto>();
		try {
			staffDto.setAssignedManagerName(staffDetails.getManagerDetails().getUser().getName());
			staffDto.setManagerWorkingArea(staffDetails.getManagerDetails().getBranch().getBranchName()+","+staffDetails.getManagerDetails().getBranch().getBranchPincode());
			for(PropertyAssignDetails propertyAssignedDetailsObj : staffDetails.getPropertyAssignDetails()) {
				PropertyInfoDto propertyInfoDto = new PropertyInfoDto();
				propertyInfoDto.setActiveStatus(propertyAssignedDetailsObj.getProperties().getActiveStatus());
				propertyInfoDto.setAddedDate(propertyAssignedDetailsObj.getProperties().getAddedDate());
				propertyInfoDto.setDisplayPrice(propertyAssignedDetailsObj.getProperties().getDisplayPrice());
				propertyInfoDto.setPropertyId(propertyAssignedDetailsObj.getProperties().getPropertyId());
				propertyInfoDto.setPropertyLocation(propertyAssignedDetailsObj.getProperties().getPropertyLocation());
				propertyInfoDto.setPropertyType(propertyAssignedDetailsObj.getProperties().getPropertyType());
				propertyList.add(propertyInfoDto);
			}
			staffDto.setPropertyList(propertyList);
			staffDto.setStaffId(staffDetails.getStaffId());
			staffDto.setStaffMobileNumber(staffDetails.getUser().getMobileNumber());
			staffDto.setStaffName(staffDetails.getUser().getName());
			staffDto.setAddress(staffDetails.getUser().getAddress());
		}catch(Exception e) {
			e.printStackTrace();
		}
		return staffDto;
	}

	public static PropertyAssignedDto getPropertyAssignedDetails(PropertyAssignDetails propertyAssignedDetails) {
		PropertyAssignedDto propertyAssignedDto = new PropertyAssignedDto();
		try {
			propertyAssignedDto.setManagerName(propertyAssignedDetails.getStaffDetails().getManagerDetails().getUser().getName());
			propertyAssignedDto.setManagerWorkingArea(propertyAssignedDetails.getStaffDetails().getManagerDetails().getBranch().getBranchName()+
					","+propertyAssignedDetails.getStaffDetails().getManagerDetails().getBranch().getBranchPincode());
			propertyAssignedDto.setPropertyAssignedId(propertyAssignedDetails.getPropertyAssignedId());
			propertyAssignedDto.setStaffMobileNamber(propertyAssignedDetails.getStaffDetails().getUser().getMobileNumber());
			propertyAssignedDto.setStaffName(propertyAssignedDetails.getStaffDetails().getUser().getName());
			propertyAssignedDto.setStaffId(propertyAssignedDetails.getStaffDetails().getStaffId());
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyAssignedDto;
	}

	public static List<PropertyAssignedDto> getPropertyAssignedToStaffDetails(
			List<PropertyAssignDetails> propertyAssignDetails) {
		List<PropertyAssignedDto> propertyAssignedDtoList = new ArrayList<PropertyAssignedDto>();
		try {
			for(PropertyAssignDetails propertyAssignDetailsObj: propertyAssignDetails) {
				PropertyAssignedDto propertyAssignedDto = new PropertyAssignedDto();
				propertyAssignedDto.setAssignedUserMobileNumber(propertyAssignDetailsObj.getUser().getMobileNumber());
				propertyAssignedDto.setAssignedUserName(propertyAssignDetailsObj.getUser().getName());
				for(Role roleObj:propertyAssignDetailsObj.getUser().getRoles()) {
					propertyAssignedDto.setAssignedUserRole(roleObj.getName());
				}
				propertyAssignedDto.setPropertyId(propertyAssignDetailsObj.getProperties().getPropertyId());
				propertyAssignedDtoList.add(propertyAssignedDto);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyAssignedDtoList;
	}

	public static StaffDto getStaffDetailsOfAgent(AgentDetails agentDetails) {
		StaffDto staffDto = new StaffDto();
		try {
			staffDto.setAssignedManagerName(agentDetails.getStaffDetails().getManagerDetails().getUser().getName());
			staffDto.setManagerWorkingArea(agentDetails.getStaffDetails().getManagerDetails().getBranch().getBranchName()+
					","+agentDetails.getStaffDetails().getManagerDetails().getBranch().getBranchPincode());
			staffDto.setStaffName(agentDetails.getStaffDetails().getUser().getName());
			staffDto.setStaffMobileNumber(agentDetails.getStaffDetails().getUser().getMobileNumber());
		}catch(Exception e) {
			e.printStackTrace();
		}
		return staffDto;
	}

	public static List<UserDetails> fetchAgentDetailsOfStaff(List<AgentDetails> agentDetailsList) {
		List<UserDetails> userDetailsList = new ArrayList<UserDetails>();
		try {
			for(AgentDetails userObj : agentDetailsList) {
				UserDetails userDetails = new UserDetails();
				userDetails.setUserId(userObj.getUser().getUserId());
				userDetails.setName(userObj.getUser().getName());
				userDetails.setMobileNumber(userObj.getUser().getMobileNumber());
				userDetails.setEmailId(userObj.getUser().getEmailId());
				userDetails.setAddedBy(userObj.getUser().getAddedUserMobileNumber());
				userDetails.setAddress(userObj.getUser().getAddress());
				userDetails.setLocation(userObj.getUser().getLocation());
				userDetails.setAgentId(userObj.getAgentDetailsId());
//				userDetails.setAssignedStaff(userObj.getAgentDetails().getStaffDetails().getUser().getName());
				userDetails.setStatus(userObj.getUser().getActiveStatus());
				userDetails.setNumberOfPropertyUploaded(userObj.getUser().getProperty().size());
				userDetails.setRole("Agent");
				userDetails.setAddedUserId(userObj.getUser().getAddedUserId());
				userDetailsList.add(userDetails);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return userDetailsList;
	}

	public static List<PropertyInfoDto> fetchPropertyAssignedToStaff(List<PropertyAssignDetails> propertyAssignedList) {
		List<PropertyInfoDto> propertyInfoDtoList = new ArrayList<PropertyInfoDto>();
		try {
			for(PropertyAssignDetails propertyAssignDetailsObj:propertyAssignedList) {
				PropertyInfoDto propertyInfoDto = new PropertyInfoDto();
				propertyInfoDto.setPropertyId(propertyAssignDetailsObj.getProperties().getPropertyId());
				propertyInfoDto.setPropertyName(propertyAssignDetailsObj.getProperties().getPropertyName());
				propertyInfoDto.setPropertyLocation(propertyAssignDetailsObj.getProperties().getPropertyLocation());
				propertyInfoDto.setName(propertyAssignDetailsObj.getProperties().getUser().getName());
				propertyInfoDto.setMobileNumber(propertyAssignDetailsObj.getProperties().getUser().getMobileNumber());
				propertyInfoDto.setDisplayPrice(propertyAssignDetailsObj.getProperties().getDisplayPrice());
				propertyInfoDto.setAddedDate(propertyAssignDetailsObj.getProperties().getAddedDate());
				propertyInfoDto.setActiveStatus(propertyAssignDetailsObj.getProperties().getActiveStatus());
				propertyInfoDto.setPropertyStatus(propertyAssignDetailsObj.getProperties().getPropertyStatus());
				propertyInfoDto.setPropertyType(propertyAssignDetailsObj.getProperties().getPropertyType());
				propertyInfoDto.setFeaturedProperties(propertyAssignDetailsObj.getProperties().getFeaturedProperties());
				propertyInfoDto.setAssignedUserId(propertyAssignDetailsObj.getUser().getUserId());
				propertyInfoDtoList.add(propertyInfoDto);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return propertyInfoDtoList;
	}

	public static UserDetails getSingleUserDetails(User user) {
		UserDetails userDetails = new UserDetails();
		try {
			userDetails.setName(user.getName());
			userDetails.setMobileNumber(user.getMobileNumber());
			userDetails.setEmailId(user.getEmailId());
			userDetails.setAddress(user.getAddress());
			userDetails.setLocation(user.getLocation());
			userDetails.setNumberOfPropertyUploaded(user.getProperty().size());
			userDetails.setUserId(user.getUserId());
			userDetails.setAddedUserId(user.getAddedUserId());
			for(Role roleObj:user.getRoles()) {
				userDetails.setRole(roleObj.getName());
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return userDetails;
	}

	public static List<UserDetails> getStaffsOfManager(List<StaffDetails> staffDetails,int role) {
		List<UserDetails> userDetailsList = new ArrayList<UserDetails>();
		try {
			for(StaffDetails staffDetailsObj : staffDetails){
				UserDetails userDetails = new UserDetails();
				userDetails.setUserId(staffDetailsObj.getUser().getUserId());
				userDetails.setName(staffDetailsObj.getUser().getName());
				userDetails.setMobileNumber(staffDetailsObj.getUser().getMobileNumber());
				userDetails.setEmailId(staffDetailsObj.getUser().getEmailId());
				userDetails.setAddedBy(staffDetailsObj.getUser().getAddedUserMobileNumber());
				userDetails.setAddress(staffDetailsObj.getUser().getAddress());
				userDetails.setLocation(staffDetailsObj.getUser().getLocation());
				userDetails.setAddedUserId(staffDetailsObj.getUser().getAddedUserId());
				for(Role roleObj:staffDetailsObj.getUser().getRoles()) {
					userDetails.setRole(roleObj.getName());
				}
				if(role == 4) {
					userDetails.setStaffId(staffDetailsObj.getStaffId());
					userDetails.setAssignedManagerName(staffDetailsObj.getManagerDetails().getUser().getName());
				}
				userDetails.setStatus(staffDetailsObj.getUser().getActiveStatus());
				userDetails.setNumberOfPropertyUploaded(staffDetailsObj.getUser().getProperty().size());
				userDetailsList.add(userDetails);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return userDetailsList;
	}

	public static List<BeegruScoreDto> fetchBeegruScoreList(List<BeegruScore> beegruScore) {
		List<BeegruScoreDto> beegruScoreDtoList = new ArrayList<BeegruScoreDto>();
		try {
			for(BeegruScore beegruScoreObj:beegruScore) {
				BeegruScoreDto beegruScoreDto = new BeegruScoreDto();
				//Mapper mapper = new DozerBeanMapper();
				//beegruScoreDto = mapper.map(beegruScoreObj, beegruScoreDto.getClass());
				beegruScoreDto.setAccesibility(beegruScoreObj.getAccesibility());
				beegruScoreDto.setAppreciationPotential(beegruScoreObj.getAppreciationPotential());
				beegruScoreDto.setApprovalStage(beegruScoreObj.getApprovalStage());
				beegruScoreDto.setBeegruScoreId(beegruScoreObj.getBeegruScoreId());
				beegruScoreDto.setLegalAndBanksSupport(beegruScoreObj.getLegalAndBanksSupport());
				beegruScoreDto.setNeighbourhood(beegruScoreObj.getNeighbourhood());
				beegruScoreDto.setPriceValue(beegruScoreObj.getPriceValue());
				beegruScoreDto.setPropertyId(beegruScoreObj.getProperties().getPropertyId());
				beegruScoreDto.setPropertyName(beegruScoreObj.getProperties().getPropertyName());
				beegruScoreDto.setVaastuAndLocation(beegruScoreObj.getVaastuAndLocation());
				beegruScoreDtoList.add(beegruScoreDto);
				System.out.println(beegruScoreDto);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return beegruScoreDtoList;
	}

	public static BeegruScoreDto fetchBeegruScoreDto(BeegruScore beegruScore) {
		BeegruScoreDto beegruScoreDto = new BeegruScoreDto();
		try {
			//Mapper mapper = new DozerBeanMapper();
			//beegruScoreDto = mapper.map(beegruScoreObj, beegruScoreDto.getClass());
			beegruScoreDto.setAccesibility(beegruScore.getAccesibility());
			beegruScoreDto.setAppreciationPotential(beegruScore.getAppreciationPotential());
			beegruScoreDto.setApprovalStage(beegruScore.getApprovalStage());
			beegruScoreDto.setBeegruScoreId(beegruScore.getBeegruScoreId());
			beegruScoreDto.setLegalAndBanksSupport(beegruScore.getLegalAndBanksSupport());
			beegruScoreDto.setNeighbourhood(beegruScore.getNeighbourhood());
			beegruScoreDto.setPriceValue(beegruScore.getPriceValue());
			beegruScoreDto.setPropertyId(beegruScore.getProperties().getPropertyId());
			beegruScoreDto.setPropertyName(beegruScore.getProperties().getPropertyName());
			beegruScoreDto.setVaastuAndLocation(beegruScore.getVaastuAndLocation());
			return beegruScoreDto;
		}catch(Exception e) {
			e.printStackTrace();
			return beegruScoreDto;
		}	
	}

	public static List<PostYourRequirementDto> getPostYourRequirements(List<PostYourRequirement> postYourRequirement) {
		List<PostYourRequirementDto> postYourRequirementDtoList = new ArrayList<PostYourRequirementDto>();
		try {
			for(PostYourRequirement requirementObj: postYourRequirement) {
				PostYourRequirementDto postYourRequirementDto = new PostYourRequirementDto();
				postYourRequirementDto.setComments(requirementObj.getComments());
				postYourRequirementDto.setMobileNumber(requirementObj.getUser().getMobileNumber());
				postYourRequirementDto.setPostYourRequirementId(requirementObj.getPostYourRequirementId());
				postYourRequirementDto.setPropertyLocation(requirementObj.getPropertyLocation());
				postYourRequirementDto.setPropertyName(requirementObj.getPropertyName());
				postYourRequirementDto.setRequirement(requirementObj.getRequirement());
				postYourRequirementDto.setUserName(requirementObj.getUser().getName());
				postYourRequirementDto.setPostYourRequirementId(requirementObj.getPostYourRequirementId());
				postYourRequirementDto.setActiveStatus(requirementObj.getActiveStatus());
				postYourRequirementDtoList.add(postYourRequirementDto);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return postYourRequirementDtoList;
	}

	public static TestimonialsDto getTestimonials(PeopleTestimonials peopletestimonials) {
		TestimonialsDto testimonialsDto = new TestimonialsDto();
		try {
			testimonialsDto.setAddedDate(peopletestimonials.getAddedDate());
			testimonialsDto.setDate(peopletestimonials.getDate());
			testimonialsDto.setDescription(peopletestimonials.getDescription());
			testimonialsDto.setPeopleTestimonialsId(peopletestimonials.getPeopleTestimonialsId());
			testimonialsDto.setPropertyId(peopletestimonials.getProperties().getPropertyId());
			testimonialsDto.setPropertyName(peopletestimonials.getProperties().getPropertyName());
			testimonialsDto.setRatings(peopletestimonials.getRatings());
			testimonialsDto.setSubject(peopletestimonials.getSubject());
			testimonialsDto.setTestimonialAddress(peopletestimonials.getTestimonialAddress());
			testimonialsDto.setTestimonialName(peopletestimonials.getTestimonialName());
			testimonialsDto.setVideoLink(peopletestimonials.getVideoLink());
		}catch(Exception e) {
			e.printStackTrace();
		}
		return testimonialsDto;
	}

	public static List<TestimonialsDto> gettestimonialList(List<PeopleTestimonials> peopleTestimonials) {
		 List<TestimonialsDto> testimonialList = new ArrayList<TestimonialsDto>();
		try {
			for(PeopleTestimonials peopleTestimonialsObj:peopleTestimonials) {
				testimonialList.add(getTestimonials(peopleTestimonialsObj));
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return testimonialList;
	}

	public static UserDetails getEmployeeDetails(User userObj, int role) {
		UserDetails userDetails = new UserDetails();
		userDetails.setUserId(userObj.getUserId());
		userDetails.setName(userObj.getName());
		userDetails.setMobileNumber(userObj.getMobileNumber());
		userDetails.setEmailId(userObj.getEmailId());
		userDetails.setAddedBy(userObj.getAddedUserMobileNumber());
		userDetails.setAddress(userObj.getAddress());
		userDetails.setLocation(userObj.getLocation());
		userDetails.setPasswordConfirm(userObj.getPasswordConfirm());
		userDetails.setAddedUserId(userObj.getAddedUserId());
		for(Role roleObj:userObj.getRoles()) {
			userDetails.setRole(roleObj.getName());
		}
		if(role == 3) {
			userDetails.setWorkingAreaId(userObj.getManagerDetails().getBranch().getBranchId());
			userDetails.setManagerId(userObj.getManagerDetails().getManagerDetailsId());
			userDetails.setWorkingArea(userObj.getManagerDetails().getBranch().getBranchName()+","+userObj.getManagerDetails().getBranch().getBranchPincode());
		}else if(role == 4) {
			userDetails.setManagerId(userObj.getStaffDetails().getManagerDetails().getManagerDetailsId());
			userDetails.setStaffId(userObj.getStaffDetails().getStaffId());
			userDetails.setAssignedManagerName(userObj.getStaffDetails().getManagerDetails().getUser().getName());
		}else if(role == 5) {
			userDetails.setAgentId(userObj.getAgentDetails().getAgentDetailsId());
			userDetails.setAgentSpeciality(userObj.getAgentDetails().getSpeciality());
			userDetails.setAgentExpectIn(userObj.getAgentDetails().getLocation());
			if(userObj.getAgentDetails().getStaffDetails()!=null) {
				userDetails.setStaffId(userObj.getAgentDetails().getStaffDetails().getStaffId());
				userDetails.setAssignedStaff(userObj.getAgentDetails().getStaffDetails().getUser().getName());
			}
		}
		System.out.println("inside manager1111111");
		userDetails.setStatus(userObj.getActiveStatus());
		userDetails.setNumberOfPropertyUploaded(userObj.getProperty().size());
		return userDetails;
	}
}

