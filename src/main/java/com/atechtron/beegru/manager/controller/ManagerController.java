package com.atechtron.beegru.manager.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.atechtron.beegru.entitytodtoconverter.DtoConverter;
import com.atechtron.beegru.login.dto.PropertyInfoDto;
import com.atechtron.beegru.login.dto.StaffDto;
import com.atechtron.beegru.login.dto.UserDetails;
import com.atechtron.beegru.login.service.LoginService;

@Controller
@RequestMapping(value = "manager")
public class ManagerController {
	
	@Autowired
	private LoginService loginService;
	
	public ManagerController() {
		System.out.println("INside controller");
	}
	
	@RequestMapping(value = "/manager_agent_details", method = RequestMethod.GET)
	public String managerAgentDetails(Model model) {
		try {
			List<UserDetails> userDetails = loginService.fetchUserDetails(5);
			System.out.println(userDetails);
			model.addAttribute("userDetails",userDetails);
			return "manager_agent_details";
		}catch(Exception e) {
			e.printStackTrace();
			return "manager_agent_details";
		}
	}	
	
	@RequestMapping(value = "/manager_dashboard", method = RequestMethod.GET)
	public String manager_dashboard() {
		try {
			return "manager_dashboard";
		}catch(Exception e) {
			e.printStackTrace();
			return "manager_dashboard";
		}
	}	
	
	@RequestMapping(value = "/manager_profile", method = RequestMethod.GET)
	public String manager_profile() {
		try {
			return "manager_profile";
		}catch(Exception e) {
			e.printStackTrace();
			return "manager_profile";
		}
	}	
	@RequestMapping(value = "/manager_report_issue", method = RequestMethod.GET)
	public String manager_report_issue() {
		try {
			return "manager_report_issue";
		}catch(Exception e) {
			e.printStackTrace();
			return "manager_report_issue";
		}
	}	
	
	@RequestMapping(value = "/manager_upload_properties", method = RequestMethod.GET)
	public String manager_upload_properties() {
		try {
			return "manager_upload_properties";
		}catch(Exception e) {
			e.printStackTrace();
			return "manager_upload_properties";
		}
	}	
	
	@RequestMapping(value = "/manager_uploaded_properties", method = RequestMethod.GET)
	public String manager_uploaded_properties(Authentication authentication,Model model) {
		List<PropertyInfoDto> propertyInfoList = new ArrayList<PropertyInfoDto>();
		try {
			propertyInfoList = loginService.fetchPropertyInfo();
			model.addAttribute("propertyInfoList",propertyInfoList);
			return "manager_uploaded_properties";
		}catch(Exception e) {
			e.printStackTrace();
			return "manager_uploaded_properties";
		}
	}	
	
	@RequestMapping(value = "/manager_user_details", method = RequestMethod.GET)
	public String manager_user_details() {
		try {
			return "manager_user_details";
		}catch(Exception e) {
			e.printStackTrace();
			return "manager_user_details";
		}
	}	
	
	@RequestMapping(value = "/manager_staff_details", method = RequestMethod.GET)
	public String manager_staff_details(Authentication authentication,Model model) {
		List<UserDetails> staffDtoList = new ArrayList<UserDetails>();
		try {
			staffDtoList = loginService.fetchStaffsOfManager(authentication.getName());
			model.addAttribute("userDetails",staffDtoList);
			//userDetailsList = DtoConverter.getUserDetails(usersList,role);
			return "manager_staff_details";
		}catch(Exception e) {
			e.printStackTrace();
			return "manager_staff_details";
		}
	}

}
