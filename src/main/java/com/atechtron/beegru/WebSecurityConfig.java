package com.atechtron.beegru;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.atechtron.beegru.auth.service.UserAuthenticationSuccessHandler;


@ComponentScan
@EnableWebSecurity
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{
	
	 @Autowired
	 private UserDetailsService userDetailsService;
	
	@Autowired
	private UserAuthenticationSuccessHandler successHandler;
    
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
    	System.out.println(" inside bcrypt passwordencoder");
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
        .csrf().disable()
         .cors();
           http.authorizeRequests()
                .antMatchers("/app-assets/**","/admin/app-assets/**","/admin/assets/**","/admin/forget_password","/assets/**","/website/signup", "/website/login","login/fetchWishList" ,"/website/saveContactUs","/login/career","/user/**","/website/**").permitAll()
                .antMatchers("/admin/**").hasAuthority("SuperAdmin")
                .antMatchers("/manager/**").hasAuthority("Manager")
                .antMatchers("/staff/**").hasAuthority("Staff")
                .anyRequest().authenticated()
                .and().httpBasic()
                .and()
                .formLogin().successHandler(successHandler)
                .loginPage("/admin/login")
                .permitAll()
                .and()
        		.exceptionHandling().accessDeniedPage("/admin/login")
        		.and()
        		.logout()
                .permitAll();
    }

    @Bean
    public AuthenticationManager customAuthenticationManager() throws Exception {
        return authenticationManager();
    }
    
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
    }
    
    
}